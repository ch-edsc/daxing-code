# -*- coding: utf-8 -*-
import os
import cv2
import argparse
import numpy as np
import sys

cameraNo = "3"

# cd C:\Users\leo_1\Documents\GitHub\daxing-code\deep_sort_pytorch
# python test_draw_turn.py 
class Car:
    def __init__(self, carID = 0, carType = 2):
        self.carType = carType #車種
        self.carID = carID #車種
        self.tracks = [] #軌跡紀錄，目前為[x, y, w, h, frameID]
        #以後根據情況看要不要擴充車子長寬/出現時間點等等數值進去
        self.clusterType = 0
        self.lastFrame = 0#紀錄最後一次更新時的frame
        return
    '''        
    def add(self, x, y):
        #幾乎沒移動時不計算
        if len(self.tracks) > 0:
            last = self.tracks[len(self.tracks) - 1]
            d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
            if d > 400:
                self.tracks.append(np.array([x, y]))
        else:
            self.tracks.append(np.array([x, y]))
        return
    '''
    
    def add(self, x, y, w, h, frameID):
        self.tracks.append(np.array([x, y, w, h, frameID]))
        #if len(self.tracks) > 0:
        #    last = self.tracks[len(self.tracks) - 1]
        #    d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
        #    if d > 400:
        #        self.tracks.append(np.array([x, y, w, h, frameID]))
        #else:
        #    self.tracks.append(np.array([x, y, w, h, frameID]))
        return

    def getAngle(self, v1, v2):
        #向量求夾角，來源 https://www.it145.com/9/94136.html
        x1 = v1[0]
        y1 = v1[1]
        x2 = v2[0]
        y2 = v2[1]
        theta = np.arctan2(x1 * y2 - y1 * x2, x1 * x2 + y1 * y2)
        return theta * 180 / np.pi

def drawCells(img, cells):
    imw = img.shape[1]
    imh = img.shape[0]
    for i in range(cells):
        tarw = int(i * (imw/cells))
        tarh = int(i * (imh/cells))
        cv2.line(img, (0, tarh), (imw, tarh), (255, 255, 255), 1)
        cv2.line(img, (tarw, 0), (tarw, imh), (255, 255, 255), 1)
    return img

videoPath = "data/camera_" + cameraNo + "_2021-09-20-09-00-00.mp4"
#clusteringDataPath = "data/Clustering_Camera" + cameraNo + "_turn.csv"
#clusteringDataPath = "data/Clustering_Camera" + cameraNo + "_extra.csv"
#clusteringDataPath = "data/Clustering_Camera" + cameraNo + "_extraEnd.csv"

clusteringDataPath = "data/tracks" + str(cameraNo) + "_with_label.csv"


outputPath = "output/turn_camera" + cameraNo + ".mp4"
#videoPath = "data/camera14_10_min.mp4"
#clusteringDataPath = "data/Clustering_Camera14_2.csv"
#outputPath = "output/track_camera14_keras.mp4"

npfile = np.loadtxt(clusteringDataPath,dtype=np.int,delimiter=',')
print("read data OK")
carList = {}
for i in range(len(npfile)):
    line = npfile[i]
    car = Car(line[1], line[2])
    car.clusterType = line[0]
    for j in range(3, len(line), 5):
        if line[j+4] == 0:
            break
        car.add(line[j], line[j+1], line[j+2], line[j+3], line[j+4])
    carList[(car.carID, car.carType)] = car

print("set car track OK")
vdo = cv2.VideoCapture()
vdo.open(videoPath)
im_width = int(vdo.get(cv2.CAP_PROP_FRAME_WIDTH))
im_height = int(vdo.get(cv2.CAP_PROP_FRAME_HEIGHT))
#print(im_width)
#print(im_height)
#exit()


frameObjs = []
length = int(vdo.get(cv2.CAP_PROP_FRAME_COUNT))

#print("length:")
#print(length)

for i in range(length+1):
    frameObjs.append([])
for key, car in carList.items():
    for track in car.tracks:
        #print(track[4])
        frameObjs[track[4]].append([track[0], track[1], track[2], track[3], 
                                    car.carID, car.carType, car.clusterType])


#exit(0)

colors =[]
for i in range(50):
    B = (i * 40) % 155 + 100
    G = (i * 90 + 30) % 155 + 100
    R = (i * 110 + 60) % 155 + 100
    colors.append((B, G, R))

idx_frame = 0
trackMap = np.zeros((im_height, im_width, 3), np.uint8)

fourcc = cv2.VideoWriter_fourcc(*'MP4V')
writer = cv2.VideoWriter(outputPath, fourcc, 7, (im_width, im_height))


trackMaps = []
for i in range(20):
    img = np.zeros((im_height, im_width, 3), np.uint8)
    #img = cv2.imread("output/temp%s.png" % cameraNo)
    img = drawCells(img, 16)
    trackMaps.append(img)
    


while vdo.grab():
    idx_frame += 1
    #if idx_frame > 400:
    #    break
    
    _, img = vdo.retrieve()   

    if idx_frame == 1:
        trackMap = np.zeros(img.shape, np.uint8)
    
    nowFrame = frameObjs[idx_frame]
    textSize = 2

    for obj in nowFrame:
        x1 = obj[0]
        x2 = obj[0] + obj[2]
        xc = (x1 + x2) // 2
        y1 = obj[1]
        y2 = obj[1] + obj[3]
        yc = (y1 + y2) // 2
        cv2.putText(img, "%d" %(obj[6]) , (x1, y1), cv2.FONT_HERSHEY_PLAIN, textSize, colors[obj[6]], 2)
        cv2.rectangle(img, (x1, y1), (x2, y2), colors[obj[6]] , 1, cv2.LINE_AA)
        cv2.circle(trackMap, (xc, yc), 2, colors[obj[6]], -1)
        
        cv2.circle(trackMaps[obj[6]], (xc, yc), 2, colors[obj[6]], -1)
        #cv2.circle(trackMaps[obj[6] // 4], (xc, yc), 2, colors[obj[6]], -1)
        #cv2.circle(trackMaps[obj[6] % 4 + 4] , (xc, yc), 2, colors[obj[6]], -1)
        
    #, (x1, y1), (x2, y2), (0, 0, 255), 1, cv2.LINE_AA)
    cv2.imshow("img", img)
    #scaleMat = cv2.resize(trackMap, (50, 50))
    cv2.imshow("trackMap", trackMap)
    #show(trackMap, scale=0.25)
    cv2.waitKey(1)
    #cv2.waitKey(60)
    
    writer.write(img)
writer.release()
for i in range(20):
    cv2.imwrite("output/temp/%d.png" % i, trackMaps[i])
'''
vdo.grab()
_, img = vdo.retrieve()
for key, car in carList.items():
    #trackMap = np.zeros((im_height, im_width, 3), np.uint8)
    trackMap = img.copy()
    for i in range(len(car.tracks)):
        track = car.tracks[i]
        x1 = track[0]
        x2 = track[0] + track[2]
        xc = (x1 + x2) // 2
        y1 = track[1]
        y2 = track[1] + track[3]
        yc = (y1 + y2) // 2   
        cv2.circle(trackMap, (xc, yc), 2, colors[car.clusterType] , 1, cv2.LINE_AA)
        if i > 0:
            preTrack = car.tracks[i-1]
            cv2.line(trackMap, (xc, yc), (preTrack[0] + preTrack[2]//2, preTrack[1] + preTrack[3]//2), colors[car.clusterType], 2)
    
    cv2.putText(trackMap, "%d %d" %(car.carID, car.carType) , (30, 30), cv2.FONT_HERSHEY_PLAIN, 2, (255, 0, 0), 2)
    cv2.imshow("trackMap", trackMap)
    cv2.waitKey(0)
'''
vdo.release()
cv2.waitKey(0)
cv2.destroyAllWindows() 

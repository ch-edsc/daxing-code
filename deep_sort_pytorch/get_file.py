import sys
from urllib import response
import requests
from moviepy.editor import VideoFileClip, concatenate_videoclips
import os
from lxml import html, etree
#from bs4 import BeautifulSoup as bs

cameraNo = 7
year = 2022
month = 3
day = 29
hour = 1
UserName = 'superuserjoy@gmail.com'
Password = 'superuserjoy'
#Password = input('enter your Password')

#首先，到pmis登入頁面抓登入用的 __ VIEWSTATE與 __VIEWSTATEGENERATOR 
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.5005.63 Safari/537.36',
    }
LOGIN_URL = 'https://pmis05.sinotech.com.tw/5049Z/BaseApp/Login.aspx?ReturnUrl=%2f5049Z%2fBaseApp%2fHome%2fSSO_Goto.aspx%3fapp%3dCCTV&app=CCTV'
CCTV_URL ='https://bimdata.sinotech.com.tw'

session = requests.Session()
response = session.get(LOGIN_URL)

html = etree.HTML(response.text)

#顯示所有輸入欄位
print(html.xpath('//input/@name'))

#依照抓到的欄位，再把帳密放進去
data = {
    '__VIEWSTATE': html.xpath('//input[@name="__VIEWSTATE"]/@value')[0],
    '__VIEWSTATEGENERATOR': html.xpath('//input[@name="__VIEWSTATEGENERATOR"]/@value')[0],
    'Login1$UserName': UserName,   
    'Login1$Password': Password,
    'Login1$LoginButton': '登入系統'
}
#登入
response = session.post(LOGIN_URL, headers = headers, data = data)

response.status_code

#然後帶著登入狀態，連到cctv的下載網頁
url = "https://pmis05.sinotech.com.tw/5049Z/BaseApp/Home/SSO_Goto.aspx?app=CCTV"
response = session.get(url)
print(response.text)

#取得清單
camera_adderss_html = etree.HTML(response.text)
print(camera_adderss_html.xpath('//a[@class="ui basic button icon popup_button"][contains(@href, "date")]/@href'))
#清單順序有按照cctv 1 2 3 4 ...排列

findCameraClipUrl = "_%d_" % cameraNo
targetCameraUrl = camera_adderss_html.xpath('//a[@class="ui basic button icon popup_button"][contains(@href, "dates")][contains(@href, "_%d")]/@href'% cameraNo)
print(response.text)
if len(targetCameraUrl) > 0:
    targetCameraUrl = targetCameraUrl[0]
print(targetCameraUrl)

#搜尋時間的get是 ?st=06%3A00&et=07%3A00&page=1
#時間我要抓07
#抓日期的部分省略，直接把dates/改成date/再加日期

page = 1
totalFileList = []
while True:
    timezone = "?st=%02d%%3A58&et=%02d%%3A00&page=%d" % (hour-1, hour+1, page)
    #避免每小時的一開始20秒左右消失，往前抓一點，例如要抓7~8點，就從6:58抓到8:00
    days = "/%d%02d%02d/" % (year, month, day)
    print(timezone)
    print(type(CCTV_URL), targetCameraUrl)
    url = CCTV_URL + targetCameraUrl[0:-2] + days + timezone
    #https://bimdata.sinotech.com.tw/cctv/camera/NO_2_G11/date/20220322/?st=07%3A00&et=08%3A00
    response = session.get(url)
    #print(response.text)

    html = etree.HTML(response.text)
    #"/cctv_archive_media/1/00000141/20220322/_NO_2_G11_2022_03_22_rec_2022_03_22_07_44_25.mp4"
    #時間由大排到小，而且有多個頁面
    fileList = html.xpath('//a[@class="download_button"]/@href')
    if len(fileList) == 0:
        break
    totalFileList.extend(fileList)
    page+=1
print("OK")

#下載與存檔，檔名叫 _NO_2_G11_2022_03_23_hour_2022_03_22_07_00.mp4 這樣的格式好了，
#也就是說抓video的隨便一個，把/前面砍掉再最後12字元換掉，再把_rec_換_hour_
print(totalFileList[0])
saveFileName = (totalFileList[0].split("/")[-1][0:-12] + ("%02d_00_00.mp4" % hour)).replace("_rec_", "_hour_")
print(saveFileName)

#存檔

for i in range(len(totalFileList)-1, -1, -1):
    mp4_url = CCTV_URL + totalFileList[i]
    saveRecPath = totalFileList[i].split("/")[-1]
    r = session.get(mp4_url, stream=True)
    with open(saveRecPath, 'wb') as f:
        f.write(r.content)
        print("save %s" % saveRecPath, end="\r")
#串接
clips = []
for i in range(len(totalFileList)-1, -1, -1):
    saveRecPath = totalFileList[i].split("/")[-1]
    clipVideo = VideoFileClip(saveRecPath)
    clips.append(clipVideo)
output = concatenate_videoclips(clips)
output.write_videofile(saveFileName)

#砍片段的影片檔
for tar in clips:
    tar.close()
for i in range(len(totalFileList)-1, -1, -1):
    saveRecPath = totalFileList[i].split("/")[-1]
    if os.path.isfile(saveRecPath):
        os.remove(saveRecPath)
        print("remove temp file %s" % saveRecPath, end="\r")
        
session.close()
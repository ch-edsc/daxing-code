from django.db import models
from django.utils import timezone as datetime
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import (
  AbstractBaseUser,
  BaseUserManager,
  PermissionsMixin
)
from app import settings 
import json


class ErrorRecord(models.Model):
  error_function = models.CharField(max_length=255, null=True, blank=True)
  error_message = models.TextField(default='', null=True, blank=True)
  occur_at = models.DateTimeField(default=datetime.now)
  levelname = models.CharField(max_length=255, null=True, blank=True) 
  
class Task(models.Model):
  setting = models.JSONField(default=dict, null=True, blank=True)
  status = models.CharField(default='pending', max_length=255, null=True, blank=True) #pending/running/success/error
  message = models.TextField(default=None, null=True, blank=True)
  timestamp  = models.BigIntegerField(default=0, null=True, blank=True)

class Video(models.Model):
  setting = models.JSONField(default=dict, null=True, blank=True)
  path = models.TextField(default=None, null=True, blank=True)

class Result(models.Model):
  task = models.ForeignKey(
    'Task',
    on_delete=models.SET_NULL,
    null=True,
    blank=True
  )
  video = models.ForeignKey(
    'Video',
    on_delete=models.SET_NULL,
    null=True,
    blank=True
  )
  data = models.JSONField(default=dict, null=True, blank=True)

  
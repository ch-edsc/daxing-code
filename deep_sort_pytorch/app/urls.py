"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from app.views import *

urlpatterns = [
    #path('admin/', admin.site.urls),
    #path('api/merge_report', MergeReportView.as_view(), name='merge-report'),
    path('api/uplode_video', uplode_video.as_view(), name='uplode_video'),
    path('api/get_video_list', get_video_list.as_view(), name='get_video_list'),
    path('api/add_task', add_task.as_view(), name='add_task'),
    path('api/get_task', get_task.as_view(), name='get_task'),
    path('api/get_task_list', get_task_list.as_view(), name='get_task_list'),
    path('api/delete_task', delete_task.as_view(), name='delete_task'),
    path('api/get_result_list', get_result_list.as_view(), name='get_result_list'),
    path('api/get_result', get_result.as_view(), name='get_result'),
    path('api/get_result_video', get_result_video.as_view(), name='get_result_video'),
    path('api/get_result_video_warp', get_result_video_warp.as_view(), name='get_result_video_warp'),
    path('api/get_result_speed', get_result_speed.as_view(), name='get_result_speed'),
    path('api/get_result_car_spacing', get_result_car_spacing.as_view(), name='get_result_car_spacing'),
    path('api/get_result_excel', get_result_excel.as_view(), name='get_result_excel'),
    path('api/get_result_track_maps', get_result_track_maps.as_view(), name='get_result_track_maps'),
    path('api/delete_result', delete_result.as_view(), name='delete_result'),
    path('api/delete_video', delete_video.as_view(), name='delete_video'),

]

from django.apps import AppConfig

class MyAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'
    def ready(self):
        print('MyAppConfig...')
        from app.views import start_schedule
        
        start_schedule()
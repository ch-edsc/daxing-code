
from django.http import HttpResponse
from rest_framework.views import APIView
from django.forms.models import model_to_dict
from django.http import JsonResponse
import json
import os
import sys
from datetime import datetime
from dotenv import load_dotenv

from django.utils.encoding import escape_uri_path
from rest_framework.response import Response
from rest_framework import status
import base64
import io
import xlsxwriter
import csv
import time
import threading
from app import settings
import schedule
from app.models import Task, Video, Result
from minio import Minio
import urllib3

import yolov7_deepsort
load_dotenv()

http_client = urllib3.PoolManager(
    timeout=urllib3.util.Timeout(connect=60, read=3600)  # 連線 60 秒，讀取 1 小時
)

def toStr(value):
    if value is None:
        value = ''
    else:
        value = str(value) # + "\t"
    return value

def toInt(value):
    try:
      value = int(float(value))
    except Exception as e:
      print(f'Exception toInt: {e}, {value}')
      value = 0
    return value

def csv_to_dict(csv_value, key, outputcols=[]):
  dict_value = {}
  no_id_data_value = {}
  col_titles = []
  key_index = -1
  unknow_row_name = 'unknow_'
  unknow_index = 0
  for i, row in enumerate(csv_value):
    if i == 0:
      for j, col in enumerate(row):
        col_titles.append(col)
        if toStr(key) == toStr(col):
          key_index = j
    else:
      if key_index == -1:
        break
      values = {}
      for j, col in enumerate(row):
        if j == len(col_titles):
          break
        if col_titles[j] in outputcols or len(outputcols) == 0:
          values[col_titles[j]] = col
      if row[key_index] != None and row[key_index] != '':
        dict_value[toStr(row[key_index])] = values
      else:
        no_id_data_value[unknow_row_name + str(unknow_index)] = values
        unknow_index += 1
  return dict_value, no_id_data_value

def csv_to_array(csv_value, outputcols=[], skip_void_cols = []):
  array_value = []
  col_titles = []
  for i, row in enumerate(csv_value):
    if i == 0:
      for j, col in enumerate(row):
        col_titles.append(col)
    else:
      values = {}
      for j, col in enumerate(row):
        if j == len(col_titles):
          break
        if col_titles[j] in outputcols or len(outputcols) == 0:
          values[col_titles[j]] = col
      # 20230119 空值過濾
      need_skip = False
      for skip_null in skip_void_cols:
        #print(values)
        if skip_null in values and values[skip_null] == '':
          #print(f'need skip: {values}')
          need_skip = True
          break
      if need_skip:
        continue
      array_value.append(values)
  return array_value

def remove_symbol_in_csv(csv_value):
  outputcsv = []
  for i, row in enumerate(csv_value):
    one_row = []
    for j, col in enumerate(row):
      value = toStr(col)
      if len(value) > 1:
        if value[0] == '=':
          value = value[1:]
      if len(value) > 2:
        if value[0] == '"' and value[-1] == '"':
          value = value[1:-1]
      one_row.append(value)
    outputcsv.append(one_row)
  return outputcsv


def merge_csv(data1_key, data2_key, inputs1, inputs2, ignore_cols=[]):

  output_ab = []
  output_only_a = []
  output_only_b = []
  
  for data1 in inputs1:
    if data1_key in data1 and data1[data1_key] == '':
      output_only_a.append(data1)
  for data2 in inputs2:
    if data2_key in data2 and data2[data2_key] == '':
      output_only_b.append(data2)
  for data1 in inputs1:
    if data1_key not in data1:
      continue
    if data1[data1_key] == '':
      continue
    for data2 in inputs2:
      if data2_key not in data2:
        continue
      if data2[data2_key] == '': 
        continue
      elif data1[data1_key] == data2[data2_key]:
        output = {}
        for key, value in data1.items():
          if key in ignore_cols:
            continue
          output[key] = value
        for key, value in data2.items():
          if key in ignore_cols:
            continue
          output[key] = value
        output_ab.append(output)
        data1['find'] = True
        data2['find'] = True
  for data1 in inputs1:
    if 'find' not in data1:
      output_only_a.append(data1)
  for data2 in inputs2:
    if 'find' not in data2:
      output_only_b.append(data2)
  for values in output_ab:
    if 'find' in values:
      del values['find']
  for values in output_only_a:
    if 'find' in values:
      del values['find']
  for values in output_only_b:
    if 'find' in values:
      del values['find']
  return output_ab, output_only_a, output_only_b

def add_data_to_workbook(data, workbook, worksheetName, cell_format):
  worksheet = workbook.add_worksheet(worksheetName)
  nowRow = 2
  if len(data) == 0:
    return workbook
  cols_len_max = [10] * max(len(data[0]), 100) #欄寬調整用
  
  for i, col in enumerate(data):
    for j, (key, value) in enumerate(col.items()):
      if i == 0 :
        worksheet.write(nowRow-1, j, key, cell_format)
        col_len = len(toStr(key))
        if cols_len_max[j] < col_len:
          cols_len_max[j] = col_len
      worksheet.write(nowRow, j, value, cell_format)
      col_len = len(toStr(value))
      if cols_len_max[j] < col_len:
        cols_len_max[j] = col_len
    nowRow += 1
  worksheet.freeze_panes(2, 0)#凍結頂部列
  for i, col_len in enumerate(cols_len_max):
    worksheet.set_column(i, i, min(int(cols_len_max[i] * 1.5), 120)) # 限制長度上限
  return workbook

def decode_data(data):
  try:
    #print('utf-8')
    return data.decode('utf-8')
  except:
    try:
      #print('cp950')
      return data.decode('cp950')
    except:
      #print('utf16')
      return data.decode('utf16')
    
def csv_reader(content):
  #分隔符號有時 \t 有時 , 總之兩種都試試
  file = io.StringIO(content)
  output = csv.reader(file, delimiter=',')
  #print(len(output[0][0]))
  need_try_else = False
  for i, row in enumerate(output):
    #print(len(row))
    if len(row) == 1:
      need_try_else = True
    break
  if need_try_else:
    #print('need retry..')
    file = io.StringIO(content)
    output = csv.reader(file, delimiter='\t')
  else:
    #這邊還要重讀一次，不然第一行會不見
    file = io.StringIO(content)
    output = csv.reader(file, delimiter=',')
  return output
import traceback
def run_a_task():

  #print('run_a_task...')
  task = Task.objects.filter(status='running').first()
  if task != None:
    print('a task is running...')
    if task.timestamp == None:
      task.status = 'error'
      task.message = 'task無回應'
      task.save()
    else:
      now_time = int(round(datetime.now().timestamp()))
      #print('time')
      #print(now_time - task.timestamp)
      wait_time = 70
      if task.message != None:
        wait_time = 600
      if now_time - task.timestamp > wait_time:
        task.status = 'error'
        task.message = 'task無回應'
        task.save()
    return
  task = Task.objects.filter(status='pending').first()
  if task == None:
    return
  try:
    task.status = 'running'
    task.save()
    video_id = None
    if 'video_id' in task.setting:
      video_id = task.setting['video_id']
    if video_id == None:
      client = Minio(
        settings.MINIO_SERVER,
        access_key=settings.MINIO_ACCESS_KEY,
        secret_key=settings.MINIO_SECRET_KEY,
        secure=False,
        http_client=http_client  # 使用自訂的 HTTP 客戶端
      )
      bucket_name = task.setting['bucket_name']
      object_name = task.setting['object_name']
      video = Video.objects.create()
      video.path = settings.FILE_PATH + f'{video.id}.mp4'
      video.setting = {'camera_no': task.setting['camera_no']}
      video.save()
      video_id = video.id
      task.setting['video_id'] = video.id
      task.message = f'從 minio bucket 下載 video 中 {object_name}'
      task.save()
      client.fget_object(bucket_name, object_name, video.path)
      task.message = None
      task.save()
    video = Video.objects.filter(id=video_id).first()
    task.setting['camera_no'] = video.setting['camera_no']
    task.save()
    yolov7_deepsort.run_for_task(video.path, video.setting['camera_no'], task.id)
    task.status = 'success'
    #print('a')
    task.save()
  except Exception as e:
    '''
    exc_type, exc_obj, exc_tb = sys.exc_info()
    fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    out_txt = f'{exc_type} {toStr(e)} {fname} {exc_tb.tb_lineno}'
    task.status = 'error'
    task.message = str(out_txt)
    task.save()
    '''

    error_trace = traceback.format_exc()

    # 將錯誤資訊記錄到 task.message
    task.status = 'error'
    task.message = error_trace
    task.save()

def run_schedule():
  while True:
    try:
      #print('run_schedule...')
      schedule.run_pending()
      time.sleep(5)
    except Exception as e:
      print(e)

def start_schedule():
  print('start_schedule++')
  tasks = Task.objects.filter(status='running')
  for task in tasks:
    task.status = 'error'
    task.message = 'task無回應'
    task.save()
  schedule.every(5).seconds.do(run_a_task)
  scheduleThread = threading.Thread(target = run_schedule)

  scheduleThread.setDaemon(True)
  scheduleThread.start()
  print('start_schedule--')
  return


class uplode_video(APIView):
  authentication_classes = []
  permission_classes = []
  def post(self, request):
    try:
      print("uplode_video ++")
      video_data = request.body
      camera_no = self.request.query_params.get('camera_no')
      if camera_no == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'need camera_no'})

      if not os.path.isdir(settings.FILE_PATH):
        os.mkdir(settings.FILE_PATH)
      
      video = Video.objects.create()
      video.path = settings.FILE_PATH + f'{video.id}.mp4'
      video.setting = {'camera_no': camera_no}
      video.save()
      with open(video.path, 'wb') as file:
        file.write(video_data)

      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'video_id': video.id, 'camera_no': camera_no})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_video_list(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      print("get_video_list ++")

      videos = Video.objects.all()
      return_data = []
      for video in videos:
        return_data.append(model_to_dict(video))

      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'data': return_data})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})


class get_task_list(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      print("get_task_list ++")

      tasks = Task.objects.all()
      return_data = []
      for task in tasks:
        return_data.append(model_to_dict(task))

      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'data': return_data})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_result_list(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      print("get_result_list ++")

      results = Result.objects.all()
      return_data = []
      for result in results:
        return_data.append(model_to_dict(result))

      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'data': return_data})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_task(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      task_id = self.request.query_params.get('task_id')
      if task_id == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'need task_id'})
      task = Task.objects.filter(id=task_id).first()
      if task == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})

      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'data': model_to_dict(task)})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_result(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      result = None
      task_id = self.request.query_params.get('task_id')
      if task_id != None: 
        task = Task.objects.filter(id=task_id).first()
        if task == None:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
        result = Result.objects.filter(task=task).first()
      result_id = self.request.query_params.get('result_id')
      if result_id != None:
        result = Result.objects.filter(id=result_id).first()

      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})

      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'data': model_to_dict(result)})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})


class get_result_excel(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      task_id = self.request.query_params.get('task_id')
      if task_id != None:
        
        task = Task.objects.filter(id=task_id).first()
        if task == None:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
        result = Result.objects.filter(task=task).first()
      result_id = self.request.query_params.get('result_id')
      if result_id != None:
        result = Result.objects.filter(id=result_id).first()

      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})

      file_path = result.data['excel_path']

      file_content = None
      with open(file_path, 'rb') as file:
        file_content = file.read()
      if file_content == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'excel file found'})

      # 创建 HttpResponse 对象
      response = HttpResponse(file_content, content_type='application/octet-stream')

      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path(f'{task.id}_output.xlsx')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'
      return response
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_result_video(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      task_id = self.request.query_params.get('task_id')
      result = None
      if task_id != None:
        
        task = Task.objects.filter(id=task_id).first()
        if task == None:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
        result = Result.objects.filter(task=task).first()
      result_id = self.request.query_params.get('result_id')
      if result_id != None:
        result = Result.objects.filter(id=result_id).first()

      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})

      file_path = result.data['save_video_path']

      file_content = None
      with open(file_path, 'rb') as file:
        file_content = file.read()
      if file_content == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'video file found'})

      # 创建 HttpResponse 对象
      response = HttpResponse(file_content, content_type='application/octet-stream')

      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path(f'{task.id}_output.mp4')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'
      return response
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_result_video_warp(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      task_id = self.request.query_params.get('task_id')
      result = None
      if task_id != None:
        
        task = Task.objects.filter(id=task_id).first()
        if task == None:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
        result = Result.objects.filter(task=task).first()
      result_id = self.request.query_params.get('result_id')
      if result_id != None:
        result = Result.objects.filter(id=result_id).first()

      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})

      file_path = result.data['save_video_path2']

      file_content = None
      with open(file_path, 'rb') as file:
        file_content = file.read()
      if file_content == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'video file found'})

      # 创建 HttpResponse 对象
      response = HttpResponse(file_content, content_type='application/octet-stream')

      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path(f'{task.id}_output.mp4')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'
      return response
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_result_speed(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      task_id = self.request.query_params.get('task_id')
      if task_id != None:
        
        task = Task.objects.filter(id=task_id).first()
        if task == None:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
        result = Result.objects.filter(task=task).first()
      result_id = self.request.query_params.get('result_id')
      if result_id != None:
        result = Result.objects.filter(id=result_id).first()

      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})
      task = result.task

      file_path = settings.FILE_PATH + f'{task.id}' + 'AvgSpeed.csv'

      file_content = None
      with open(file_path, 'rb') as file:
        file_content = file.read()
      if file_content == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'file not found'})

      # 创建 HttpResponse 对象
      response = HttpResponse(file_content, content_type='application/octet-stream')

      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path(f'{task.id}_output.xlsx')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'
      return response
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class get_result_car_spacing(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      task_id = self.request.query_params.get('task_id')
      if task_id != None:
        
        task = Task.objects.filter(id=task_id).first()
        if task == None:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
        result = Result.objects.filter(task=task).first()
      result_id = self.request.query_params.get('result_id')
      if result_id != None:
        result = Result.objects.filter(id=result_id).first()

      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})
      task = result.task

      file_path = settings.FILE_PATH + f'{task.id}' + 'CarSpacing.csv'

      file_content = None
      with open(file_path, 'rb') as file:
        file_content = file.read()
      if file_content == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'file not found'})

      # 创建 HttpResponse 对象
      response = HttpResponse(file_content, content_type='application/octet-stream')

      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path(f'{task.id}_output.xlsx')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'
      return response
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})



class get_result_track_maps(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      task_id = self.request.query_params.get('task_id')
      if task_id != None: 
        task = Task.objects.filter(id=task_id).first()
        if task == None:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
        result = Result.objects.filter(task=task).first()
      result_id = self.request.query_params.get('result_id')
      if result_id != None:
        result = Result.objects.filter(id=result_id).first()

      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})
      task = result.task

      start = self.request.query_params.get('start')
      if start == None:
        start = 0
      get_num = self.request.query_params.get('get_num')
      if get_num == None:
        get_num = 10
      start = int(start)
      get_num = int(get_num)
      if 'track_map_count' not in result.data:
        return Response(status=status.HTTP_200_OK, data=[])
      end = result.data['track_map_count']
      if start > end:
        start = end
      get_end = start + get_num
      if get_end > end:
        get_end = end
      return_data = []

      for i in range(start, get_end, 1):
        file_path = result.data['track_map_path'] + f'{i}.png'
        with open(file_path, 'rb') as image_file:
          #return_data.append(image_file.read())
          #return_data[f'{i}.png'] = image_file.read()
          #return_data[f'{i}.png'] = base64.b64encode(image_file.read()).decode('utf-8')
          return_data.append(base64.b64encode(image_file.read()).decode('utf-8'))

      #return JsonResponse(return_data)
      return Response(status=status.HTTP_200_OK, data=return_data)
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class add_task(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      print("add_task ++")
      video_id = self.request.query_params.get('video_id')
      bucket_name = None
      object_name = None
      camera_no = None
      if video_id == None:
        #return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'need video_id'})
        bucket_name = self.request.query_params.get('bucket_name')
        object_name = self.request.query_params.get('object_name')
        camera_no = self.request.query_params.get('camera_no')
      road_mode = self.request.query_params.get('road_mode') # straight cross
      if road_mode == None:
        road_mode = 'cross'
      video = None
      if video_id != None:
        video = Video.objects.filter(id=video_id).first()
      if video == None and (object_name == None or bucket_name == None or camera_no == None):
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'video not found, or need bucket_name, object_name and camera_no'})
      task = Task.objects.create()
      task.setting = {
        'video_id': None,
        'bucket_name': None,
        'object_name': None,
        'camera_no': None,
        'road_mode': road_mode,
        'srcPoints': [[129, 97], [38, 478], [544, 352], [222, 95]],
        'tarW': 176,
        'tarH': 480,
        'warpPixelRate': 0.09009,
        'road_nums': 4,
        'road_in_points': [[394,194], [25,264], [171,175] ,[580,289]],
        #'road_out_points': [[430,205], [43,204], [240,175] ,[419,420]],
        'videoDate': '2023-11-29',
        'videoStartTime': '08:00:00',
        'videoEndTime': '09:00:00',
        'road_1_name': '路口1',
        'road_2_name': '路口2',
        'road_3_name': '路口3',
        'road_4_name': '路口4',
        'debug_run_sec': None,
      }
      debug_run_sec = self.request.query_params.get('debug_run_sec')
      if debug_run_sec != None:
        task.setting['debug_run_sec'] = int(debug_run_sec)
      srcPoints = self.request.query_params.get('srcPoints') # 左上x,y ，左下 x,y ，右下 x,y ，右上 x,y
      tarW = self.request.query_params.get('tarW')
      tarH = self.request.query_params.get('tarH')
      warpPixelRate = self.request.query_params.get('warpPixelRate')
      road_in_points = self.request.query_params.get('road_points')
      if road_in_points == None:
        road_in_points = self.request.query_params.get('road_in_points')
      #20250111 現在沒吃 road_out_points 了
      #road_out_points = self.request.query_params.get('road_out_points')
      videoDate = self.request.query_params.get('videoDate')
      videoStartTime = self.request.query_params.get('videoStartTime')
      videoEndTime = self.request.query_params.get('videoEndTime')
      if videoDate != None:
        task.setting['videoDate'] = videoDate
      if videoStartTime != None:
        task.setting['videoStartTime'] = videoStartTime
      if videoEndTime != None:
        task.setting['videoEndTime'] = videoEndTime

      #現在說要支援N叉道，預設4叉
      road_nums = 4
      if self.request.query_params.get('road_nums') != None:
        road_nums = int(self.request.query_params.get('road_nums'))
      task.setting['road_nums'] = road_nums
      for i in range(road_nums):
        road_no = i+1
        tar_road_name = f'road_{road_no}_name'
        if self.request.query_params.get(tar_road_name) != None:
          task.setting[tar_road_name] = self.request.query_params.get(tar_road_name)


      '''
      if self.request.query_params.get('road_1_name') != None:
        task.setting['road_1_name'] = self.request.query_params.get('road_1_name')
      if self.request.query_params.get('road_2_name') != None:
        task.setting['road_2_name'] = self.request.query_params.get('road_2_name')
      if self.request.query_params.get('road_3_name') != None:
        task.setting['road_3_name'] = self.request.query_params.get('road_3_name')
      if self.request.query_params.get('road_4_name') != None:
        task.setting['road_4_name'] = self.request.query_params.get('road_4_name')
      '''
      if srcPoints != None:
        plist = srcPoints.split(',')
        if len(plist) == 8:
          task.setting['srcPoints'] = [[int(plist[0]), int(plist[1])], [int(plist[2]), int(plist[3])], [int(plist[4]), int(plist[5])], [int(plist[6]), int(plist[7])]]
      
      if road_in_points != None:
        plist = road_in_points.split(',')
        list_road_in_points = []
        for i in range(road_nums):
          list_road_in_points.append([0, 0])
        for i in range(road_nums*2):
          list_road_in_points[i//2][i%2] = int(plist[i])
        task.setting['road_in_points'] = list_road_in_points
        #現在要支援N叉道
        #if len(plist) == 8:
        #  task.setting['road_in_points'] = [[int(plist[0]), int(plist[1])], [int(plist[2]), int(plist[3])], [int(plist[4]), int(plist[5])], [int(plist[6]), int(plist[7])]]
      #20250111 現在沒吃 road_out_points 了
      #if road_out_points != None:
      #  plist = road_out_points.split(',')
      #  if len(plist) == 8:
      #    task.setting['road_out_points'] = [[int(plist[0]), int(plist[1])], [int(plist[2]), int(plist[3])], [int(plist[4]), int(plist[5])], [int(plist[6]), int(plist[7])]]
      if tarW != None:
        task.setting['tarW'] = int(tarW)
      if tarH != None:
        task.setting['tarH'] = int(tarH)
      if warpPixelRate != None:
        task.setting['warpPixelRate'] = float(warpPixelRate)

      if video != None:
        task.setting['video_id'] = video.id
      else:
        task.setting['bucket_name'] = bucket_name
        task.setting['object_name'] = object_name
        task.setting['camera_no'] = camera_no

      task.timestamp = int(round(datetime.now().timestamp()))
      task.save()
      #yolov7_deepsort.run_for_task(video.path, video.setting['camera_no'], task.id)

      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'data': model_to_dict(task)})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class delete_task(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      print("delete_task ++")
      task_id = self.request.query_params.get('task_id')
      if task_id == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'need task_id'})
      task = Task.objects.filter(id=task_id).first()
      if task == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'task not found'})
      task.delete()
      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK'})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class delete_result(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      print("delete_result ++")
      result_id = self.request.query_params.get('result_id')
      if result_id == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'need result_id'})
      result = Result.objects.filter(id=result_id).first()
      if result == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'result not found'})
      filename = result.data['excel_path']
      if os.path.exists(filename):
        os.remove(filename)
      if 'save_video_path' in result.data:
        filename = result.data['save_video_path']
        if os.path.exists(filename):
          os.remove(filename)
      result.delete()
      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK'})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})

class delete_video(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      video_id = self.request.query_params.get('video_id')
      if video_id == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'need video_id'})
      video = Video.objects.filter(id=video_id).first()
      if video == None:
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': 'video not found'})
      filename = settings.FILE_PATH + f'{video.id}.mp4'
      if os.path.exists(filename):
        os.remove(filename)
      video.delete()
      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK'})
  
    except Exception as e:
      print(e)
      return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'ERROR', 'message': toStr(e)})
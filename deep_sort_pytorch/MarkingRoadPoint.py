import cv2
import sys


# python MarkingRoadPoint.py "_NO_2_G11_2022_07_12_hour_2022_07_12_07_00_00.mp4"
srcH = 0
srcW = 0
tarH = 0
tarW = 0

cameraNo = 0

markPointsList = []

frame = None
showFrame = None
# Create a VideoCapture object and read from input file
videoPath = sys.argv[1]

# _NO_2_G11_2022_07_12_hour_2022_07_12_07_00_00.mp4 
# _NO_7_2022_07_12_hour_2022_07_12_07_00_00.mp4
filePathStrs = videoPath.split('/')[-1].split('\\')[-1].split('_')

if len(filePathStrs) > 2:
  cameraNo = int(filePathStrs[2])
print(f'cameraNo = {cameraNo}')

roadPointDataPath = "data/road_camera" + str(cameraNo) + ".csv"
roadEndPointDataPath = "data/road_end_camera" + str(cameraNo) + ".csv"


try:
    cap = cv2.VideoCapture(videoPath)
except FileNotFoundError:
    print("Failed to open video file")
    sys.exit(1)
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video  file")

def clickToMark(event, x, y, flags, params):
    global markPointsList
    global showFrame
    global frame
    if event == cv2.EVENT_LBUTTONDOWN:
        if len(markPointsList) < 8:
          markPointsList.append((x, y))
        showMessage()
        
        
       
class readKeyPressed():
    def __init__(self, rawKeyPressed):
        self.rawKeyPressed = rawKeyPressed
    def __enter__(self):
        return self.rawKeyPressed
    def __exit__(self, exc_type, exc_value, traceback):
        pass

def showMessage():
    global markPointsList
    global showFrame
    global frame  
    showFrame = frame.copy()
    for i, p in enumerate(markPointsList):
        if i < 4:
            cv2.circle(showFrame, p, 3, (255, 0, 255), -1)
        else:
            cv2.circle(showFrame, p, 3, (0, 255, 255), -1)
    if len(markPointsList) < 8:
      cv2.putText(showFrame, 'please mark point:', (10, 20), cv2.FONT_HERSHEY_PLAIN, 1.2, [0, 255, 0], 1)
      index = len(markPointsList)
      if index > 7:
          index = 7
      cv2.putText(showFrame, f'{roadtexts[index]}', (220, 20), cv2.FONT_HERSHEY_PLAIN, 1.2, [0, 255, 255], 2)
      cv2.putText(showFrame, 'you can press "r" to reset all points', (10, 50), cv2.FONT_HERSHEY_PLAIN, 1, [255, 255, 0], 1)
      cv2.putText(showFrame, 'you can press "b" to back one point', (10, 80), cv2.FONT_HERSHEY_PLAIN, 1, [255, 255, 0], 1)
      cv2.putText(showFrame, 'you can press "f" to next frame', (10, 110), cv2.FONT_HERSHEY_PLAIN, 1, [255, 255, 0], 1)
      cv2.putText(showFrame, 'you can press "g" to next 10 frame', (10, 140), cv2.FONT_HERSHEY_PLAIN, 1, [255, 255, 0], 1)
      cv2.imshow("Frame", showFrame)
    else:
      cv2.putText(showFrame, 'you can press "s" to save points and exit', (10, 20), cv2.FONT_HERSHEY_PLAIN, 1.2, [255, 255, 0], 2)
      cv2.imshow("Frame", showFrame)

    
roadtexts = ['Right In', 'Left In', 'Up In', 'Down In', 'Right Out', 'Left Out', 'Up Out', 'Down Out']

needKeepFrame = False
while(cap.isOpened()):
    if not needKeepFrame:
        ret, frame = cap.read()
    needKeepFrame = False
    if ret == True:
        # Display the resulting frame
        showMessage()
        cv2.setMouseCallback("Frame", clickToMark)
        
        # Press Q on keyboard to exit
        with readKeyPressed(cv2.waitKey(0) & 0xFF) as inputWhenPlaying:
            if inputWhenPlaying == ord('q') or inputWhenPlaying == ord('Q'):
                cv2.destroyAllWindows()
                sys.exit(1)
            elif inputWhenPlaying == ord('f') or inputWhenPlaying == ord('F'):
                continue
            elif inputWhenPlaying == ord('g') or inputWhenPlaying == ord('G'):
                for i in range(9):
                    cap.read()
                continue
            elif inputWhenPlaying == ord('r') or inputWhenPlaying == ord('R'):
                markPointsList = []
                needKeepFrame = True
                continue
            elif inputWhenPlaying == ord('b') or inputWhenPlaying == ord('B'):
                if len(markPointsList) > 0:
                  del(markPointsList[-1])
                needKeepFrame = True
                continue
            elif inputWhenPlaying == ord('s') or inputWhenPlaying == ord('S'):
                if len(markPointsList) < 8:
                  needKeepFrame = True
                  continue
                else:
                  file = open(roadPointDataPath, 'w')
                  file.write(str(markPointsList[0][0]) + "," + str(markPointsList[0][1]) + "\n")
                  file.write(str(markPointsList[1][0]) + "," + str(markPointsList[1][1]) + "\n")
                  file.write(str(markPointsList[2][0]) + "," + str(markPointsList[2][1]) + "\n")
                  file.write(str(markPointsList[3][0]) + "," + str(markPointsList[3][1]) + "\n")
                  file.close()
                  file = open(roadEndPointDataPath, 'w')
                  file.write(str(markPointsList[4][0]) + "," + str(markPointsList[4][1]) + "\n")
                  file.write(str(markPointsList[5][0]) + "," + str(markPointsList[5][1]) + "\n")
                  file.write(str(markPointsList[6][0]) + "," + str(markPointsList[6][1]) + "\n")
                  file.write(str(markPointsList[7][0]) + "," + str(markPointsList[7][1]) + "\n")
                  file.close()
                  cv2.destroyAllWindows()
                  sys.exit(1)
            else:
                needKeepFrame = True
    # Break the loop
    else: 
        break
分析來源為15分鐘影片，每個分析結果為該15分鐘的統計量，依影像內容

場景(單一道路路段、十字路口)，會有不同的統計量。

 

格式如下：

{"cameraNo":3, "vedioStartTime": "2021-09-11 09:00:00", "vedioLength":900, "type": 2,

"standardCounting" : {

"bigCarNum": 10, "smallCarNum": 80, "motorbikeNum": 100, "peopleNum": 5

},

"turnCounting": {

"straight_R": {

"bigCar": { "straight": 2, "turnRight": 3, "turnLeft": 0 },

"smallCar": { "straight": 33, "turnRight": 44, "turnLeft": 12 },

"motorbike": { "straight": 80, "turnRight": 20, "turnLeft": 0, "hookTurn": 5}

},

"straight_L": {

"bigCar": { "straight": 2, "turnRight": 3, "turnLeft": 0 },

"smallCar": { "straight": 33, "turnRight": 44, "turnLeft": 12 },

"motorbike": { "straight": 80, "turnRight": 20, "turnLeft": 0, "hookTurn": 5}

},

"cross_U": {

"bigCar": { "straight": 3, "turnRight": 1, "turnLeft": 1 },

"smallCar": { "straight": 51, "turnRight": 33, "turnLeft": 22 },

"motorbike": { "straight": 23, "turnRight": 10, "turnLeft": 0, "hookTurn": 7}

},

"cross_D": {

"bigCar": { "straight": 3, "turnRight": 1, "turnLeft": 1 },

"smallCar": { "straight": 51, "turnRight": 33, "turnLeft": 22 },

"motorbike": { "straight": 23, "turnRight": 10, "turnLeft": 0, "hookTurn": 7}

}

},

"accelerationCounting": {

"bigCar": { "maxAccSpeed": 30.5, "minAccSpeed": 18.1, "75thPercentileAccSpeed": 26.2, "avgAccSpeed": 23.1 },

"smallCar": {  "maxAccSpeed": 35.5, "minAccSpeed": 19.2, "75thPercentileAccSpeed": 24, "avgAccSpeed": 23},

"motorbike": { "maxAccSpeed": 40.3, "minAccSpeed": 20.2, "75thPercentileAccSpeed": 35.4, "avgAccSpeed": 32.1}

},

"extraCounting": {

}

}

 

1.共通統計量：

cameraNo(攝影機編號)，vedioStartTime(影像拍攝時間)：目前無法知道來源，但可以指定來源影片的檔名格式，例如camera3_20210911090000.mp4，然後從檔名抓值。

type：路段類型，目前定義1=直線路段，2=十字路口

vedioLength：影片長度(單位為秒)，雖然現在影片定義為15分鐘1段，但不確定以後是否會延展，所以記錄下來。

 

2.

standardCounting: 一般數量統計，大小車/機車/行人各有幾台

bigCarNum 大車數量，smallCarNum小車數量，motorbikeNum機車數量，peopleNum行人數量

 

3.

turnCounting: 轉彎量統計，只有在路段類型為十字路口才會有。裡面轉彎量依路口分開統計straight_R:右邊進，straight_L:左，cross_U:上，cross_D:下，
然後各路口依大車/小車/機車分開統計。straight直行車，turnRight右轉，turnLeft左轉，hookTurn兩段式左轉

 

4.

accelerationCounting: 加速度相關統計量，只有在路段類型為直線路段才會有。裡面的值也依大小機車分開統計。

maxAccSpeed 最大加速度，minAccSpeed最小加速度，75thPercentileAccSpeed第75百分位加速度，avgAccSpeed依[實驗設計v2.pdf]定義的平均加速度

 

5.

extraCounting: 未來如果有要擴充的統計量且不屬於加速度或轉彎量相關統計的(例如，切換車道次數)可以放這邊
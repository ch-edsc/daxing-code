import cv2
import numpy as np
import sys


# python MarkingTransformingReference.py "影片檔位置"
# python MarkingTransformingReference.py "data/_NO_7_2022_03_29_clip_2022_03_29_17_00.mp4"
srcH = 0
srcW = 0
tarH = 0
tarW = 0


# Create a VideoCapture object and read from input file
videoPath = sys.argv[1]
try:
    cap = cv2.VideoCapture(videoPath)
except FileNotFoundError:
    print("Failed to open video file")
    sys.exit(1)
# Check if camera opened successfully
if (cap.isOpened()== False): 
  print("Error opening video  file")
def clickToMark(event, x, y, flags, params):
    global markPointsList
    if event == cv2.EVENT_LBUTTONDOWN:
        print("# Marked at: ", x, y)
        markPointsList.append((x, y))
        cv2.circle(frame, (x, y), 10, (0, 0, 255), -1)
        cv2.imshow("Frame", frame)
def clickToMarkWarpped(event, x, y, flags, params):
    global markWarppedPointsList
    global warppedframe
    if event == cv2.EVENT_LBUTTONDOWN:
        print("# Marked at: ", x, y)
        markWarppedPointsList.append((x, y))
        cv2.circle(warppedframe, (x, y), 10, (0, 0, 255), -1)
        cv2.imshow("Warpped", warppedframe)
        if len(markWarppedPointsList) == 2:
            pixelWithinTenMeter = max(abs(markWarppedPointsList[0][1]-markWarppedPointsList[1][1]),
                                        abs(markWarppedPointsList[0][0]-markWarppedPointsList[1][0]))
            print("warpPixelRate = %f" % (10/pixelWithinTenMeter))            
def frameTransformationDirectionIdentifier():
    global srcH, srcW, tarH, tarW, markPointsList
    if srcH == 0 and srcW == 0:
        srcH = frame.shape[0]
        srcW = frame.shape[1]
        if len(markPointsList) == 4:
            slopeRLDifference = abs(((markPointsList[0][1]-markPointsList[1][1])/(markPointsList[0][0]-markPointsList[1][0])) - ((markPointsList[3][1]-markPointsList[2][1])/(markPointsList[3][0]-markPointsList[2][0])))
            slopeUDDifference = abs(((markPointsList[0][1]-markPointsList[3][1])/(markPointsList[0][0]-markPointsList[3][0])) - ((markPointsList[1][1]-markPointsList[2][1])/(markPointsList[1][0]-markPointsList[2][0])))
            if slopeRLDifference > slopeUDDifference:
                tarH = srcH
                tarW = srcW/4
            else:
                tarH = srcH/4
                tarW = srcW
    if tarH == 0 and tarW == 0:
        if len(markPointsList) == 4:
            slopeRLDifference = abs(((markPointsList[0][1]-markPointsList[1][1])/(markPointsList[0][0]-markPointsList[1][0])) - ((markPointsList[3][1]-markPointsList[2][1])/(markPointsList[3][0]-markPointsList[2][0])))
            slopeUDDifference = abs(((markPointsList[0][1]-markPointsList[3][1])/(markPointsList[0][0]-markPointsList[3][0])) - ((markPointsList[1][1]-markPointsList[2][1])/(markPointsList[1][0]-markPointsList[2][0])))
            if slopeRLDifference > slopeUDDifference:
                tarH = srcH
                tarW = srcW/4
            else:
                tarH = srcH/4
                tarW = srcW
class readKeyPressed():
    def __init__(self, rawKeyPressed):
        self.rawKeyPressed = rawKeyPressed
    def __enter__(self):
        return self.rawKeyPressed
    def __exit__(self, exc_type, exc_value, traceback):
        pass
while(cap.isOpened()):
    global markWarppedPointsList
    global markPointsList
    markWarppedPointsList = []
    # Capture frame-by-frame
    ret, frame = cap.read()
    if ret == True:
        # Display the resulting frame
        markPointsList = []
        cv2.imshow('Frame', frame)
        cv2.setMouseCallback("Frame", clickToMark)
        
        print("# srcH = ", srcH, "srcW = ", srcW, "tarH = ", tarH, "tarW = ", tarW)
        # Press Q on keyboard to exit
        with readKeyPressed(cv2.waitKey(0) & 0xFF) as inputWhenPlaying:
            if inputWhenPlaying == ord('q') or inputWhenPlaying == ord('Q'):
                sys.exit(1)
            elif inputWhenPlaying == ord('f') or inputWhenPlaying == ord('F'):
                continue
            elif inputWhenPlaying == ord('s') or inputWhenPlaying == ord('S'):
                global warppedframe
                frameTransformationDirectionIdentifier()
                markWarppedPointsList = []
                srcPoints = np.array([markPointsList[0], markPointsList[1], markPointsList[2], markPointsList[3]]).astype(np.float32)
                tarPoints = np.array([[0, 0], [0, tarH], [tarW, tarH], [tarW, 0]]).astype(np.float32)
                print("tarW = %d" % (tarW))
                print("tarH = %d" % (tarH))
                print("srcPoints = np.array([[%d, %d], [%d, %d], [%d, %d], [%d, %d]]).astype(np.float32)" % (srcPoints[0][0], srcPoints[0][1], srcPoints[1][0], srcPoints[1][1], srcPoints[2][0], srcPoints[2][1], srcPoints[3][0], srcPoints[3][1]))
                
                warpM = cv2.getPerspectiveTransform(srcPoints, tarPoints)
                warppedframe = cv2.warpPerspective(frame, warpM, (int(tarW), int(tarH)))
                cv2.imshow("Warpped", warppedframe)
                cv2.setMouseCallback("Warpped", clickToMarkWarpped)
                

    # Break the loop
    else: 
        break
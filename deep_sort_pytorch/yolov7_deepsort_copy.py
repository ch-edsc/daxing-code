from builtins import id
import csv
import io
import os
import pickle
from xml.dom import INDEX_SIZE_ERR
import cv2
import time
import argparse
import torch
import warnings
import numpy as np
import sys
import json
import copy
import math
import heapq

from detector import build_yolov7_detector

from deep_sort import build_tracker
from utils.draw import draw_boxes, draw_boxes2, compute_color_for_labels
from utils.parser import get_config
from utils.log import get_logger
from utils.io import write_results

import sqlite3
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from numpy import mean, nan
import tensorflow as tf

#20211123輸出excel
from openpyxl import load_workbook
from app.models import Task, Video, Result
#20231007 YOLOV8 用
#from ultralytics import YOLO
from datetime import datetime
from app import settings

excel_format_file_path = "data/output_format.xlsx"



# cd C:\Users\leo_1\Documents\GitHub\daxing-code\deep_sort_pytorch
# python yolov7_deepsort.py "D:/cctv/0323/camera_1_2022-03-23-08-00-00.mp4"
# python yolov7_deepsort.py "_NO_2_G11_2022_07_12_hour_2022_07_12_07_00_00.mp4"
# python yolov7_deepsort.py "_NO_2_G11_2022_07_12_hour_2022_07_12_17_00_00.mp4"

# 20220720關於 deepsort模型測試
# 原本模型 https://github.com/ZQPei/deep_sort_pytorch 放在 https://drive.google.com/drive/folders/1xhG0kRH1EX5B9_Iz8gQJb7UNnn_riXi6
# configs/deep_sort.yaml 設定成 "./deep_sort/deep/checkpoint/ckpt.t7" ，並把檔案放到對應位置
# size mismatch for classifier.4.bias: copying a param with shape torch.Size([751]) from checkpoint, the shape in current model is torch.Size([2377]).
# deep_sort_pytorch\deep_sort\deep\feature_extractor.py 的 self.net = Net(num_classes=2377, reid=True) 要依模型的分類數量把2377更改為751


#coco 80 類 大車 id 5 ，小車 2 ，機車3
#自行訓練的 大車 id 1 ，小車 2 ，機車3，行人4

IS_DAXING_CLASS = False
BigCarID = 5
ExtraBigCarID = 7 #公車卡車都是大車
SmallCarID = 2
MotoBikeID = 3
PeopleID = 0

#IS_DAXING_CLASS = True
#BigCarID = 1
#SmallCarID = 2
#MotoBikeID = 3
#PeopleID = 4

video_day = 20220329 #發現camera7在20220416前後，影像不同，加一個依照日期，切換道路投影轉換設定值用的變數


#20220225 產生座標參考點需要跑 track_create_road_point.ipynb
#但 track_create_road_point.ipynb 需要車輛路徑紀錄 data/tracks%d.csv 要跑過這邊才會有
#因此當座標參考點的檔案不存在時，預設使用camera3的值
#20220521 同一影像 road_points 共用就好，不需要每個car都建立，搬到global
road_points = [[698.97437, 182.46745], 
                    [409.28937,538.0], 
                    [5.2457047,178.84816], 
                    [699.0,273.52292]]

road_end_points = [[697.86664,232.1254], 
                        [5.0,407.64438], 
                        [5.669643,140.2039], 
                        [698.0,414.52475 ]]
minStartX = 0
minStartY = 0
maxStartX = 0
maxStartY = 0
minEndX = 0
minEndY = 0
maxEndX = 0
maxEndY = 0
'''
20210814
接下來主要任務有
1. 對deepsort tracking方式改良
2. 對轉彎車/直行車判斷改良
3. 兩段式左轉的判斷

這週先做[對轉彎車/直行車判斷改良]
結構設計：
定義物件 Car，紀錄車種/出現在每個frame的座標點
然後寫個method用座標點判定車子的行為

20210821
依上周開會討論，要把轉彎車/直行車判斷改成非監督式學習(Kmean/AgglomerativeClustering之類的)
首先需要有把整批data記錄下來的機制/讀回的機制
然後要把各筆軌跡資料的長度轉成相同的機制
然後用sklearn跑Clustering
'''

'''
20231007
增加 yoloV8 支援，
這版本的 yolo 有被包成 library ，然後要在python 3.8 以上環境跑
要先
pip install ultralytics


activate python_310
d:
cd D:/Github/daxing-code/deep_sort_pytorch
python yolov7_deepsort.py D:/FFOutput/_NO_106_G10_2023_09_01_hour_2023_09_01_07_00_00.mp4
python yolov7_deepsort.py D:/FFOutput/_NO_106_G10_2023_09_01_hour_2023_09_01_08_00_00.mp4
python yolov7_deepsort.py D:/FFOutput/_NO_107_G10_2023_09_01_hour_2023_09_01_07_00_00.mp4
python yolov7_deepsort.py D:/FFOutput/_NO_107_G10_2023_09_01_hour_2023_09_01_08_00_00.mp4

'''

#output用的json
jsonData = { }

#trunGate = 10
roadPointDataPath = "" #"data/road_camera3.csv"
roadEndPointDataPath = ""

#trunGateDataPath = ""#"data/turn_camera3.csv" #"data/turn_setting_camera_3.txt"
saveTrackDataPath = ""#"data/tracks3.csv"
saveTrackDataWithLabelPath = ""#"data/tracks3_with_label.csv"
videoStartTime_ori = ""

cameraNo = 0 # 預設值0，運行時會從讀入影片的檔名抓
#trunGate = [] # 運行時會從 trunGateDataPath 抓

#20220310 試跑30 fps的影片，跑太久...加個debug用功能...每5個畫面讀1次
debug_skip_mode = False
#開啟後對十字路口影像儲存個別車輛軌跡...注意這個很吃硬碟空間，15分鐘影像大約3GB
debug_draw_track = True

#開啟後影片只跑前面n秒
debug_test_n_sec = False
debug_test_secs = 5 #60*15

#開啟後輸出車間距結果圖
debug_draw_car_spacing = True

#跑1小時的影片輸出太吃硬碟空間(3月29的camera7，跑10分鐘就輸出3GB，1小時就18GB，吃不消)，加個開關切換是否輸出影片
debug_output_video = True
debug_save_fomat_is_mp4 = True #存影片 True時存mp4，否則存avi

#開啟後輸出軌跡csv -> 軌跡分群會用到這個檔案
debug_save_car_tracking = False

#開啟後測試車道分離功能
test_split_road = False

#魚眼測試
test_fish_eye = False

#是否進行遮檔車 reMatch
debug_reMatch = False

#是否用熱區抓轉向量
debug_turn_hot_area = False
debug_turn_with_rl = False #是否用CNN抓轉向量
CNN_MODEL_PATH = 'model/model_camera2.h5'

#20231008 增加 YOLOv8 支援
YOLO_VERSION = 'V7'  # 目前吃 'V7' 或 'V8'
#跑 yolov8n.pt 的話1秒大約可以28個 frame ， yolov8x.pt 1秒大約12個 frame
#yolov7目測準確度大約與 yolov8s.pt 差不多
V8_MODEL = 'yolov8x.pt' # yolov8n.pt yolov8s.pt yolov8m.pt yolov8l.pt yolov8x.pt

mat_in = [None, None, None, None]
mat_out = [None, None, None, None]

def getXcYc(track):
    xc = track[0] + track[2] // 2
    yc = track[1] + track[3]
    return xc, yc, track[4]

#20220706魚眼校正測試用function，來源 https://www.codeleading.com/article/38442757719/ 
def undistort(src,r):
    R = 2*r
    Pi = np.pi
    dst = np.zeros((R, R, 3))
    src_h, src_w, _ = src.shape

    x0, y0 = src_w//2, src_h//2

    range_arr = np.array([range(R)])
 
    theta = Pi - (Pi/R)*(range_arr.T)
    temp_theta = np.tan(theta)**2
 
    phi = Pi - (Pi/R)*range_arr
    temp_phi = np.tan(phi)**2

    tempu = r/(temp_phi + 1 + temp_phi/temp_theta)**0.5
    tempv = r/(temp_theta + 1 + temp_theta/temp_phi)**0.5
    flag = np.array([-1] * r + [1] * r)
    u = x0 + tempu * flag + 0.5
    v = y0 + tempv * np.array([flag]).T + 0.5

    u[u<0]=0
    u[u>(src_w-1)] = src_w-1
    v[v<0]=0
    v[v>(src_h-1)] = src_h-1

    dst[:, :, :] = src[v.astype(int),u.astype(int)]
    return dst
 
def readSetting(args):
    #global trunGate
    global cameraNo
    global roadPointDataPath
    global roadEndPointDataPath
    global saveTrackDataPath
    global saveTrackDataWithLabelPath
    global videoStartTime_ori
    global jsonData
    global video_day
    
    global road_points
    global road_end_points
    global minStartX
    global minStartY
    global maxStartX
    global maxStartY
    global minEndX
    global minEndY
    global maxEndX
    global maxEndY
    
    videoPath = args.VIDEO_PATH
    
    #20220310 新影片的編號與十字路口不太一樣
    # 十字路口 1, 2, 10, 12
    # 直線 3, 4, 5, 6, 7, 8, 9, 11, 13
    # 6的十字路口區域太小，算它直線
    # 新檔名格式 _NO_1_G11_2022_03_07_clip_2022_03_07_08_30.mp4
    # 以前格式 camera_7_2021-12-28-09-00-00.mp4
    # 讓它兩種都支援
    # 還有一個沒有G10的... _NO_7_2022_03_08_clip_2022_03_08_08_00.mp4
    
    # camera, 7, 2021-12-28-09-00-00.mp4
    # , NO, 1, G11, 2022, 03, 07, clip, 2022, 03, 07, 08, 30.mp4
    #
    
    
    temp = videoPath.split("\\")[-1].split("/")[-1].split("_")
    
    videoNameMode = 1
    if len(temp) >= 10:
        videoNameMode = 2
    # 20230929 新的檔名格式： 6八德路松山路_NAB_07-09.mp4 6八德路松山路_NCD_07-09.mp4
    if videoPath.find('camera') < 0 and videoPath.find('_NO') < 0:
        videoNameMode = 3
    
    if videoNameMode == 1:
        # camera_7_2021-12-28-09-00-00.mp4
        cameraNo = int(temp[-2])
        videoStartTime_ori = temp[-1].split(".")[0]
        videoStartTime = list(videoStartTime_ori)
        videoStartTime[10] = " "
        videoStartTime[13] = ":"
        videoStartTime[16] = ":"
        videoStartTime = "".join(videoStartTime)
    elif videoNameMode == 2:
        # _NO_1_G11_2022_03_07_clip_2022_03_07_08_30.mp4
        # _NO_7_2022_03_08_clip_2022_03_08_08_00.mp4
        # _NO_7_2022_07_12_hour_2022_07_12_07_00_00.mp4
        cameraNo = int(temp[2])
        videoStartTime_ori = "%s-%s-%s-%s-%s-00" % (temp[-6], temp[-5], temp[-4], temp[-3], temp[-2].split(".")[0])
        videoStartTime = list(videoStartTime_ori)
        videoStartTime[10] = " "
        videoStartTime[13] = ":"
        videoStartTime[16] = ":"
        videoStartTime = "".join(videoStartTime)
        video_day = int(temp[-6]) * 10000 + int(temp[-5]) * 100 + int(temp[-4])
        #print("video_day: ")
        #print(int(temp[-6]))
        #print(int(temp[-5]))
        #print(int(temp[-4]))
        #print(video_day)
    elif videoNameMode == 3:
        # 6八德路松山路_NAB_07-09.mp4
        cameraNo = 106
        videoStartTime_ori = '2023-09-29-07-00-00'
        videoStartTime = '2023_09_29 07:00:00'

    jsonData["cameraNo"] = cameraNo
    #print(cameraNo)
    jsonData["videoStartTime"] = ""
    
    #20220310 舊影片的十字路口
    '''
    type2Nums = [3, 10, 11, 14]
    type3Nums = [-1]
    if videoNameMode == 2:
        #20220310 對應新影片的十字路口
        type2Nums = [1, 2, 10, 12]
        type3Nums = [3, 4, 5, 6, 8, 9, 11, 13]  #20220320 新影片有要抓單一車道停等區的，定義為type3
    '''
    #20220324 現在都用新的CCTV影像了，路口編號直接用這組
    type2Nums = [1, 2, 10, 12, 106, 107]
    type3Nums = [3, 4, 5, 6, 8, 9, 11, 13]
    
    #暫時要測舊影片切這組
    #type2Nums = [3, 10, 11, 14]
    #type3Nums = [-1]
    
    if cameraNo in type2Nums:
        jsonData["type"] = 2
    elif cameraNo in type3Nums:
        jsonData["type"] = 3
    else:
        jsonData["type"] = 1
        
    roadPointDataPath = "data/road_camera" + str(cameraNo) + ".csv"
    roadEndPointDataPath = "data/road_end_camera" + str(cameraNo) + ".csv"
    saveTrackDataPath = "data/tracks" + str(cameraNo) + ".csv"
    saveTrackDataWithLabelPath = "data/tracks" + str(cameraNo) + "_with_label.csv"
    
    
    #trunGateDataPath = "data/turn_setting_camera_" + str(cameraNo) + ".txt"
    
    #file = open(trunGateDataPath, 'r')
    #trunGateStr = file.readline()
    #trunGate = float(trunGateStr)
    #file.close()
    #現在不用trunGate判斷轉向，改用起點與終點
    #trunGateDataPath = "data/turn_camera" + str(cameraNo) + ".txt"
    #trunGate = np.loadtxt(trunGateDataPath, dtype=np.int,delimiter=',')
    
    if jsonData["type"] == 2: #轉彎車才要road_points
        try:
            road_points = np.loadtxt(roadPointDataPath, dtype=np.float64,delimiter=',')
            road_end_points = np.loadtxt(roadEndPointDataPath, dtype=np.float64,delimiter=',')
        except:
            #20220225 產生座標參考點需要跑 track_create_road_point.ipynb
            #但 track_create_road_point.ipynb 需要車輛路徑紀錄 data/tracks%d.csv 要跑過這邊才會有
            #因此當座標參考點的檔案不存在時，預設使用camera3的值
            road_points = [[698.97437, 182.46745], 
                                [409.28937,538.0], 
                                [5.2457047,178.84816], 
                                [699.0,273.52292]]

            road_end_points = [[697.86664,232.1254], 
                                    [5.0,407.64438], 
                                    [5.669643,140.2039], 
                                    [698.0,414.52475 ]]
    minStartX = min(np.array(road_points)[:,0])
    minStartY = min(np.array(road_points)[:,1])
    maxStartX = max(np.array(road_points)[:,0])
    maxStartY = max(np.array(road_points)[:,1])
    minEndX = min(np.array(road_end_points)[:,0])
    minEndY = min(np.array(road_end_points)[:,1])
    maxEndX = max(np.array(road_end_points)[:,0])
    maxEndY = max(np.array(road_end_points)[:,1])
    
    
    return


def extrapolate(points, minX=0, minY=0, maxX=704, maxY=576):
    #目標是外插到畫面邊緣，也就是 x = 0或704 時 y 在 0~576之間
    #或者 y = 0或576時 x在0~704之間
    xarr = []
    yarr = []
    for i in range(len(points)):
        xarr.append(points[i][0])
        yarr.append(points[i][1])

    if xarr[0] == xarr[1] and yarr[0] == yarr[1]:
        return [xarr[0], yarr[0]]
        
    fy = interp1d(xarr, yarr, fill_value='extrapolate')
    fx = interp1d(yarr, xarr, fill_value='extrapolate')
    tarX = minX
    tarY = minY
    if xarr[0] < xarr[-1]:
        #起始點x比較小，往左邊外插
        tarX = minX
        tarY = fy(tarX).tolist()
    else: 
        #往右邊外插
        tarX = maxX
        tarY = fy(tarX).tolist()
    if (tarY > maxY) or (tarY < 0):
        # y出界了，反過來外插x
        if yarr[0] < yarr[-1]:
            #起始點y比較小，往上方外插
            tarY = minY
            tarX = fx(tarY).tolist()
        else:
            tarY = maxY
            tarX = fx(tarY).tolist()
    
    tarX = max(minX, tarX)
    tarX = min(maxX, tarX)
    tarY = max(minY, tarY)
    tarY = min(maxY, tarY)
    return [tarX, tarY]


#20220510 車道區分，回傳長度用，在區分車道模式，getCountByLane回傳2個值的list，依序[內車道,外車道]，沒區分時就回傳一個int
def getCountByLane(roadList):
    global test_split_road
    result = 0
    if test_split_road:
        result = [len(roadList[0]), len(roadList[1])]
    else:
        result = len(roadList[0])
    return result

#20220510 車道區分，顯示數值用，回傳字串格式
def showCountByLane(roadList):
    global test_split_road
    result = "0"
    if test_split_road:
        result = "%d, %d" % (len(roadList[0]), len(roadList[1]))
    else:
        result = len(roadList[0])
    return result

class Car:
    def __init__(self, carID = 0, carType = 2):
        self.carType = carType #車種
        self.carID = carID #車種
        self.tracks = [] #軌跡紀錄，目前為[x, y, w, h, frameID]
        self.pre_len = 0
        #以後根據情況看要不要擴充車子長寬/出現時間點等等數值進去
        self.clusterType = 0
        self.lastFrame = 0#紀錄最後一次更新時的frame
        '''
        #同一影像 road_points 共用就好，不需要每個car都建立，搬到global
        if jsonData["type"] == 2: #轉彎車才要road_points
            try:
                self.road_points = np.loadtxt(roadPointDataPath, dtype=np.float64,delimiter=',')
                self.road_end_points = np.loadtxt(roadEndPointDataPath, dtype=np.float64,delimiter=',')
            except:
                #20220225 產生座標參考點需要跑 track_create_road_point.ipynb
                #但 track_create_road_point.ipynb 需要車輛路徑紀錄 data/tracks%d.csv 要跑過這邊才會有
                #因此當座標參考點的檔案不存在時，預設使用camera3的值
                self.road_points = [[698.97437, 182.46745], 
                                    [409.28937,538.0], 
                                    [5.2457047,178.84816], 
                                    [699.0,273.52292]]

                self.road_end_points = [[697.86664,232.1254], 
                                        [5.0,407.64438], 
                                        [5.669643,140.2039], 
                                        [698.0,414.52475 ]]

        '''
        #20211004增加，停等時間計算用
        self.maxWaitSec = 0 #停等秒數
        self.turnID = -1 # 轉向
        self.roadID = -1 # 進入道路
        self.isLeave = False
        self.isCounted = False
        
        #20220320增加type3用的停等車計算
        self.stopPoint = [0, 0]
        self.waitSec = 0
        self.totalWaitSec = 0
        self.isWaiting = False
        
        #20220409停等車增加反向車過濾
        self.isReverse = False
        
        
        #20220428 內車道/外車道， 1=內車道， 2=外車道
        self.in_lane_type = -1
        self.out_lane_type = -1
        
        #20221122 熱區轉彎量
        self.hot_area_in = -1
        self.hot_area_out = -1
        self.hot_area_out_frame = -1

        return
    
    def getWaitTimes(self, fps):
        #取得停等時間，預計在車子離開畫面時(也就是取得該車輛得完整軌跡後)呼叫
        
        #基本上畫面中只有一個路口，所以一台車停等時間只會有一次
        #定義為：每台車的[最長連續N個frames沒有移動]除以[FPS]為停等時間
        isWaiting = False
        
        startWaitFrame = 0
        for i in range(len(self.tracks)):
            if i==0:
                continue
            preTrack = self.tracks[i-1]
            nowTrack = self.tracks[i]
            preXc = preTrack[0] + preTrack[2] / 2
            preYc = preTrack[1] + preTrack[3] / 2
            nowXc = nowTrack[0] + nowTrack[2] / 2
            nowYc = nowTrack[1] + nowTrack[3] / 2
            diffX = preXc - nowXc
            diffY = preYc - nowYc
            if diffX * diffX + diffY * diffY < 100: #允許偵測框偏移值10個像素
                #這邊表示車子沒在動
                if isWaiting == False:
                    #車子沒在動，且之前狀態不是等待中，紀錄目標frame (車子開始等待的時間點)
                    isWaiting = True
                    startWaitFrame = nowTrack[4]
            else:
                #這邊表示車子在動
                if isWaiting:
                    #車子在動，且之前狀態為等待中，更新 maxWaitFrames
                    isWaiting = False
                    endWaitFrame = nowTrack[4]
                    waitSec = (endWaitFrame - startWaitFrame) / (float)(fps)
                    if waitSec > self.maxWaitSec:
                        self.maxWaitSec = waitSec
                    
        return
    
    '''        
    def add(self, x, y):
        #幾乎沒移動時不計算
        if len(self.tracks) > 0:
            last = self.tracks[len(self.tracks) - 1]
            d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
            if d > 400:
                self.tracks.append(np.array([x, y]))
        else:
            self.tracks.append(np.array([x, y]))
        return
    '''
    
    def add(self, x, y, w, h, frameID):
        self.tracks.append(np.array([x, y, w, h, frameID]))
        #if len(self.tracks) > 0:
        #    last = self.tracks[len(self.tracks) - 1]
        #    d = (last[0] - x) * (last[0] - x) + (last[1] - y) * (last[1] - y)
        #    if d > 400:
        #        self.tracks.append(np.array([x, y, w, h, frameID]))
        #else:
        #    self.tracks.append(np.array([x, y, w, h, frameID]))
        return

    def getAngle(self, v1, v2):
        #向量求夾角，來源 https://www.it145.com/9/94136.html
        x1 = v1[0]
        y1 = v1[1]
        x2 = v2[0]
        y2 = v2[1]
        theta = np.arctan2(x1 * y2 - y1 * x2, x1 * x2 + y1 * y2)
        return theta * 180 / np.pi
    
    def predict(self):
        global road_points
        global road_end_points
        global minStartX
        global minStartY
        global maxStartX
        global maxStartY
        global minEndX
        global minEndY
        global maxEndX
        global maxEndY
        
        global debug_turn_hot_area
        global mat_in
        global mat_out
        
        result = -1
        result2 = -1
        getLength = 5

        if jsonData["type"] == 1 or jsonData["type"] == 3:
            self.turnID = result
            self.roadID = result2          
        else: #十字路口
            if len(self.tracks) > 20: # 30 10
                if debug_turn_hot_area:

                    #result2 進入路口，等於 self.hot_area_in
                    result2 = self.hot_area_in
                    endRoad = self.hot_area_out

                else:
                    size = len(self.tracks)
                    #計算前 n 點向量
                    vector1 = self.tracks[getLength] - self.tracks[0]
                    #計算後 n 點向量
                    vector2 = self.tracks[size - 1] - self.tracks[size - 1 - getLength]
                    angle = self.getAngle(vector1, vector2)
                    #print(angle)
                    
                    #路口計算，目前先寫死，以後看看有沒有辦法自動化
                    #對應camera3，由下往上
                    #vectorRoad = np.array([-5, -1])
                    #對應camera14
                    #vectorRoad = np.array([13, -35])
                    #觀察進入向量與第一個路口向量的夾角
                    '''
                    inAngle = self.getAngle(vector1, vectorRoad)
                    #print(inAngle)
                    if abs(inAngle) < roadGate: #路口1，正向
                        result2 = 0 #"road1_A" 
                    elif abs(inAngle) > 150: #路口1，反向
                        result2 = 1 #"road1_B"
                    elif inAngle >= roadGate:
                        result2 = 2 #"road2_A"
                    else:
                        result2 = 3 #"road2_B"
                    '''
                    
                    
                    #20211110增加：從頭到尾停住的車不判定
                    startPoint = np.array([self.tracks[0][0] + self.tracks[0][2]//2, self.tracks[0][1]  + self.tracks[0][3]//2 ])
                    endPoint = np.array([self.tracks[-1][0] + self.tracks[-1][2]//2, self.tracks[-1][1]  + self.tracks[-1][3]//2 ])
                    
                    #[20220225增加：這邊也改外插]開始
                    
                    # 起點外插修正
                    #20231002 外插後好像反而比較不準確(一進入就轉彎的車會歪掉)？先關閉外插
                    temp_track = self.tracks[getLength]
                    temp_xc = temp_track[0] + temp_track[2] // 2
                    temp_yc = temp_track[1] + temp_track[3] // 2
                    extraPoint = extrapolate([[startPoint[0], startPoint[1]], [temp_xc, temp_yc]], minStartX, minStartY, maxStartX, maxStartY)
                    #startPoint = np.array(extraPoint)
                    
                    # 終點外插修正
                    temp_track = self.tracks[size - 1 - getLength]
                    temp_xc = temp_track[0] + temp_track[2] // 2
                    temp_yc = temp_track[1] + temp_track[3] // 2
                    #print([[end_xc, end_yc], [temp_xc, temp_yc]])
                    extraPoint = extrapolate([[endPoint[0], endPoint[1]], [temp_xc, temp_yc]], minEndX, minEndY, maxEndX, maxEndY)
                    end_xc = extraPoint[0]
                    end_yc = extraPoint[1]
                    #endPoint = np.array(extraPoint)
                    #[20220225增加：這邊也改外插]結束
                    
                    st_ed_diff = np.linalg.norm(startPoint - endPoint)
                    if st_ed_diff < 50: # 50 10
                        return -1, -1
                    
                    #20210915 路口判定改為用分群結果算出來的座標點，看距離哪個路口點最近
                    result2 = 0
                    
                    minDiff = np.linalg.norm(startPoint - road_points[0])
                    
                    for i in range(len(road_points)):
                        tarDiff = np.linalg.norm(startPoint - road_points[i])
                        if tarDiff < minDiff:
                            minDiff = tarDiff
                            result2 = i
                    
                    #20211103轉向改為抓起點/終點路口判斷
                    endRoad = 0
                    
                    minDiff = np.linalg.norm(endPoint - road_end_points[0])
                    
                    for i in range(len(road_end_points)):
                        tarDiff = np.linalg.norm(endPoint - road_end_points[i])
                        if tarDiff < minDiff:
                            minDiff = tarDiff
                            endRoad = i
                    
                #路口順序 右左上下
                if result2 == 0: #右入
                    if endRoad == 0:
                        #右入 右出，迴轉
                        result = 4
                    elif endRoad == 1:
                        #右入 左出，直行
                        result = 0
                    elif endRoad == 2:
                        #右入 上出，右轉
                        result = 2
                    elif endRoad == 3: #20221206 這邊不能 else 因為有 endRoad = -1 的沒進到熱區的車
                        #右入 下出，左轉
                        result = 1
                elif result2 == 1: #左入
                    if endRoad == 0:
                        #左入 右出，直行
                        result = 0
                    elif endRoad == 1:
                        #左入 左出，迴轉
                        result = 4
                    elif endRoad == 2:
                        #左入 上出，左轉
                        result = 1
                    elif endRoad == 3:
                        #左入 下出，右轉
                        result = 2      
                elif result2 == 2: #上入
                    if endRoad == 0:
                        #上入 右出，左轉
                        result = 1
                    elif endRoad == 1:
                        #上入 左出，右轉
                        result = 2
                    elif endRoad == 2:
                        #上入 上出，迴轉
                        result = 4
                    elif endRoad == 3:
                        #上入 下出，直行
                        result = 0
                elif result2 == 3: #下入
                    if endRoad == 0:
                        #下入 右出，右轉
                        result = 2
                    elif endRoad == 1:
                        #下入 左出，左轉
                        result = 1
                    elif endRoad == 2:
                        #下入 上出，直行
                        result = 0
                    elif endRoad == 3:
                        #下入 下出，迴轉
                        result = 4
                
                if debug_turn_hot_area:
                  if result == 4:
                    result = -1 # 熱區判斷很容易把開的偏中間，且路徑斷掉的當迴轉，熱區判斷不抓迴轉車
                #兩段式左轉，額外判斷
                #定義為：移動途中發生[右轉超過10度再左轉]定義為兩段式左轉
                #有時候判斷會異常，改為指針對摩托車判斷
                if debug_turn_hot_area:
                    #熱區判斷轉彎目前還沒兩段式左轉判斷
                    pass
                else:
                    if self.carType == MotoBikeID and result == 1:#如果左轉
                        maxRightTurn = 0
                        
                        for i in range(10, size, 5): #每個點都判斷太花時間，間隔5判斷1次
                            vector2 = self.tracks[i - 1] - self.tracks[i - 1 - getLength]
                            angle = self.getAngle(vector1, vector2)
                            if maxRightTurn < angle:
                                maxRightTurn = angle
                        if maxRightTurn > 10:
                            result = 3 #符合條件，把左轉改成兩段式左轉
                        
                self.clusterType = result2 * 4 + endRoad
                
                '''
                #20211027 現在轉向量閾值，4個路口有不同值
                if abs(angle) > 150: #除了左右轉，還有迴轉車
                    result = 3 #"trunBack"
                elif angle < -trunGate[result2]:
                    result = 1 #"trunLeft"
                elif angle > trunGate[result2]:
                    result = 2 #"trunRight"
                else:
                    result = 0 #"straight"
                '''  
                    
                '''
                print("time: %d " % (self.lastFrame // 7))
                print("ID: %d Type: %d " % (self.carID, self.carType))
                print(vector1)
                print(vector2)
                #print(vectorRoad)
                print(angle)
                print(inAngle)
                print('=======')
                if self.carID == 6:
                    for i in self.tracks:
                        print(i)
                '''
            self.turnID = result
            self.roadID = result2
            
        return result, result2
    
    def trackToMat(self, im_width, im_height):
        #把路徑畫成mat
        img = np.zeros((im_height, im_width, 3), np.uint8)
        for i in range(len(self.tracks)):
            track = self.tracks[i]
            xc = track[0] + track[2] // 2
            yc = track[1] + track[3] // 2
            
            if i > 0:
                preTrack = self.tracks[i-1]
                pxc = preTrack[0] + preTrack[2] // 2
                pyc = preTrack[1] + preTrack[3] // 2
                if i < len(self.tracks) // 5:
                    cv2.line(img, (xc, yc), (pxc, pyc), (255, 255, 0), 9)
                else:
                    cv2.line(img, (xc, yc), (pxc, pyc), (255, 0, 0), 5)
            
        for i in range(len(self.tracks)):
            track = self.tracks[i]
            xc = track[0] + track[2] // 2
            yc = track[1] + track[3] // 2
            cv2.circle(img, (xc, yc), 2, (0, 0, 255), -1)
        
        return img
        
    
    def predictWithModel(self, cnnModel, im_width, im_height):
        #20211012 改用cnn算路口轉彎
        
        #把路徑畫成mat
        img = self.trackToMat(im_width, im_height)
        
        #resize 100x100 轉RGB 然後餵給模型
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = cv2.resize(img, (100, 100))
        img = img / 255.0
        
        x = np.expand_dims(img, axis=0)
        y = cnnModel.predict_classes(x)[0]
        #y = 0
        #類別順序 [右直、右左、右右、右兩段、右迴轉、左直、左左.....上直、....下直...其他] 共21類
        #也就是，y // 5 得到路口， y % 5 得到轉向        
        roadType = y // 5
        turnType = y % 5
                
        result = -1
        result2 = -1
        
        if roadType < 4:
            result = turnType
            result2 = roadType

        self.turnID = result
        self.roadID = result2
        self.clusterType = y
        return result, result2
    




def show(mat, name='___', scale = 1, pause = False):
    #運算過程的 show 圖放這邊，到時候要關掉直接把下面改 False 就好
    if True:
        if scale != 1:
            h = mat.shape[0]
            w = mat.shape[1]
            scaleMat = cv2.resize(mat, (int(w * scale), int(h * scale)))
            cv2.imshow(name, scaleMat)
        else:
            cv2.imshow(name, mat)
        if pause :
            cv2.waitKey(0)  
    return

def exit(code=0):
    # OpenCV 結束前不把它產生的視窗關閉的話會 crash
    cv2.destroyAllWindows()
    sys.exit(code) 



class VideoTracker(object):
    def __init__(self, cfg, args, video_path, task_id=None):
        
        global IS_DAXING_CLASS
        global BigCarID
        global ExtraBigCarID
        global SmallCarID
        global MotoBikeID
        global PeopleID
        
        if IS_DAXING_CLASS:
            BigCarID = 1
            SmallCarID = 2
            MotoBikeID = 3
            PeopleID = 4
        else:
            BigCarID = 5
            ExtraBigCarID = 7 #公車卡車都是大車
            SmallCarID = 2
            MotoBikeID = 3
            PeopleID = 0
        
        self.cfg = cfg
        self.args = args
        self.video_path = video_path
        self.logger = get_logger("root")
        self.fps = 0
        self.task_id = task_id
        self.task = None
        if self.task_id != None:
            self.task = Task.objects.filter(id=self.task_id).first()
            self.task.timestamp = int(round(datetime.now().timestamp()))
        is_using_cuda = True
        ##to run in cpu mode
        if is_using_cuda == True:
            use_cuda = True
            use_cuda = args.use_cuda and torch.cuda.is_available()
        else:
            use_cuda = False
        if not use_cuda:
            warnings.warn("Running in cpu mode which maybe very slow!", UserWarning)

        if args.display:
            cv2.namedWindow("test", cv2.WINDOW_NORMAL)
            cv2.resizeWindow("test", args.display_width, args.display_height)

        if args.cam != -1:
            print("Using webcam " + str(args.cam))
            self.vdo = cv2.VideoCapture(args.cam)
        else:
            self.vdo = cv2.VideoCapture()
        

        
        # python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --conf-thres 0.5 --img-size 640 --device 0 --names data/custom/car.names --source data/camera3/obj_train_data --output inference/output_camera3
        # opt.output, opt.source, opt.weights, opt.view_img, opt.save_txt, opt.img_size, opt.cfg, opt.names
        
        # python detect.py --weights data/yolov7-w6.pt --source D:/cctv/0323/camera_1_2022-03-23-08-00-00_2min.mp4 
        # --img-size 1280 --conf-thres 0.25 --iou-thres 0.45 --device 0
        opt = {}
        opt["output"] = "inference/output"
        opt["source"] = "data/obj_train_data/"
        #opt["weights"] = "detector/YOLOv7/data/yolov7-w6.pt"
        opt["weights"] = "detector/YOLOv7/data/yolov7-daxing.pt"
        #opt["cfg"] = "detector/YOLOv7/cfg/deploy/yolov7-w6.yaml" # cfg/yolor_p6.cfg
        opt["cfg"] = "detector/YOLOv7/cfg/deploy/yolov7-w6.yaml"
        #opt["names"] = "detector/YOLOv7/cfg/coco_names_with_our_label.names"
        opt["names"] = "detector/YOLOv7/cfg/car.names"
        opt["view_img"] = False
        opt["save_txt"] = False
        opt["img_size"] = 1280
        
        
        if use_cuda:
          opt["device"] = "0"
        else:
          opt["device"] = "cpu"
        opt["augment"] = False
        opt["conf_thres"] = 0.25#0.8 #0.4
        opt["iou_thres"] = 0.45
        opt["agnostic_nms"] = False
        self.detector = build_yolov7_detector(opt, use_cuda=use_cuda)
        self.deepsort1 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort2 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort3 = build_tracker(cfg, use_cuda=use_cuda)
        self.deepsort4 = build_tracker(cfg, use_cuda=use_cuda)

        # yolo V8 用
        self.detector_v8 = None

        self.class_names = self.detector.class_names
        
        
        self.isAreaNeedInit = True
        self.inArea1_1 = set()
        self.inArea1_2 = set()
        
        self.inArea2_1 = set()
        self.inArea2_2 = set()
        
        self.inArea3_1 = set()
        self.inArea3_2 = set()
        
        self.inArea4_1 = set()
        self.inArea4_2 = set()
        
        self.outArea1_1 = set()
        self.outArea1_2 = set()
        
        self.outArea2_1 = set()
        self.outArea2_2 = set()
        
        self.outArea3_1 = set()
        self.outArea3_2 = set()
        
        self.outArea4_1 = set()
        self.outArea4_2 = set()
        
        
        self.turnRightCar = set()
        self.straightCar = set()
        self.turnLeftCar = set()
        
        self.turnRightBigCar = set()
        self.straightBigCar = set()
        self.turnLeftBigCar = set()
        
        self.turnRightMotorbike = set()
        self.straightMotorbike = set()
        self.turnLeftMotorbike = set()
        
        self.carList = {}
        self.lossCarList = {}
        self.occurCarList = {}
        self.matchList = {}
        '''
        20211012轉向定義增加：迴轉與兩段式
        改為各路段分別統計，雙向道，十字路口共4路段，大車小車機車共3種車，然後左右轉/直行共3種狀況，要36個set
        資料用多層結構來放，roads[哪條道路][哪種轉向][哪種車]
        道路順序，先橫向後直向，定義
        roads[0] = 右往左
        roads[1] = 左往右
        roads[2] = 上往下
        roads[3] = 下往上
        轉向定義為
        roads[n][0] = 直行
        roads[n][1] = 左轉
        roads[n][2] = 右轉
        roads[n][3] = 兩段式左轉
        roads[n][4] = 迴轉
        車種定義為
        roads[n][m][0] = 小車
        roads[n][m][1] = 機車
        roads[n][m][2] = 大車
        
        20220510再增加一層，內外車道
        roads[n][m][o][0] = 內車道
        roads[n][m][o][1] = 外車道
        '''
        self.roads = resetRoadCounting()

        #20211015要改成每分鐘輸出，建一個temproads用來儲存該分鐘統計量
        self.temproads = resetRoadCounting()

                    
        self.infoSpaceWidth = 400
        
        
        self.max_x = 703
        self.max_y = 575
        
        #200211012 改用CNN模型算轉彎與路口
        if debug_turn_with_rl == True:
            self.turnModel = tf.keras.models.load_model(CNN_MODEL_PATH)
        
        #20220223 安全距離用，依序放小車/機車/大車 -> 20240312 發現部分小車/機車/大車，部分機車/小車/大車，現在統一改為機車/小車/大車
        self.carSpacings_D = [[],[],[]]
        self.carSpacings_V = [[],[],[]]
        self.carSpacings_D_OneMin = [[],[],[]]
        self.carSpacings_V_OneMin = [[],[],[]]
        self.speedDict = {}#Merge for acc
        self.speedDict_OneQuarter = {}
        self.speedMaDict = {}
        
        self.speedFilteredDict = {}
        self.accDict = {}
        self.accDictQuarter = [{},{},{},{}]
        self.negAccDict = {}
        self.negAccDictQuarter = [{},{},{},{}]
        self.speedLevelToAccDict = {}
        self.speedLevelToAccDictQuarter = [{},{},{},{}]
        #切成15分鐘一個時間區段
        self.speedLevelToNegAccDict = {}
        self.speedLevelToNegAccDictQuarter = [{},{},{},{}]
        self.timeStoreSegment = 15 #2
        
        #存負的加速度
        self.startTime = time.time()
        self.speedLevelToNegAccCsv = io.StringIO()
        self.speedLevelToAccCsv = io.StringIO()#Merge for acc end
        
        self.carSpacingsTotalDict = {}
        # Structure: {
        #     frame_count:{
        #         [carID, carType, backCarID, backCarType, x2w, 
        #           x3w, centerX, centerY, backCarX2w, backCarX3w, 
        #           backCarCenterX, backCarCenterY, backCarSpeed, distance]     
        #     }
        # }
        
        

    def __enter__(self):
        global debug_skip_mode
        global debug_output_video
        global videoStartTime_ori
        if self.args.cam != -1:
            ret, frame = self.vdo.read()
            assert ret, "Error: Camera error"
            self.im_width = frame.shape[0]
            self.im_height = frame.shape[1]
            self.fps = self.vdo.get(cv2.CAP_PROP_FPS)
            
            if debug_skip_mode:
               self.fps /= 5
               
            if self.fps == 0:
                self.fps = 7

        else:
            assert os.path.isfile(self.video_path), f"Path error {self.video_path}"
            self.vdo.open(self.video_path)
            self.im_width = int(self.vdo.get(cv2.CAP_PROP_FRAME_WIDTH))
            self.im_height = int(self.vdo.get(cv2.CAP_PROP_FRAME_HEIGHT))
            #20220225修正，跑30 fps的影片時，要從這邊抓input的fps，後面設置給存出影片的fps用
            self.fps = self.vdo.get(cv2.CAP_PROP_FPS)
            
            if debug_skip_mode:
               self.fps /= 5
               
               
            if self.fps == 0:
                self.fps = 7
            assert self.vdo.isOpened()
            
        self.helf_im_wh = self.im_width * self.im_height / 2#20220504增加，過濾大範圍背景誤抓用

        if self.args.save_path:
            os.makedirs(self.args.save_path, exist_ok=True)

            # path of saved video and results
            #self.save_video_path = os.path.join(self.args.save_path, "results.avi")
            if self.task != None:

                if debug_save_fomat_is_mp4:
                    #self.save_video_path = os.path.join(self.args.save_path, "results_camera" + str(cameraNo) + f'_yolo{YOLO_VERSION}_{videoStartTime_ori}.mp4')
                    self.save_video_path = settings.FILE_PATH + f'output_{self.task.id}_t.mp4'
                    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
                else:
                    #self.save_video_path = os.path.join(self.args.save_path, "results_camera" + str(cameraNo) + f'_yolo{YOLO_VERSION}_{videoStartTime_ori}.avi')
                    self.save_video_path = settings.FILE_PATH + f'output_{self.task.id}.avi'
                    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            else:
                if debug_save_fomat_is_mp4:
                    self.save_video_path = os.path.join(self.args.save_path, "results_camera" + str(cameraNo) + f'_yolo{YOLO_VERSION}_{videoStartTime_ori}.mp4')
                    fourcc = cv2.VideoWriter_fourcc(*'MP4V')
                else:
                    self.save_video_path = os.path.join(self.args.save_path, "results_camera" + str(cameraNo) + f'_yolo{YOLO_VERSION}_{videoStartTime_ori}.avi')
                    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
                
            self.save_results_path = os.path.join(self.args.save_path, "results_camera" + str(cameraNo) + f'_yolo{YOLO_VERSION}_{videoStartTime_ori}.txt')

            # create video writer
            # 20220225 雅筑回報在ubuntu主機上跑，影片無法撥放，測試後mp4格式不行但avi可以，所以以後存result改存avi
            #fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            #fourcc = cv2.VideoWriter_fourcc(*'MP4V')
            #fourcc = cv2.VideoWriter_fourcc('m','p','4','v')
            #fourcc = cv2.VideoWriter_fourcc(*'X264')
            #fourcc = cv2.VideoWriter_fourcc(*'XVID')
            #fourcc = cv2.VideoWriter_fourcc(*'mp4a')
            #fourcc = -1
            
            if self.im_height < 500: #20220225 新camera是寬畫面，高度太短output的字會出界，要把infoSpaceWidth加寬
                self.infoSpaceWidth = 700
                
            if debug_output_video:
                self.writer = cv2.VideoWriter(self.save_video_path, fourcc, self.fps, (self.im_width + self.infoSpaceWidth, self.im_height))

            # logging
            self.logger.info("Save results to {}".format(self.args.save_path))

        self.max_x = self.im_width  - 1
        self.max_y = self.im_height - 1
        
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type:
            print(exc_type, exc_value, exc_traceback)
          
      
    def checkCarList(self, results, idx_frame, fps, frame_count, mat_inside_lane, mat_outside_lane):
        
        global BigCarID
        global SmallCarID
        global MotoBikeID
        global PeopleID
        
        global minStartX
        global minStartY
        global maxStartX
        global maxStartY
        global minEndX
        global minEndY
        global maxEndX
        global maxEndY
        
        global debug_reMatch
        
        global debug_turn_hot_area
        global mat_in
        global mat_out
        
        #print(len(self.matchList))
        #更新self.carList
        for frame_id, tlwhs, track_ids, class_id in results:
            for tlwh, track_id in zip(tlwhs, track_ids):
                x1, y1, w, h = tlwh
                '''
                x = x1 + w//2
                y = y1 + h//2
                if x > self.max_x:
                    x = self.max_x
                elif x < 0:
                    x = 0
                if y > self.max_y:
                    y = self.max_y
                elif y < 0:
                    y = 0
                '''
                rematchTimes = 10# 避免無窮回圈
                while (track_id, class_id) in self.matchList and rematchTimes > 0:
                    (track_id, class_id) = self.matchList[(track_id, class_id)]
                    rematchTimes -= 1
                
                if (track_id, class_id) in self.carList:
                    #追蹤目標已存在，更新座標
                    car = self.carList[(track_id, class_id)]
                    #car.add(x, y)
                    car.add(x1, y1, w, h, idx_frame)
                    car.lastFrame = idx_frame
                    self.carList[(track_id, class_id)] = car
                    #20210908遮擋車處理
                    #如過loss car 被原本sort機制自動找到，就從 lossCarList 清除
                    if debug_reMatch:
                        if (track_id, class_id) in self.lossCarList:
                            del self.lossCarList[(track_id, class_id)]
                else:
                    
                    car = Car(track_id, class_id)
                    car.add(x1, y1, w, h, idx_frame)
                    car.lastFrame = idx_frame
                    
                    #20210908遮擋車處理
                    #抓不是在畫面邊界出現的車(遮擋車)
                    if debug_reMatch:
                        x = x1 + w // 2
                        y = y1 + h // 2
                        boundW = maxStartX // 8
                        boundH = maxStartY // 8
                        if x > boundW and h > 30: #20211013 camera14 有[距離太遠，導致物體太小而消失]的情況，因此增加物體高度限制
                            if x < maxStartX - boundW:
                                if y > boundH:
                                    if y < maxStartY - boundH:
                                        if idx_frame > 7:
                                            self.occurCarList[(track_id, class_id)] = car #這行測試時畫圖用
                                            if len(self.lossCarList.items()) > 0:
                                                #minDistance = sys.maxsize
                                                #minDistance = 10000
                                                minDistance = 100 #20220521 100 pixel平方太寬鬆，改50還是太寬鬆，改10
                                                
                                                targetId = (0, 0)
                                                isMatch = False
                                                for key, lossCar in self.lossCarList.items():
                                                    if len(lossCar.tracks) > 2 and class_id == lossCar.carType:
                                                        track = lossCar.tracks[len(lossCar.tracks) - 1]
                                                        className = self.class_names[lossCar.carType]
                                                        
                                                        #用外插法估算loss car最新座標
                                                        pretrack = lossCar.tracks[len(lossCar.tracks) - 2]
                                                        diff = track - pretrack
                                                        if diff[4] > 1:
                                                            diff = diff // diff[4]
                                                        frameGap = idx_frame - track[4]
                                                        guessPos = track + diff * frameGap
                                                        guessPosX = guessPos[0] + guessPos[2] // 2
                                                        guessPosY = guessPos[1] + guessPos[3] // 2
                                                        distance = (x - guessPosX) * (x - guessPosX) + (y - guessPosY) * (y - guessPosY)
                                                        if distance < minDistance:
                                                            minDistance = distance
                                                            targetId = key
                                                            isMatch = True
                                                if isMatch:
                                                    #print("match %d to %d" % (track_id, targetId[0]) )
                                                    #print(minDistance)
                                                    self.matchList[(track_id, class_id)] = targetId
                                                    del self.lossCarList[targetId]
                                                    
                                                    '''
                                                    print("LossCar List:")
                                                    for key, lossCar in self.lossCarList.items():
                                                        print(lossCar.carID)
                                                    '''
                    
                    #check match list
                    if debug_reMatch:
                        reMatch = (track_id, class_id)
                        rematchTimes = 10
                        while reMatch in self.matchList and rematchTimes > 10:
                            reMatch = self.matchList[reMatch]
                            rematchTimes -= 1
                        #self.carList[(track_id, class_id)] = car
                        if reMatch in self.carList:
                            car = self.carList[reMatch]
                            car.add(x1, y1, w, h, idx_frame)
                            car.lastFrame = idx_frame
                        
                        #self.carList[(track_id, class_id)] = car
                        self.carList[reMatch] = car
                    else:
                        self.carList[(track_id, class_id)] = car
                        
        if test_split_road:
            #20220510 車道判定搬到checkCarList裡面
            for key, car in self.carList.items():
                if car.lastFrame == idx_frame:
                    if len(car.tracks) > 1:
                        track = car.tracks[len(car.tracks) - 1]
                        
                        xc = track[0] + track[2] // 2
                        #yc = track[1] + track[3] // 2
                        yc = track[1] + track[3]#20220510離地面遠的話偏移比較大，改用偵測框的底部
                        
                        if mat_inside_lane[yc][xc] == 255:
                            car.out_lane_type = 1
                        elif mat_outside_lane[yc][xc] == 255:
                            car.out_lane_type = 2
                            
                        if len(car.tracks) == 2:
                            car.in_lane_type = car.out_lane_type
                            
        if debug_turn_hot_area:#20221122 熱區轉彎量
            for key, car in self.carList.items():
                if car.lastFrame == idx_frame:
                    n = 1
                    if len(car.tracks) > n:
                        track = car.tracks[len(car.tracks) - 1]
                        xc = track[0] + track[2] // 2
                        yc = track[1] + track[3]
                        pre_frame_index = track[4]
                        #print('yc')
                        #print(yc)
                        #print(xc)
                        for i, mat in enumerate(mat_in):
                          if mat[yc][xc] == 255:
                            #car.hot_area_in = i
                            is_ok = True
                            for j in range(n-1):
                              xc2, yc2, frame_index2 = getXcYc(car.tracks[len(car.tracks) - 2 - n])
                              if mat[yc2][xc2] != 255:
                                is_ok = False
                              if abs(frame_index2 - pre_frame_index) > 5:
                                is_ok = False
                              pre_frame_index = frame_index2
                            if is_ok:
                              car.hot_area_in = i
                        for i, mat in enumerate(mat_out):
                          if mat[yc][xc] == 255:
                            #car.hot_area_out = i  
                            is_ok = True
                            for j in range(n-1):
                              xc2, yc2, frame_index2 = getXcYc(car.tracks[len(car.tracks) - 2 - n])
                              if mat[yc2][xc2] != 255:
                                is_ok = False
                              if abs(frame_index2 - pre_frame_index) > 5:
                                is_ok = False
                              pre_frame_index = frame_index2
                            #判定離開時，軌跡長度至少要50
                            if len(car.tracks) < 50:
                              is_ok = False
                            if is_ok:
                              if car.hot_area_out_frame == -1:
                                car.hot_area_out = i
                                car.hot_area_out_frame = pre_frame_index
                              #else:
                              #  if abs(car.hot_area_out_frame - pre_frame_index) > 150:
                              #    car.hot_area_out = -1
                            
                                
                                
        #判斷有沒有車子離開，定義為[連續5個frame沒出現]為離開
        delList = []
        #print(f'cars {len(self.carList.items())}')
        #preds = 0
        for key, car in self.carList.items():
            #car = Car(track_id, class_id)
            #(track_id, class_id)
            #20211110 修正，影片結束時，剩下的車也要進行 predict
            #20231002
            if (idx_frame - car.lastFrame > 5 or idx_frame == frame_count) and car.isLeave == False:
            #if True:
                #20230213 如果 tracks 沒變長，不用重新判定
                if len(car.tracks) == car.pre_len:
                    continue
                car.pre_len = len(car.tracks)
                #20211012 改用CNN算路口轉彎
                if debug_turn_with_rl == True:
                    #print(f'preds {preds}')
                    #preds += 1
                    trunID, roadID = car.predictWithModel(self.turnModel, self.im_width, self.im_height)
                else:
                    trunID, roadID = car.predict()
                car.isWaiting = False
                if trunID != -1: #20211110修正，斷掉的(長度小於10)路徑應該直接判定成 isLeave，因為後面還有機會接回來
                    self.carList[key].isLeave = True
                    
                
                '''
                車種定義為
                roads[n][m][0] = 小車
                roads[n][m][1] = 機車
                roads[n][m][2] = 大車
                BigCarID = 1
                SmallCarID = 2
                MotoBikeID = 3
                '''
                #print("predict: car %d, is %d, %d" % ())
                if trunID != -1 and roadID != -1:
                    carTypeID = 0
                    if car.carType == SmallCarID:
                        carTypeID = 0
                    elif car.carType == MotoBikeID:
                        carTypeID = 1
                    elif car.carType == BigCarID:
                        carTypeID = 2
                    elif car.carType == PeopleID:
                        carTypeID = 3
                        
                    #20220510 依內外車道分開統計
                    idx = 0
                    if test_split_road:
                        if car.out_lane_type == 2:
                            idx = 1
                    self.roads[roadID][trunID][carTypeID][idx].add((car.carID, car.carType))
                    self.temproads[roadID][trunID][carTypeID][idx].add((car.carID, car.carType))
                    #20211004停等時間計算，統一在車子離開時計算停等時間
                    car.getWaitTimes(fps)
                #else:
                #    print("lossTrack: %d in frame %d " % (track_id, idx_frame))
                #del self.carList[track_id]
                #delList.append(key)
            #20210908遮擋車處理
            #抓不是在畫面邊界消失的車(遮擋車)
            if debug_reMatch:
                if idx_frame - car.lastFrame == 1:
                    track = car.tracks[len(car.tracks) - 1]
                    x = track[0] + track[2] // 2
                    y = track[1] + track[3] // 2
                    boundW = self.im_width // 8
                    boundH = self.im_height // 8
                    if x > boundW and track[3] > 30: #20211013 camera14 有[距離太遠，導致物體太小而消失]的情況，因此增加物體高度限制
                        if x < self.im_width - boundW:
                            if y > boundH:
                                if y < self.im_height - boundH:
                                    #print("ADD: %d %d" % (car.carID, car.carType))
                                    self.lossCarList[(car.carID, car.carType)] = car

                
                
        #print(f'preds {preds}')        
        #for key in delList:
        #    del self.carList[key]
    
    def saveCarList(self):
        file = open(saveTrackDataPath, 'w')
        maxLength = 0
        for key, car in self.carList.items():
            length = len(car.tracks)
            if maxLength < length:
                maxLength = length
        for key, car in self.carList.items():
            file.write(str(car.carID) + ",")
            file.write(str(car.carType) + ",")
            #用補0補滿的方式讓每筆資料長度一樣
            for i in range(maxLength):
                if i < len(car.tracks):
                    track = car.tracks[i]                
                    file.write(str(track[0]) + ",")
                    file.write(str(track[1]) + ",")
                    file.write(str(track[2]) + ",")
                    file.write(str(track[3]) + ",")
                    if i < maxLength - 1:
                        file.write(str(track[4]) + ",")
                    else:
                        file.write(str(track[4]) + "\n")
                else:
                    if i < maxLength - 1:
                        file.write("0,0,0,0,0,")
                    else:
                        file.write("0,0,0,0,0\n")
        file.close()
        
        file = open(saveTrackDataWithLabelPath, 'w')
        maxLength = 0
        for key, car in self.carList.items():
            length = len(car.tracks)
            if maxLength < length:
                maxLength = length
        for key, car in self.carList.items():
            file.write(str(car.clusterType) + ",")
            file.write(str(car.carID) + ",")
            file.write(str(car.carType) + ",")
            #用補0補滿的方式讓每筆資料長度一樣
            for i in range(maxLength):
                if i < len(car.tracks):
                    track = car.tracks[i]                
                    file.write(str(track[0]) + ",")
                    file.write(str(track[1]) + ",")
                    file.write(str(track[2]) + ",")
                    file.write(str(track[3]) + ",")
                    if i < maxLength - 1:
                        file.write(str(track[4]) + ",")
                    else:
                        file.write(str(track[4]) + "\n")
                else:
                    if i < maxLength - 1:
                        file.write("0,0,0,0,0,")
                    else:
                        file.write("0,0,0,0,0\n")
        file.close()
        return
    
    def test_drawLossCar(self, img, idx_frame):#測試用，把 LossCar 位置畫出來   
        
        #print(len(self.lossCarList.items()))
        for key, car in self.lossCarList.items():
            #print(car.carID)
            if len(car.tracks) > 3:
                track = car.tracks[len(car.tracks) - 1]
                className = self.class_names[car.carType]
                
                x1 = track[0]
                y1 = track[1]
                x2 = track[0] + track[2]
                y2 = track[1] + track[3]
                color = compute_color_for_labels(car.carID)
                label = "Loss%d(%s)" % (car.carID, className)
                t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1.2 , 1)[0]
                cv2.rectangle(img,(x1, y1),(x2,y2),color,3)
                cv2.rectangle(img,(x1, y1),(x1+t_size[0]+3,y1+t_size[1]+4), color,-1)
                cv2.putText(img,label,(x1,y1+t_size[1]+4), cv2.FONT_HERSHEY_PLAIN, 1.2, [255,255,255], 1)
                cv2.circle(img, ((x1+x2)//2, (y1+y2)//2), 3, color, -1)
                
                #用外插法預測loss car最新座標
                pretrack = car.tracks[len(car.tracks) - 3]
                diff = track - pretrack
                if diff[4] > 1:
                    diff = diff // diff[4]
                frameGap = idx_frame - track[4]
                guessPos = track + diff * frameGap
                x1 = guessPos[0]
                y1 = guessPos[1]
                x2 = guessPos[0] + guessPos[2]
                y2 = guessPos[1] + guessPos[3]   
                cv2.rectangle(img,(x1, y1),(x2,y2),color,5)
                
        for key, car in self.occurCarList.items():
            if len(car.tracks) > 1:
                track = car.tracks[len(car.tracks) - 1]
                className = self.class_names[car.carType]
                
                x1 = track[0]
                y1 = track[1]
                x2 = track[0] + track[2]
                y2 = track[1] + track[3]
                color = compute_color_for_labels(car.carID)
                label = "Occur%d(%s)" % (car.carID, className)
                t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1.2 , 1)[0]
                cv2.rectangle(img,(x1, y1),(x2,y2),color,3)
                cv2.rectangle(img,(x1, y1),(x1+t_size[0]+3,y1+t_size[1]+4), color,-1)
                cv2.putText(img,label,(x1,y1+t_size[1]+4), cv2.FONT_HERSHEY_PLAIN, 1.2, [255,255,255], 1)
                cv2.circle(img, ((x1+x2)//2, (y1+y2)//2), 3, color, -1)
        return img
                
    def drawCarList(self, img, idx_frame):
        img = img.copy()
        for key, car in self.carList.items():
            if car.lastFrame == idx_frame:
                if len(car.tracks) > 1:
                    track = car.tracks[len(car.tracks) - 1]
                    className = self.class_names[car.carType]
                    
                    x1 = track[0]
                    y1 = track[1]
                    x2 = track[0] + track[2]
                    y2 = track[1] + track[3]
                    color = compute_color_for_labels(car.carID)
                    label = "%d(%s)" % (car.carID, className)
                    
                    if car.isWaiting:#20220323增加：如果車輛停等中，顯示該輪停等秒數
                        label = "%d(%s) wait:%.2f sec" % (car.carID, className, car.waitSec)
                    
                    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1.2 , 1)[0]
                    cv2.rectangle(img,(x1, y1),(x2,y2),color,1)
                    cv2.rectangle(img,(x1, y1),(x1+t_size[0]+3,y1+t_size[1]+4), color,-1)
                    #20231002
                    label_out = f'{label}_{car.roadID}_{car.turnID}'
                    cv2.putText(img,label,(x1,y1+t_size[1]+4), cv2.FONT_HERSHEY_PLAIN, 1.2, [255,255,255], 1)
                    #cv2.circle(img, ((x1+x2)//2, (y1+y2)//2), 3, color, -1)
                    
                    #20231002
                    #t0 = car.tracks[0]
                    #cv2.line(img, (x1, y1),(int(t0[0]), int(t0[1])),color,2)

                    
                    
                    
        return img
    
    def drawCarListWithWarp(self, img, idx_frame, warpM, maxW, maxH, warpPixelRate):
        global BigCarID
        global SmallCarID
        global MotoBikeID
        global PeopleID
        img = img.copy()     
        carLines = []
        for key, car in self.carList.items():
            if car.lastFrame == idx_frame:
                if len(car.tracks) > 1:
                    track = car.tracks[len(car.tracks) - 1]
                    className = self.class_names[car.carType]
                    
                    x1 = track[0]
                    y1 = track[1]
                    x2 = track[0] + track[2]
                    y2 = track[1] + track[3]
                
                    x1w = (warpM[0][0] * x1 +warpM [0][1] * y1 +warpM [0][2]) / (warpM[2][0] * x1 + warpM[2][1] * y1 + warpM[2][2])
                    y1w = (warpM[1][0] * x1 +warpM [1][1] * y1 +warpM [1][2]) / (warpM[2][0] * x1 + warpM[2][1] * y1 + warpM[2][2])
                    
                    x2w = (warpM[0][0] * x2 +warpM [0][1] * y2 +warpM [0][2]) / (warpM[2][0] * x2 + warpM[2][1] * y2 + warpM[2][2])
                    y2w = (warpM[1][0] * x2 +warpM [1][1] * y2 +warpM [1][2]) / (warpM[2][0] * x2 + warpM[2][1] * y2 + warpM[2][2])
                    
                    x1w = int(x1w)
                    y1w = int(y1w)
                    x2w = int(x2w)
                    y2w = int(y2w)
                    
                    #框框座標投影變形後會擴大太多，改用左下，右下
                    x3 = track[0]
                    y3 = track[1] + track[3]
                    
                    x3w = (warpM[0][0] * x3 +warpM [0][1] * y3 +warpM [0][2]) / (warpM[2][0] * x3 + warpM[2][1] * y3 + warpM[2][2])
                    y3w = (warpM[1][0] * x3 +warpM [1][1] * y3 +warpM [1][2]) / (warpM[2][0] * x3 + warpM[2][1] * y3 + warpM[2][2])
                    x3w = int(x3w)
                    y3w = int(y3w)
                    centerX = (x2w + x3w) // 2
                    centerY = (y2w + y3w) // 2

                    #如果中心點出界則不畫
                    #20220119修正，y應該要車尾
                    #if centerX < 0 or centerX > maxW or centerY < 0 or centerY > maxH:
                    if centerX < 0 or centerX > maxW or y3w < 0 or y3w > maxH:
                        continue
                    
                    #首先把每條車的參考線放到同一個list
                    #20220111 加上車輛種類做為參考值，算車間距時，減掉前車的參考車長
                    carLines.append([x3w, centerY, x2w, centerY, car.carType])
                    
                    #畫圖
                    color = (0, 255, 0)
                    cv2.line(img,(x3w, centerY),(x2w, centerY),color,5)
        
        #20220111 車間距修正，如果有3台以上的車，只算前後車的間距(也就是，兩台車之前如果有第三台車就不算車間距)
        #依照車輛的y值排序
        carLines.sort(key = lambda s: s[1])
        
                
        carSpacings = []
        #對排序後的車輛參考線前後比對
        #if len(carLines) > 2:
        if len(carLines) > 1: #至少要兩台車才有車間距
            drawShiftX = 0 #畫圖時，兩條以上車間距線要shift避免重疊
            for i in range(1, len(carLines), 1):
                car1 = carLines[i - 1] #前車
                car2 = carLines[i] #後車
                #如果後車在不同車道，抓下一輛車
                shiftIndex = 0
                getCar = True
                while min(car1[2], car2[2]) - max(car1[0], car2[0]) <= 0:
                    shiftIndex += 1
                    if shiftIndex + i < len(carLines):
                        car2 = carLines[i + shiftIndex]
                    else:
                        getCar = False
                        break
                if getCar:
                    diff = abs(car2[1] - car1[1]) * warpPixelRate
                    drawX = drawShiftX + (car1[0] + car1[2]) // 2
                    drawShiftX += 20
                    
                    
                    #根據前車種類修正車間距
                    #根據 https://www.uhelp.cc/a/202105/431468.html
                    #小車大約4m，大車差距比較大，這邊取6m
                    #然後根據 https://zhidao.baidu.com/question/1894812022468146780.html
                    #摩托車大約2m
                    #20220120小車參考值改為5.8，大車改為9.1
                    carSpacing_FixValue = 0
                    #if car1[4] == BigCarID:#20220119更正...修正值應該是後車
                    if car2[4] == BigCarID:
                        carSpacing_FixValue = 9.1
                    elif car2[4] == SmallCarID:
                        carSpacing_FixValue = 5.8
                    elif car2[4] == MotoBikeID:
                        carSpacing_FixValue = 2
                    diff -= carSpacing_FixValue
                    
                    
                    if diff > 0:
                        carSpacings.append(diff)
                        #畫未修正車長的線
                        cv2.line(img, (drawX, car1[1]), (drawX, car2[1]), (0, 255, 0), 3)
                        #畫有修正車長的線
                        carLength = int(carSpacing_FixValue / warpPixelRate)
                        cv2.line(img, (drawX + 10, car1[1]), (drawX + 10, car2[1] - carLength), (0, 128, 0), 2)
                        cv2.line(img, (car2[0], car2[1] - carLength), (car2[2], car2[1] - carLength), (0, 128, 0), 2)
                        
                        
                        label = "%.2fm" % diff
                        cv2.putText(img, label, (drawX + 20, car2[1] - 20), cv2.FONT_HERSHEY_PLAIN, 1.2, [0,255,0], 1)
        return img
    
    
    def getWarp(self, x, y, warpM):
        xw = (warpM[0][0] * x + warpM[0][1] * y + warpM[0][2]) / (warpM[2][0] * x + warpM[2][1] * y + warpM[2][2])
        yw = (warpM[1][0] * x + warpM[1][1] * y + warpM[1][2]) / (warpM[2][0] * x + warpM[2][1] * y + warpM[2][2])
        return xw, yw
   
    def getSpeed(self, idx_frame, warpM, maxW, maxH, warpPixelRate):#Merge acc
        global BigCarID
        global SmallCarID
        global MotoBikeID
        global PeopleID
        #speedDict 結構是 各種className(Dict) -> 各種car的key(Dict) -> 該車輛各frame的速度(List) -> 每筆速度值用 frame,speed組成(Tuple)
        #例如 {BigCar: {(100, 1): [(1, 10), (2, 11)], (101, 1): [(1, 30), (2, 28)]}}
        for key, car in self.carList.items():
            if car.lastFrame == idx_frame:
                if len(car.tracks) > 1:
                    track = car.tracks[-1]
                    className = self.class_names[car.carType]
                    # 使用物件的中心點當作參考點
                    x = track[0] + track[2]/2
                    y = track[1] + track[3]/2
                    # 如果物件離邊框太近，則不計算速度
                    if ( (track[0] - track[2] / 2) >= (self.im_width * 0.05) ) and ( (track[0] + track[2] / 2) <= (self.im_width * 0.95) ) and ( (track[1] - track[3] / 2) >= (self.im_height * 0.05) ) and ( (track[1] + track[3] / 2) <= (self.im_height * 0.95) ):
                        baseMinimalArea = (self.im_width * self.im_height * 0.02 * 0.1)
                        minimalArea = baseMinimalArea
                        if car.carType == BigCarID:
                            minimalArea = baseMinimalArea * 6
                        elif car.carType == SmallCarID:
                            minimalArea = baseMinimalArea * 3
                        elif car.carType == MotoBikeID:
                            minimalArea = baseMinimalArea
                            
                        
                        # 如果物件的面積小於某個限制，則不計算速度
                        if track[2] * track[3] >= minimalArea :
                        #變形
                            xw, yw = self.getWarp(x, y, warpM)
                            
                            #如果中心點出界則不計算(過濾路邊停車)
                            if xw < 0 or xw > maxW or yw < 0 or yw > maxH:
                                continue
                            
                            
                                
                            
                            #20220505 防邊框抖動，速度取樣間隔用1秒
                            #如果樣本不足時，有多遠抓多遠
                            previousTrack = car.tracks[max(0, len(car.tracks) - 1 - int(self.fps))]
                            
                            #previousTrack = car.tracks[-2]
                            
                            previousX = previousTrack[0] + previousTrack[2] / 2
                            previousY = previousTrack[1] + previousTrack[3] / 2
                            previousXW, previousYW = self.getWarp(previousX, previousY, warpM)
                            #currentSpeed = (abs(yw-previousYW) * warpPixelRate / ((track[4] - previousTrack[4])/self.fps)) * 3.6
                            #這邊乘3.6是把單位轉時速， m/s * 3.6 = km/hr
                            currentSpeed = (abs(yw-previousYW) * warpPixelRate / ((track[4] - previousTrack[4])/self.fps))*3.6
                            
                            #20220506 輸出速度還要分正向道路/反向道路，所以需要再存一個有帶正負號的
                            currentSpeedWithSig = ((yw-previousYW) * warpPixelRate / ((track[4] - previousTrack[4])/self.fps))*3.6
                            #if currentSpeed >= 70:
                            #    print(currentSpeed)
                            #20220604 過濾異常值，背景抓成車/ID switch可能造成異常車速，把速度大於120的過濾掉
                            if currentSpeed > 120:
                                continue
                            currentSpeedTuple = (idx_frame, currentSpeed, currentSpeedWithSig)
                            
                            if className not in self.speedDict:
                                self.speedDict[className] = {}
                            if key not in self.speedDict[className]:
                                self.speedDict[className][key] = []
                                
                            if className not in self.speedDict_OneQuarter:
                                self.speedDict_OneQuarter[className] = {}
                            if key not in self.speedDict_OneQuarter[className]:
                                self.speedDict_OneQuarter[className][key] = []
                            self.speedDict[className][key].append(currentSpeedTuple)
                            self.speedDict_OneQuarter[className][key].append(currentSpeedTuple)
                    
    def getMaSpeed(self, idx_frame, samplePointQuantity):#速度的移動平均
        #speedMaDict結構是 各種className(Dict) -> 各種car的key(Dict) -> 該車輛各frame的ma速度(List) -> 每筆速度值用 frame,MaSpeed 組成(Tuple)
        for className, carDict in self.speedDict.items():
            if className not in self.speedMaDict:
                self.speedMaDict[className] = {}

            for key, speedList in carDict.items():
                if key not in self.speedMaDict[className]:
                    self.speedMaDict[className][key] = []

                if speedList[-1][0] == idx_frame:
                    if len(speedList) >= samplePointQuantity:#如果目標car的速度取樣數大於等於窗格
                        speedList.sort(key=lambda x: x[0])#就對速度取樣值依frame順序排序
                        #然後抓後n比取平均
                        speedMa = sum([x[1] for x in speedList[-samplePointQuantity:]])/samplePointQuantity
                        #20220506 輸出速度還要分正向道路/反向道路，所以需要再存一個有帶正負號的
                        speedMaWithSig = sum([x[2] for x in speedList[-samplePointQuantity:]])/samplePointQuantity
                        speedMaTuple = (idx_frame, speedMa, speedMaWithSig)
                        self.speedMaDict[className][key].append(speedMaTuple)
                        
    def getFilteredSpeed(self, idx_frame, windowSize):
        #speedFilteredDict結構同speedDict
        for className, carDict in self.speedDict.items():
            if className not in self.speedFilteredDict:
                self.speedFilteredDict[className] = {}
            for key, speedList in carDict.items():
                if key not in self.speedFilteredDict[className]:
                    self.speedFilteredDict[className][key] = []
                if len(speedList)>windowSize: #若目標車輛得速度取樣數量大於窗格  (嗯？這邊等於時不計算？)
                    x = np.array(speedList)[:,0] #把frames放到x
                    y = np.array(speedList)[:,1] #把speed放到y
                    x = x[np.logical_not(np.isnan(y))] #把nan的樣本刪除
                    y = y[np.logical_not(np.isnan(y))]
                    yhat = savgol_filter(y, windowSize, 3) #呼叫savgol_filter濾波做轉換
                    x = x[np.logical_not(np.isnan(yhat))]
                    yhat = yhat[np.logical_not(np.isnan(yhat))]
                    xyHatList = np.vstack((x,yhat)).T.tolist()
                    self.speedFilteredDict[className][key] = xyHatList 

    def getAccelerationSpeed(self, idx_frame):
        #accDictQuarter/negAccDictQuarter結構:
        #最上層List，裡面包4個Dict每15分鐘分開包 
        #->Dict以車種為key，value放另一層List
        #->List裡面塞currentAccTuple
        #->Tuple結構是(frame, acc)
        
        #accDict/negAccDict結構：
        #最上層Dict，key為車種，value為另一個Dict
        #->這層Dict以車輛id為key，value為List
        #->List裡面塞currentAccTuple
        #->Tuple結構是(frame, acc)
        
        #speedLevelToAccDict/speedLevelToNegAccDict結構：
        #最上層Dict，key為車種，value為List
        #->List為依10單位切分開的速度分區，裡面再一層List
        #->List裡面放currentAcc，也就是加速度採樣值
        currentMinute = int(idx_frame / self.fps / 60) #抓目前在第幾分鐘
        #20220505依指示， smooth的地方用回average movement
        #for objectType, objectSpeedFilteredDict in self.speedFilteredDict.items():
        for objectType, objectSpeedFilteredDict in self.speedMaDict.items():
            for objectId, speedFilteredList in objectSpeedFilteredDict.items():
                #if len(speedFilteredList) > windowSize: #嗯？這行指定 windowSize 但裡面沒再一輪移動平均，像是誤寫？
                if len(speedFilteredList) > 2:
                    if objectType not in self.accDict:
                        self.accDict[objectType] = {}
                        self.negAccDict[objectType] = {}
                    if currentMinute//self.timeStoreSegment > 3: #大於3個片段也就是4個片段時(1個片段如果15m就是1小時)
                        currentMinute = self.timeStoreSegment * 3 #當現在是第4片段 (4片段分別 0 15 30 45)
                    if objectType not in self.accDictQuarter[currentMinute//self.timeStoreSegment]:
                        self.accDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
                        self.negAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
                    if objectId not in self.accDict[objectType]:
                        self.accDict[objectType][objectId] = []
                        self.negAccDict[objectType][objectId] = []
                        for index, speedFilteredTuple in enumerate(speedFilteredList):
                            if index > 0:
                                if currentMinute//self.timeStoreSegment > 3:
                                    currentMinute = self.timeStoreSegment*4-1#超過1小時的部分不分段(也就是會通通塞到第4段)
                                previousSpeedFilteredTuple = speedFilteredList[index-1]
                                #加速度就 (目前速度-之前速度) / (目前frame-之前frame)，單位似乎會有問題因為速度是 km/h
                                #如果加速度單位要 kmph/s 的話，還要乘上fps
                                #如果單位要 m/s^2 的話，還要除3.6再乘fps
                                #currentAcc = (speedFilteredTuple[1] - previousSpeedFilteredTuple[1]) / (speedFilteredTuple[0] - previousSpeedFilteredTuple[0])
                                currentAcc = (speedFilteredTuple[1] - previousSpeedFilteredTuple[1]) / (speedFilteredTuple[0] - previousSpeedFilteredTuple[0]) / 3.6 * self.fps
                                currentAccTuple = (speedFilteredTuple[0], currentAcc)
                                #如果目標Dict/List的對應區塊為None的話，就補上空List
                                if objectType not in self.speedLevelToAccDict:
                                    self.speedLevelToAccDict[objectType] = []
                                    self.speedLevelToNegAccDict[objectType] = []
                                if objectType not in self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment]:
                                    self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
                                    self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
                                currentSpeedLevel = int(previousSpeedFilteredTuple[1] / 10)#加速度分區用，以速度分區，每10單位速度分同一區
                                
                                while len(self.speedLevelToAccDict[objectType]) < currentSpeedLevel + 1:
                                    self.speedLevelToAccDict[objectType].append([])
                                    self.speedLevelToNegAccDict[objectType].append([])
                                while len(self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType]) < currentSpeedLevel + 1:
                                    self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append([])
                                    self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append([])
                                #把加速度值塞到對應的 accDictQuarter/negAccDictQuarter
                                if abs(currentAcc) >= 0.3:
                                    if currentAcc > 0:
                                        self.accDict[objectType][objectId].append(currentAccTuple)
                                        self.accDictQuarter[currentMinute//self.timeStoreSegment][objectType].append(currentAccTuple)
                                        self.speedLevelToAccDict[objectType][currentSpeedLevel].append(currentAccTuple)
                                        self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].append(currentAccTuple)
                                    else:
                                        self.negAccDict[objectType][objectId].append(currentAccTuple)
                                        self.negAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append(currentAccTuple)
                                        self.speedLevelToNegAccDict[objectType][currentSpeedLevel].append(currentAccTuple)
                                        self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].append(currentAccTuple)
                    else:
                        if currentMinute//self.timeStoreSegment > 3:
                            currentMinute = self.timeStoreSegment*4-1
                        speedFilteredTuple = None
                        previousSpeedFilteredTuple = None
                        for speedTuple in speedFilteredList:
                            if speedTuple[0] == idx_frame :
                                speedFilteredTuple = speedTuple
                                for prespeedTuple in speedFilteredList:
                                    if prespeedTuple[0] < speedTuple[0] and previousSpeedFilteredTuple is None:
                                        previousSpeedFilteredTuple = prespeedTuple
                                    elif prespeedTuple[0] < speedTuple[0] and prespeedTuple[0] > previousSpeedFilteredTuple[0]:
                                        previousSpeedFilteredTuple = prespeedTuple
                                    else:
                                        pass
                                break
                        if speedFilteredTuple is not None and previousSpeedFilteredTuple is not None:
                            currentAcc = (speedFilteredTuple[1] - previousSpeedFilteredTuple[1]) / (speedFilteredTuple[0] - previousSpeedFilteredTuple[0]) / 3.6 * self.fps
                            currentAccTuple = (speedFilteredTuple[0], currentAcc)
                            if objectType not in self.speedLevelToAccDict:
                                self.speedLevelToAccDict[objectType] = []
                                self.speedLevelToNegAccDict[objectType] = []
                            if objectType not in self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment]:
                                self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
                                self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
                            #print(previousSpeedFilteredTuple[1])
                            currentSpeedLevel = int(speedFilteredTuple[1] / 10)
                            while len(self.speedLevelToAccDict[objectType]) < currentSpeedLevel + 1:
                                self.speedLevelToAccDict[objectType].append([])
                                self.speedLevelToNegAccDict[objectType].append([])
                            while len(self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType]) < currentSpeedLevel + 1:
                                self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append([])
                                self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append([])
                            # if abs(currentAcc) >= 0.01:
                            #     if speedFilteredTuple[1] > 35 and abs(currentAcc) > 2.6:
                            #         pass
                            #     else:
                            if currentAcc > 0:
                                self.accDict[objectType][objectId].append(currentAccTuple)
                                self.accDictQuarter[currentMinute//self.timeStoreSegment][objectType].append(currentAccTuple)
                                self.speedLevelToAccDict[objectType][currentSpeedLevel].append(currentAccTuple)
                                self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].append(currentAccTuple)
                            else:
                                self.negAccDict[objectType][objectId].append(currentAccTuple)
                                self.negAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append(currentAccTuple)
                                self.speedLevelToNegAccDict[objectType][currentSpeedLevel].append(currentAccTuple)
                                self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].append(currentAccTuple)
                                # if speedFilteredTuple[0] > 35 and abs(currentAcc) > 2.6:
                                #     print("start detopleted")
                                #     if currentAcc > 0:
                                #         self.accDict[objectType][objectId].remove(currentAccTuple)
                                #         self.accDictQuarter[currentMinute//self.timeStoreSegment][objectType].remove(currentAccTuple)
                                #         self.speedLevelToAccDict[objectType][currentSpeedLevel].remove(currentAccTuple)
                                #         self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].remove(currentAccTuple)
                                #     else:
                                #         self.negAccDict[objectType][objectId].remove(currentAccTuple)
                                #         self.negAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].remove(currentAccTuple)
                                #         self.speedLevelToNegAccDict[objectType][currentSpeedLevel].remove(currentAccTuple)
                                #         self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].remove(currentAccTuple)
                                #     print("deleted")
                                # merged_acc_list = self.accDict[objectType][objectId] + self.negAccDict[objectType][objectId]
                                # merged_acc_list.sort(key=lambda x:x[0])
                                # if len(merged_acc_list) > 5:
                                #     accDifferenceList = []
                                #     lastFiveAccList = merged_acc_list[-5:]
                                #     for count, accTuple in enumerate(lastFiveAccList):
                                #         if count != 0:
                                #             accDifferenceList.append(abs(lastFiveAccList[count][1] - lastFiveAccList[count-1][1]))
                                #     strangeAccTuple = None
                                #     if max(accDifferenceList) > (sorted(accDifferenceList)[int(len(accDifferenceList)*0.5)]) * 1.4:
                                #         if accDifferenceList.index(max(accDifferenceList)) == 0 :
                                #             if accDifferenceList[1] > (sorted(accDifferenceList)[int(len(accDifferenceList)*0.5)]) * 1.4:
                                #                 strangeAccTuple = lastFiveAccList[1]
                                #                 print('remove11111111111111111')
                                #             else:
                                #                 strangeAccTuple = lastFiveAccList[0]
                                #                 print('remove00000000000000000')
                                #         elif accDifferenceList.index(max(accDifferenceList)) == 3:
                                #             if accDifferenceList[2] > (sorted(accDifferenceList)[int(len(accDifferenceList)*0.5)]) * 1.4:
                                #                 # strangeAccTuple = lastFiveAccList[3]
                                #                 # print('remove33333333333333333')
                                #                 pass
                                #             else:
                                #                 # strangeAccTuple = lastFiveAccList[4]
                                #                 # print('remove44444444444444444')
                                #                 pass
                                #         elif accDifferenceList.index(max(accDifferenceList)) == 1:
                                #             if accDifferenceList[0] > accDifferenceList[2]:
                                #                 strangeAccTuple = lastFiveAccList[1]
                                #                 print('remove11111111111111111')
                                #             else:
                                #                 strangeAccTuple = lastFiveAccList[2]
                                #                 print('remove22222222222222222')
                                #         elif accDifferenceList.index(max(accDifferenceList)) == 2:
                                #             if accDifferenceList[1] > accDifferenceList[3]:
                                #                 strangeAccTuple = lastFiveAccList[2]
                                #                 print('remove22222222222222222')
                                #             else:
                                #                 pass
                                #                 # strangeAccTuple = lastFiveAccList[3]
                                #                 # print('remove33333333333333333')
                                #         if strangeAccTuple is not None:
                                #             try:
                                #                 self.accDict[objectType][objectId].remove(strangeAccTuple)
                                #                 self.accDictQuarter[currentMinute//self.timeStoreSegment][objectType].remove(strangeAccTuple)
                                #                 self.speedLevelToAccDict[objectType][currentSpeedLevel].remove(strangeAccTuple)
                                #                 self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].remove(strangeAccTuple)
                                #             except Exception:
                                #                 self.negAccDict[objectType][objectId].remove(strangeAccTuple)
                                #                 self.negAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].remove(strangeAccTuple)
                                #                 self.speedLevelToNegAccDict[objectType][currentSpeedLevel].remove(strangeAccTuple)
                                #                 self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].remove(strangeAccTuple)
                                #             for speedMaTuple in self.speedMaDict[objectType][objectId]:
                                #                 if speedMaTuple[0] == strangeAccTuple[0]:
                                #                     self.speedMaDict[objectType][objectId].remove(speedMaTuple)
                                #                     break
                                #             for speedTuple in self.speedDict[objectType][objectId]:
                                #                 if speedTuple[0] == strangeAccTuple[0]:
                                #                     self.speedDict[objectType][objectId].remove(speedTuple)
                                #                     break
                                #         else:
                                #             pass
#     def getAccelerationSpeed(self, idx_frame):
#         currentMinute = int(idx_frame / self.fps / 60)
#         for objectType, objectSpeedMaDict in self.speedMaDict.items():
#             for objectId, speedMaList in objectSpeedMaDict.items():
#                 for speedMaTuple in speedMaList:
#                     if speedMaTuple[0] == idx_frame:
#                         if len(speedMaList) > 1:
#                             #把分鐘限制在一小時內
#                             if currentMinute//self.timeStoreSegment > 3:
#                                 currentMinute = self.timeStoreSegment*4-1
#                             if objectType not in self.accDict:
#                                 self.accDict[objectType] = {}
#                                 self.negAccDict[objectType] = {}
#                             if objectType not in self.accDictQuarter[currentMinute//self.timeStoreSegment]:
#                                 self.accDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
#                                 self.negAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
#                             if objectId not in self.accDict[objectType]:
#                                 self.accDict[objectType][objectId] = []
#                                 self.negAccDict[objectType][objectId] = []
#                             previousSpeedMaTuple = speedMaList[-2]
#                             currentAcc = (speedMaTuple[1] - previousSpeedMaTuple[1]) / ((speedMaTuple[0] - previousSpeedMaTuple[0]) / self.fps) / 3.6
#                             if abs(currentAcc) <= 4.5:
                                
#                                 currentAccTuple = (speedMaTuple[0],currentAcc)
                                
#                                 if objectType not in self.speedLevelToAccDict:
#                                     self.speedLevelToAccDict[objectType] = []
#                                     self.speedLevelToNegAccDict[objectType] = []
                                
#                                 if objectType not in self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment]:
#                                     self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
#                                     self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType] = []
                                
#                                 currentSpeedLevel = int(previousSpeedMaTuple[1] / 10)
                                
#                                 while len(self.speedLevelToAccDict[objectType]) < currentSpeedLevel + 1:
#                                     self.speedLevelToAccDict[objectType].append([])
#                                     self.speedLevelToNegAccDict[objectType].append([])
                                    
#                                 while len(self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType]) < currentSpeedLevel + 1:
#                                     self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append([])
#                                     self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append([])
                                
#                                 if abs(currentAcc) >= 0.3:
#                                     if currentAcc > 0:
#                                         self.accDict[objectType][objectId].append(currentAccTuple)
#                                         self.accDictQuarter[currentMinute//self.timeStoreSegment][objectType].append(currentAccTuple)
#                                         self.speedLevelToAccDict[objectType][currentSpeedLevel].append(currentAcc)
#                                         self.speedLevelToAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].append(currentAcc)
                                    
#                                     else:
#                                         self.negAccDict[objectType][objectId].append(currentAccTuple)
#                                         self.negAccDictQuarter[currentMinute//self.timeStoreSegment][objectType].append(currentAccTuple)
#                                         self.speedLevelToNegAccDict[objectType][currentSpeedLevel].append(currentAcc)
#                                         self.speedLevelToNegAccDictQuarter[currentMinute//self.timeStoreSegment][objectType][currentSpeedLevel].append(currentAcc)
# #currentSpeedLevel,self.speedLevelToAccDict[objectType][currentSpeedLevel][int(len(self.speedLevelToAccDict[objectType][currentSpeedLevel])*0.7)]                                
#                             else:
#                                 del self.speedMaDict[objectType][objectId][-1]
#                                 del self.speedDict[objectType][objectId][-1] #Merge acc end
                            
                                
        # idx_frame 是當下的while迴圈到哪個frame
        # accDict是存物件的加速度
    def getCarsSpacing(self, idx_frame, warpM, maxW, maxH, warpPixelRate, img=None, isDrawImg=False):
        global BigCarID
        global SmallCarID
        global MotoBikeID
        global PeopleID
        if isDrawImg:
            img = img.copy()
        '''
        20220223 安全距離AX BX:
        根據實驗設計v3(以下引用自 實驗設計v3 投影片p5)
            利用車道線間距10m，影像7fps之特性，取得物件每幀移動距離，即得每幀之瞬時速度。
            後車車速(v )：取物件進入熱區後第一筆速度資料代表。
            車輛間距(d )：前、後車離開熱區之時間差(s )×後車車速(v )。
            為確保是連續車流的跟車行為，取得的資料集應濾除：
            任一幀速度為0之物件編號，排除紅燈停等或突發情境。
            車輛間距(d )大於50m之組別，排除塞車、前方紅燈等情境。
            濾除後依車種(後車)將max(d )及min(d )分別取z值為1、0，代入公式聯立取得bx,add、bx,mult。
            
        d = ax + bx = ax + (bx_add + bx_mult*z) * sqrt(v)
        另外根據 實驗設計v3 投影片p4，ax是機車0.4，小車2.1
        沒看到大車的ax值，這邊代入小車的值，2.1
            
        需要v與d才能得到安全距離
        關於v，目前冠全那邊的code還沒整合進來，這邊另外計算
        
        這邊要記錄最大d與最小d，以及該時間點對應的v
        
        這邊是單一frame，所以先計算個別的d與v，儲存起來
        最後再找出 min max 代入公式算 bx_add 與 bx_mult
        '''
        #20220310 有些camera有雙向道，增加carLines_reverse
        carLines = []
        carLines_reverse = []
        for key, car in self.carList.items():
            if car.lastFrame == idx_frame:
                #20220223現在這邊也要車速，所以至少要有兩個座標點
                if len(car.tracks) > 2:
                    track = car.tracks[len(car.tracks) - 1]
                    
                    x1 = track[0]
                    y1 = track[1]
                    x2 = track[0] + track[2]
                    y2 = track[1] + track[3]
                
                    #x1w = (warpM[0][0] * x1 +warpM [0][1] * y1 +warpM [0][2]) / (warpM[2][0] * x1 + warpM[2][1] * y1 + warpM[2][2])
                    #y1w = (warpM[1][0] * x1 +warpM [1][1] * y1 +warpM [1][2]) / (warpM[2][0] * x1 + warpM[2][1] * y1 + warpM[2][2])
                    x1w, y1w = self.getWarp(x1, y1, warpM)
                    
                    #x2w = (warpM[0][0] * x2 +warpM [0][1] * y2 +warpM [0][2]) / (warpM[2][0] * x2 + warpM[2][1] * y2 + warpM[2][2])
                    #y2w = (warpM[1][0] * x2 +warpM [1][1] * y2 +warpM [1][2]) / (warpM[2][0] * x2 + warpM[2][1] * y2 + warpM[2][2])
                    x2w, y2w = self.getWarp(x2, y2, warpM)
                    
                    x1w = int(x1w)
                    y1w = int(y1w)
                    x2w = int(x2w)
                    y2w = int(y2w)
                    
                    #框框座標投影變形後會擴大太多，改用左下，右下
                    x3 = track[0]
                    y3 = track[1] + track[3]
                    
                    #x3w = (warpM[0][0] * x3 +warpM [0][1] * y3 +warpM [0][2]) / (warpM[2][0] * x3 + warpM[2][1] * y3 + warpM[2][2])
                    #y3w = (warpM[1][0] * x3 +warpM [1][1] * y3 +warpM [1][2]) / (warpM[2][0] * x3 + warpM[2][1] * y3 + warpM[2][2])
                    x3w, y3w = self.getWarp(x3, y3, warpM)
                    
                    x3w = int(x3w)
                    y3w = int(y3w)
                    centerX = (x2w + x3w) // 2
                    centerY = (y2w + y3w) // 2

                    #如果中心點出界則不計算
                    #20220119修正，y應該要車尾
                    #if centerX < 0 or centerX > maxW or centerY < 0 or centerY > maxH:
                    if centerX < 0 or centerX > maxW or y3w < 0 or y3w > maxH:
                        continue
                    
                    #首先把每條車的參考線放到同一個list
                    
                    
                    #20220223 加算車速
                    #20220505 防邊框抖動，速度取樣間隔用1秒
                    #如果樣本不足時，有多遠抓多遠
                    preTrack = car.tracks[max(0, len(car.tracks) - 1 - int(self.fps/2))]
                    
                    x4 = preTrack[0]
                    y4 = preTrack[1] + preTrack[3]
                    
                    #y4w = (warpM[1][0] * x4 +warpM [1][1] * y4 +warpM [1][2]) / (warpM[2][0] * x4 + warpM[2][1] * y4 + warpM[2][2])
                    x4w, y4w = self.getWarp(x4, y4, warpM)
                    
                    diffY = abs(y4w - y3w) * warpPixelRate #算出移動距離(單位公尺)
                    #修正，有時候bbox會跳frame所以要再除frame間隔數
                    speed = 0
                    # if self.class_names[car.carType] in self.speedDict:
                    #     if key in self.speedMaDict[self.class_names[car.carType]]:
                    #         for speedMaTuple in self.speedMaDict[self.class_names[car.carType]][key]:
                    #             if speedMaTuple[0] == idx_frame:
                    #                 speed = speedMaTuple[1]/3.6
                    #                 break
                    speed = diffY / (track[4] - preTrack[4]) * self.fps #然後乘上fps變成車速(單位公尺/秒)
                            # for i in range(5):
                            #     self.logger.info(f'car {key} Ma speed {speed} Raw speed {diffY / (track[4] - preTrack[4]) * self.fps}')
                    
                    #20220310 有些camera車道是反向的(之前是由下往上開，然後新camera4是由上往下開)，需要改成分開紀錄
                    #判斷進入方向
                    startTrack = car.tracks[0]
                    x_in = startTrack[0]
                    y_in = startTrack[1]
                    x_inw, y_inw = self.getWarp(x_in, y_in, warpM)
                    
                    if y_inw > maxH // 2 and speed > 1/3.6:
                        #20220111 加上車輛種類做為參考值，算車間距時，減掉前車的參考車長
                        carLines.append([x3w, centerY, x2w, centerX, car.carType, speed, key])
                    else:
                        if speed > 1/3.6:
                            carLines_reverse.append([x3w, centerY, x2w, centerX, car.carType, speed, key])
                        
                    if isDrawImg:
                        #畫圖
                        color = (0, 255, 0)
                        cv2.line(img,(x3w, centerY),(x2w, centerY),color,5)
        
        #20220111 車間距修正，如果有3台以上的車，只算前後車的間距(也就是，兩台車之前如果有第三台車就不算車間距)
        #依照車輛的y值排序
        carLines.sort(key = lambda s: s[1])
        carLines_reverse.sort(key = lambda s: s[1], reverse = True)
                
        carSpacings = []
        
        #對排序後的車輛參考線前後比對
        if len(carLines) > 1: #至少要兩台車才有車間距
            drawShiftX = 0
            for i in range(1, len(carLines), 1):
                car1 = carLines[i - 1] #前車
                car2 = carLines[i] #後車
                if car2[4] == MotoBikeID: #如果後車是機車，則不計算車間距
                    continue
                else:
                    #如果後車在不同車道，抓下一輛車
                    shiftIndex = 0
                    getCar = True
                    while min(car1[2], car2[2]) - max(car1[0], car2[0]) <= 0:
                        shiftIndex += 1
                        if shiftIndex + i < len(carLines):
                            car2 = carLines[i + shiftIndex]
                        else:
                            getCar = False
                            break
                    if getCar:
                        diff = abs(car2[1] - car1[1]) * warpPixelRate
                        #根據前車種類修正車間距
                        #根據 https://www.uhelp.cc/a/202105/431468.html
                        #小車大約4m，大車差距比較大，這邊取6m
                        #然後根據 https://zhidao.baidu.com/question/1894812022468146780.html
                        #摩托車大約2m
                        #20220120小車參考值改為5.8，大車改為9.1
                        carSpacing_FixValue = 0
                        #if car1[4] == BigCarID:#20220119更正...修正值應該是後車
                        if car2[4] == BigCarID:
                            carSpacing_FixValue = 9.1
                        elif car2[4] == SmallCarID:
                            carSpacing_FixValue = 5.8
                        elif car2[4] == MotoBikeID:
                            carSpacing_FixValue = 2
                        diff -= carSpacing_FixValue
                        if diff > 0 and diff < 50: #依據實驗設計v3，d大於50不計算
                            carSpacings.append(diff)
                            #20220223安全距離與車速
                            #依後車車種放到list
                            if idx_frame not in self.carSpacingsTotalDict:
                                self.carSpacingsTotalDict[idx_frame] = []
                            self.carSpacingsTotalDict[idx_frame].append([car1[6], car1[4], car2[6], car2[4], car1[2], car1[0], car1[3], car1[1], car2[2], car2[0], car2[3], car2[1], car2[5], diff])
                            for i in range(10):
                                self.logger.info("here!!")
                            if car2[4] == MotoBikeID:
                                self.carSpacings_D[0].append(diff)
                                self.carSpacings_V[0].append(car2[5])
                                self.carSpacings_D_OneMin[0].append(diff)
                                self.carSpacings_V_OneMin[0].append(car2[5])
                            elif car2[4] == SmallCarID:
                                self.carSpacings_D[1].append(diff)
                                self.carSpacings_V[1].append(car2[5])
                                self.carSpacings_D_OneMin[1].append(diff)
                                self.carSpacings_V_OneMin[1].append(car2[5])
                            elif car2[4] == BigCarID:
                                self.carSpacings_D[2].append(diff)
                                self.carSpacings_V[2].append(car2[5])
                                self.carSpacings_D_OneMin[2].append(diff)
                                self.carSpacings_V_OneMin[2].append(car2[5])
                            
                            if isDrawImg:
                                #畫未修正車長的線
                                drawX = drawShiftX + (car1[0] + car1[2]) // 2
                                drawShiftX += 20
                                cv2.line(img, (drawX, car1[1]), (drawX, car2[1]), (0, 255, 0), 3)
                                #畫有修正車長的線
                                carLength = int(carSpacing_FixValue / warpPixelRate)
                                cv2.line(img, (drawX + 10, car1[1]), (drawX + 10, car2[1] - carLength), (0, 128, 0), 2)
                                cv2.line(img, (car2[0], car2[1] - carLength), (car2[2], car2[1] - carLength), (0, 128, 0), 2)
                                label = "%.2fm" % diff
                                cv2.putText(img, label, (drawX + 20, car2[1] - 20), cv2.FONT_HERSHEY_PLAIN, 1.2, [0,255,0], 1)
                            
        if len(carLines_reverse) > 1: #這邊做反向車道
            drawShiftX = 0
            for i in range(1, len(carLines_reverse), 1):
                car1 = carLines_reverse[i - 1] #前車
                car2 = carLines_reverse[i] #後車
                if car2[4] == MotoBikeID: #如果後車是機車，則不計算車間距
                    continue
                else:
                    #如果後車在不同車道，抓下一輛車
                    shiftIndex = 0
                    getCar = True
                    while min(car1[2], car2[2]) - max(car1[0], car2[0]) <= 0:
                        shiftIndex += 1
                        if shiftIndex + i < len(carLines_reverse):
                            car2 = carLines_reverse[i + shiftIndex]
                        else:
                            getCar = False
                            break
                    if getCar:
                        diff = abs(car2[1] - car1[1]) * warpPixelRate                  
                        #這邊是反向車道，參考線會在前輪
                        #所以要剪掉的是前車車長
                        carSpacing_FixValue = 0
                        if car1[4] == BigCarID:
                            carSpacing_FixValue = 9.1
                        elif car1[4] == SmallCarID:
                            carSpacing_FixValue = 5.8
                        elif car1[4] == MotoBikeID:
                            carSpacing_FixValue = 2
                            
                        diff -= carSpacing_FixValue
                        if diff > 0 and diff < 50: #依據實驗設計v3，d大於50不計算
                            carSpacings.append(diff)
                            #20220223安全距離與車速
                            #依後車車種放到list
                            if idx_frame not in self.carSpacingsTotalDict:
                                self.carSpacingsTotalDict[idx_frame] = []
                            self.carSpacingsTotalDict[idx_frame].append([car1[6], car1[4], car2[6], car2[4], car1[2], car1[0], car1[3], car1[1], car2[2], car2[0], car2[3], car2[1], car2[5], diff])
                            if car2[4] == MotoBikeID:
                                self.carSpacings_D[0].append(diff)
                                self.carSpacings_V[0].append(car2[5])
                                self.carSpacings_D_OneMin[0].append(diff)
                                self.carSpacings_V_OneMin[0].append(car2[5])
                            elif car2[4] == SmallCarID:
                                self.carSpacings_D[1].append(diff)
                                self.carSpacings_V[1].append(car2[5])
                                self.carSpacings_D_OneMin[1].append(diff)
                                self.carSpacings_V_OneMin[1].append(car2[5])
                            elif car2[4] == BigCarID:
                                self.carSpacings_D[2].append(diff)
                                self.carSpacings_V[2].append(car2[5])
                                self.carSpacings_D_OneMin[2].append(diff)
                                self.carSpacings_V_OneMin[2].append(car2[5])
                                
                            if isDrawImg:
                                #畫未修正車長的線
                                drawX = drawShiftX + (car1[0] + car1[2]) // 2
                                drawShiftX += 20
                                cv2.line(img, (drawX, car1[1]), (drawX, car2[1]), (0, 255, 0), 3)
                                #畫有修正車長的線，這邊是反向，要畫前車後輪到後車前輪
                                carLength = int(carSpacing_FixValue / warpPixelRate)
                                cv2.line(img, (drawX + 10, car1[1] - carLength), (drawX + 10, car2[1]), (0, 128, 0), 2)
                                
                                cv2.line(img, (car1[0], car1[1] - carLength), (car1[2], car1[1] - carLength), (0, 128, 0), 2)
                                label = "%.2fm" % diff
                                cv2.putText(img, label, (drawX + 20, car2[1] - 20), cv2.FONT_HERSHEY_PLAIN, 1.2, [0,255,0], 1)
                            
        
        return carSpacings, img
    
    #20220505增加輸出minD, maxD
    def setAXBXToJson(self, jsonData, i, ax, bx_add, bx_mult, minD, maxD):
        if i == 0:
            jsonData["extraCounting"]["motorbike"]["ax"] = ax
            jsonData["extraCounting"]["motorbike"]["bx_add"] = bx_add
            jsonData["extraCounting"]["motorbike"]["bx_mult"] = bx_mult
            jsonData["extraCounting"]["motorbike"]["minD"] = minD
            jsonData["extraCounting"]["motorbike"]["maxD"] = maxD
        elif i == 1:
            jsonData["extraCounting"]["smallCar"]["ax"] = ax
            jsonData["extraCounting"]["smallCar"]["bx_add"] = bx_add
            jsonData["extraCounting"]["smallCar"]["bx_mult"] = bx_mult
            jsonData["extraCounting"]["smallCar"]["minD"] = minD
            jsonData["extraCounting"]["smallCar"]["maxD"] = maxD
        elif i == 2:
            jsonData["extraCounting"]["bigCar"]["ax"] = ax
            jsonData["extraCounting"]["bigCar"]["bx_add"] = bx_add
            jsonData["extraCounting"]["bigCar"]["bx_mult"] = bx_mult
            jsonData["extraCounting"]["bigCar"]["minD"] = minD
            jsonData["extraCounting"]["bigCar"]["maxD"] = maxD
        return jsonData

    def run(self):
        global debug_skip_mode
        global debug_draw_track
        global debug_test_n_sec
        global debug_test_secs
        global debug_draw_car_spacing
        global debug_save_car_tracking
        global test_split_road
        
        global debug_turn_hot_area
        global mat_in
        global mat_out
        
        global BigCarID
        global SmallCarID
        global MotoBikeID
        global PeopleID
        
        global video_day
        global IS_DAXING_CLASS

        results = []
        idx_frame = 0
        
        # 抓影片長度 https://stackoverflow.com/questions/49048111/how-to-get-the-duration-of-video-using-cv2
        fps = self.vdo.get(cv2.CAP_PROP_FPS)
        if debug_skip_mode:
            fps /= 5
        
        self.fps = fps
        if not os.path.isdir(self.args.save_path + 'temp/'):
            os.mkdir(self.args.save_path + 'temp/')
            
        #20220428 車道分離測試
        if test_split_road:
            imagePath = "data/road%d.png" % cameraNo
            if os.path.isfile(imagePath):
                mat = cv2.imread(imagePath)
                mats = cv2.split(mat)
                mat_inside_lane = mats[2]
                mat_outside_lane = mats[1]
                
                _, mat_inside_lane = cv2.threshold(mat_inside_lane, 254, 255, cv2.THRESH_BINARY)
                _, mat_outside_lane = cv2.threshold(mat_outside_lane, 254, 255, cv2.THRESH_BINARY)
            else:
                test_split_road = False
                mat_inside_lane = None
                mat_outside_lane = None
        else: #20220521修正，這邊沒加的話關閉test_split_road時會Error
            mat_inside_lane = None
            mat_outside_lane = None
        #20221122 熱區轉向量
        mat_in = [None, None, None, None]
        mat_out = [None, None, None, None]
        if debug_turn_hot_area:
            mat_in = []
            mat_out = []
            imagePath = "data/map_%d_r.png" % cameraNo
            if os.path.isfile(imagePath):
                mat = cv2.imread(imagePath)
                mats = cv2.split(mat)
                m_in = mats[2]
                m_out = mats[1]
                _, m_in = cv2.threshold(m_in, 254, 255, cv2.THRESH_BINARY)
                _, m_out = cv2.threshold(m_out, 254, 255, cv2.THRESH_BINARY)
                mat_in.append(m_in)
                mat_out.append(m_out)
                
                imagePath = "data/map_%d_l.png" % cameraNo
                mat = cv2.imread(imagePath)
                mats = cv2.split(mat)
                m_in = mats[2]
                m_out = mats[1]
                _, m_in = cv2.threshold(m_in, 254, 255, cv2.THRESH_BINARY)
                _, m_out = cv2.threshold(m_out, 254, 255, cv2.THRESH_BINARY)
                mat_in.append(m_in)
                mat_out.append(m_out)
                
                imagePath = "data/map_%d_u.png" % cameraNo
                mat = cv2.imread(imagePath)
                mats = cv2.split(mat)
                m_in = mats[2]
                m_out = mats[1]
                _, m_in = cv2.threshold(m_in, 254, 255, cv2.THRESH_BINARY)
                _, m_out = cv2.threshold(m_out, 254, 255, cv2.THRESH_BINARY)
                mat_in.append(m_in)
                mat_out.append(m_out)
                
                imagePath = "data/map_%d_d.png" % cameraNo
                mat = cv2.imread(imagePath)
                mats = cv2.split(mat)
                m_in = mats[2]
                m_out = mats[1]
                _, m_in = cv2.threshold(m_in, 254, 255, cv2.THRESH_BINARY)
                _, m_out = cv2.threshold(m_out, 254, 255, cv2.THRESH_BINARY)
                mat_in.append(m_in)
                mat_out.append(m_out)
            else:
                debug_turn_hot_area = False
                mat_in = [None, None, None, None]
                mat_out = [None, None, None, None]
        '''
        cv2.imshow("src", mat)
        cv2.imshow("R", mat_inside_lane)
        cv2.imshow("G", mat_outside_lane)
        
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        exit(0) 
        '''

        
        frame_count = int(self.vdo.get(cv2.CAP_PROP_FRAME_COUNT))
        if fps == 0:
            print("Warning: can't get video fps.... set fps = 7 ")
            fps = 7
            self.fps = 7
        videoLength = frame_count/fps
        jsonData["videoLength"] = videoLength
        
        outputJsonTimes = 1
        totalJsonData = []
        
        #車間距
        carSpacing_Total = []
        carSpacing_OneMin = []
        
        countt = 0
        temp_frame = None
        
        while self.vdo.grab():
            idx_frame += 1
            
            if self.task != None:
                if idx_frame % 5 == 0 or idx_frame == frame_count or idx_frame == frame_count - 1 or idx_frame == 1:
                    self.task.timestamp = int(round(datetime.now().timestamp()))
                    if self.task != None and 'debug_run_sec' in self.task.setting and self.task.setting['debug_run_sec'] not in [None, 0]:
                        self.task.message = f'running {idx_frame} / {int(self.task.setting["debug_run_sec"]  * self.fps)}'
                    else:
                        self.task.message = f'running {idx_frame} / {frame_count}'
                    self.task.save()
            if debug_test_n_sec:# 測試用，幾秒後break
                if idx_frame > int(debug_test_secs * self.fps):
                    break
                if idx_frame == int(debug_test_secs * self.fps):
                    print("debug_test_n_sec: skip in %d sec" % debug_test_secs)
                    idx_frame = frame_count

            if self.task != None and 'debug_run_sec' in self.task.setting and self.task.setting['debug_run_sec'] not in [None, 0]:
                if idx_frame > int(self.task.setting['debug_run_sec']  * self.fps):
                    break
                if idx_frame == int(self.task.setting['debug_run_sec']  * self.fps):
                    print("debug_test_n_sec: skip in %d sec" % self.task.setting['debug_run_sec'] )
                    idx_frame = frame_count
            
            
            if idx_frame % self.args.frame_interval:
                #print('skip A')
                #continue
                pass
            #print(f'now in {idx_frame}')
            start = time.time()
            _, ori_im = self.vdo.retrieve()
            if ori_im is not None:
                '''
                diff = ori_im.copy()
                if temp_frame is not None:
                    cv2.absdiff(ori_im, temp_frame, diff)
                    difference = np.sum(np.sum(diff,axis=0))
                    if difference < 100000:
                        print("skipping")
                        print('skip B')
                        continue
                    else:
                        pass
                '''
                temp_frame = ori_im
                countt += 1
            
            test_masking_road_section = True
            #直線道路，車間距
            if jsonData["type"] == 1 or jsonData["type"] == 3:
                #首先投影轉換，這組是camera7的座標，其他camera還沒量測
                #填轉換前座標，用小畫家依序量測左上、左下、右下、右上的座標點位置
                tarH = 360
                tarW = 320
                warpPixelRate = 0.113
                srcPoints = np.array([[417, 176], [335, 358], [593, 358], [483, 178]]).astype(np.float32)
                # tarW = 320
                # tarH = 720
                # warpPixelRate = 0.0625
                # srcPoints = np.array([[226, 144], [37, 712], [977, 522], [410, 144]]).astype(np.float32)
                # tarW = 320
                # tarH = 720
                # warpPixelRate = 0.0658
                # srcPoints = np.array([[234, 144], [10, 713], [998, 511], [400, 135]]).astype(np.float32)
                #20220111根據不同camera，轉換座標要填不同值，數值暫時填camera7的，之後會找冠全要轉換座標值
                if self.task != None and 'srcPoints' in self.task.setting:
                    tarW = self.task.setting['tarW']
                    tarH = self.task.setting['tarH']
                    srcPoints = np.array(self.task.setting['srcPoints']).astype(np.float32)
                    warpPixelRate = self.task.setting['warpPixelRate']

                elif cameraNo == 7:
                    if video_day < 20220416:
                        tarH = 360
                        tarW = 320
                        warpPixelRate = 0.113
                        srcPoints = np.array([[417, 176], [335, 358], [593, 358], [483, 178]]).astype(np.float32)
                        # tarW = 480
                        # tarH = 176
                        # warpPixelRate = 0.086
                        # srcPoints = np.array([[126, 103], [68, 477], [561, 365], [210, 94]]).astype(np.float32)
                        # tarW = 320
                        # tarH = 720
                        # warpPixelRate = 0.0625
                        # srcPoints = np.array([[226, 144], [37, 712], [977, 522], [410, 144]]).astype(np.float32)
                        # tarW = 320
                        # tarH = 720
                        # warpPixelRate = 0.0658
                        # srcPoints = np.array([[234, 144], [10, 713], [998, 511], [400, 135]]).astype(np.float32)
                        # tarH = 360
                        # tarW = 320
                        # tarH = 480
                        # tarW = 176
                        # warpPixelRate = 0.13
                        # srcPoints = np.array([[135, 101], [76, 479], [557, 364], [220, 94]]).astype(np.float32)
                    elif video_day > 20220701:
                        #Marked at:  129 97
                        #Marked at:  38 478
                        #Marked at:  544 352
                        #Marked at:  222 95
                        #srcH =  480 srcW =  704 tarH =  480 tarW =  176.0
                        #Marked at:  61 254
                        #Marked at:  65 143
                        #Warpped rate =  0.09009009009009009
                        #srcH =  480 srcW =  704 tarH =  480 tarW =  176.0
                        srcW = 704
                        srcH = 480
                        tarH = 176
                        tarW = 480
                        warpPixelRate = 0.09009
                        srcPoints = np.array([[129, 97], [38, 478], [544, 352], [222, 95]]).astype(np.float32)
                    else:
                        #20220509 新cctv(20220501)的視野不同，參數是對應下面這組
                        # tarW = 480
                        # tarH = 176
                        # warpPixelRate = 0.086
                        # srcPoints = np.array([[126, 103], [68, 477], [561, 365], [210, 94]]).astype(np.float32)
                        # 
                        tarH = 360
                        tarW = 320
                        warpPixelRate = 0.113
                        srcPoints = np.array([[417, 176], [335, 358], [593, 358], [483, 178]]).astype(np.float32)
                        # srcH =  480 srcW =  704 tarH =  480 tarW =  176.0
                        # Marked at:  37 148
                        # Marked at:  34 257
                        # tarH = 360
                        # tarW = 320
                        # tarH = 480
                        # tarW = 176
                        # warpPixelRate = 0.13
                        # # srcPoints = np.array([[411, 175], [314, 358], [592, 359], [483, 178]]).astype(np.float32)
                        # srcPoints = np.array([[135, 101], [76, 479], [557, 364], [220, 94]]).astype(np.float32)
                elif cameraNo == 3:
                    srcPoints = np.array([[543, 458], [702, 712], [1115, 623], [731, 460]]).astype(np.float32)
                    warpPixelRate = 10 / 281.0
                elif cameraNo == 4:
                    srcPoints = np.array([[229, 81], [242, 277], [658, 249], [373, 77]]).astype(np.float32)
                    #10m = 197 pixel
                    warpPixelRate = 10 / 197.0
                elif cameraNo == 6:
                    srcPoints = np.array([[214, 257], [406, 365], [628, 333], [293, 248]]).astype(np.float32)
                    warpPixelRate = 10 / 118.0
                elif cameraNo == 8:
                    #srcPoints = np.array([[169, 155], [260, 541], [1121, 299], [495, 106]]).astype(np.float32)
                    #tarW = 800
                    #tarH = 600
                    #warpPixelRate = 10 / 131.0
                    srcPoints = np.array([[218, 148], [741, 407], [974, 345], [326, 132]]).astype(np.float32)
                    warpPixelRate = 10 / 170.0
                elif cameraNo == 9:
                    srcPoints = np.array([[127, 225], [146, 389], [443, 286], [195, 203]]).astype(np.float32)
                    warpPixelRate = 10 / 118.0
                elif cameraNo == 11:
                    srcPoints = np.array([[367, 128], [362, 271], [696, 278], [495, 142]]).astype(np.float32)
                    warpPixelRate = 0.105
                elif cameraNo == 13:
                    srcPoints = np.array([[288, 99], [8, 274], [407, 271], [464, 104]]).astype(np.float32)
                    warpPixelRate = 10 / 274.0
                
                #填轉換後座標，轉換成長方形，依序填想轉出之長方形左上、左下、右下、右上的座標點位置
                tarPoints = np.array([[0, 0], [0, tarH], [tarW, tarH], [tarW, 0]]).astype(np.float32)
                
                #使用OpenCV進行投影轉換
                warpM = cv2.getPerspectiveTransform(srcPoints, tarPoints)
                matWarp = cv2.warpPerspective(ori_im, warpM, (tarW, tarH))
                
                if test_masking_road_section:
                    masked = np.zeros(ori_im.shape[:2], dtype = "uint8")
                    polygon_points = np.int32([srcPoints])
                    #polygon_points[0][0][0][0] = 
                    cv2.polylines(masked, polygon_points, 1, 255)
                    cv2.fillPoly(masked, polygon_points, 255)
                    ori_im = cv2.bitwise_and(ori_im, ori_im, mask = masked)
            if test_fish_eye:
                ori_im = undistort(ori_im, 800)
                x = 320
                y = 670
                w = 960
                h = 270
                ori_im = ori_im[y:y+h,x:x+w]
                ori_im = ori_im.astype(np.uint8)
                ori_im = cv2.resize(ori_im, (1280, 360))
            
            
            if debug_skip_mode:
                if idx_frame % 5 != 1:
                    continue
            
            self.ori_im = ori_im
            im = cv2.cvtColor(ori_im, cv2.COLOR_BGR2RGB)
            if idx_frame == 1:
                cv2.imwrite(self.args.save_path + "temp%d.png" % cameraNo, ori_im)

            #if idx_frame < 80*7:
            #    continue

            #im = cv2.imread('temp_106.png') # 20231007 測試對應用，拿一張背景塗黑到只剩一個車子的圖來測試，測試起來有直接對應
            # do detection
            bbox_xywh = None
            cls_conf = None
            cls_ids = None
            if YOLO_VERSION == 'V8':
                if self.detector_v8 == None:
                    self.detector_v8 = YOLO(V8_MODEL)
                    self.class_names = self.detector_v8.model.names

                y_hat = self.detector_v8.predict(im, verbose=False) #(frame, conf = 0.7, classes = class_IDS, device = 0, verbose = False)
                # Getting the bounding boxes, confidence and classes of the recognize objects in the current frame.
                bbox_xywh   = y_hat[0].boxes.xywh.cpu().numpy()
                cls_conf    = y_hat[0].boxes.conf.cpu().numpy()
                cls_ids = y_hat[0].boxes.cls.cpu().numpy() 

                # 20231007 測試對應用，測試起來有直接對應
                '''
                boxes, conf, classes = self.detector(im)
                if len(cls_ids) == 1 and len(classes) == 1:
                    print('bbox_xywh =')
                    print(type(bbox_xywh))
                    print(bbox_xywh)

                    print('boxes =')
                    print(type(boxes))
                    print(boxes)

                    print('cls_conf =')
                    print(type(cls_conf))
                    print(cls_conf)

                    print('conf =')
                    print(type(conf))
                    print(conf)

                    print('cls_ids =')
                    print(type(cls_ids))
                    print(cls_ids)

                    print('classes =')
                    print(type(classes))
                    print(classes)
                    time.sleep(3)
                '''
                '''
                <class 'numpy.ndarray'>
                [[      197.5         282         161         100]]
                boxes =
                <class 'numpy.ndarray'>
                [[     195.52      281.64       170.4      103.52]]
                cls_conf =
                '''
            else:
                bbox_xywh, cls_conf, cls_ids = self.detector(im)

            



            '''
            bbox_xywh =
            <class 'numpy.ndarray'>
            [[         23       189.5          44          27]
            [        282       183.5          48          25]
            [        118         379         210         154]
            [      169.5       168.5          15          17]
            [      556.5       250.5           9          35]
            [        143       171.5           6          21]]
            cls_conf =
            <class 'numpy.ndarray'>
            [    0.81592     0.78906     0.54248     0.45288     0.35474     0.32983]
            cls_ids =
            <class 'numpy.ndarray'>
            [          2           2           2           2           4           3]
            '''
            



            #20220504 新camera7會背影誤抓成大車，需要標記該影片重新訓練模型才能徹底改善
            #由於誤抓範圍超過整個螢幕，這邊暫時加個過濾：
            #如果目標偵測框面積超過畫面的一半，就過濾掉
            for i, box in enumerate(bbox_xywh):
                if box[2] * box[3] > self.helf_im_wh:
                    cls_ids[i] = -1

            # select person class
            #mask1 = cls_ids == 2
            #mask2 = cls_ids == 3
            #mask3 = cls_ids == 5
            mask1 = cls_ids == SmallCarID
            mask2 = cls_ids == MotoBikeID
            mask3 = cls_ids == BigCarID
            if not IS_DAXING_CLASS:
                mask3 += cls_ids == ExtraBigCarID
            mask4 = cls_ids == PeopleID
            
            
            bbox_xywh1 = bbox_xywh[mask1]
            outputs_one_frame = []
            #之前版本，畫面中一個框框都沒有時會error，修正
            if len(bbox_xywh) > 0:
                
                whRate = 1.2
            
                bbox_xywh1[:, 3:] *= whRate
                bbox_xywh2 = bbox_xywh[mask2]
                bbox_xywh2[:, 3:] *= whRate
                bbox_xywh3 = bbox_xywh[mask3]
                bbox_xywh3[:, 3:] *= whRate
                bbox_xywh4 = bbox_xywh[mask4]
                bbox_xywh4[:, 3:] *= whRate
                #print(cls_conf)
                #print(cls_ids)
                cls_conf1 = cls_conf[mask1]
                cls_conf2 = cls_conf[mask2]
                cls_conf3 = cls_conf[mask3]
                cls_conf4 = cls_conf[mask4]
    
                #print(bbox_xywh)
                #print(cls_conf)
                #print(cls_ids)
    
    
                # do tracking
                outputs1 = self.deepsort1.update(bbox_xywh1, cls_conf1, im)
                outputs2 = self.deepsort2.update(bbox_xywh2, cls_conf2, im)
                outputs3 = self.deepsort3.update(bbox_xywh3, cls_conf3, im)
                outputs4 = self.deepsort4.update(bbox_xywh4, cls_conf4, im)
                
                #測試結果，deepsort要的xywh，x和y是中心點座標，不是左上角
                '''
                im2 = ori_im.copy()
                for box in bbox_xywh:
                    #print(box)
                    x1 = int(box[0] - box[2] / 2)
                    y1 = int(box[1] - box[3] / 2)
                    x2 = int(box[0] + box[2] / 2)
                    y2 = int(box[1] + box[3] / 2)
                    x1 = int(x1)
                    x2 = int(x2)
                    y1 = int(y1)
                    y2 = int(y2)
                    #print("=====")
                    #print(x1)
                    #print(x2)
                    #print(y1)
                    #print(y2)
                    cv2.rectangle(ori_im, (x1, y1), (x2, y2), (0, 0, 255), 1, cv2.LINE_AA)
                #cv2.imshow("temp", im2)
                #cv2.waitKey(0)
                '''
                #print('outputs1')
                #print(outputs1)
                #print(type(outputs1))
    
                # draw boxes for visualization
                
                outputs = outputs1
                classID = SmallCarID
                if len(outputs) > 0:
                    bbox_tlwh = []
                    bbox_xyxy1 = outputs[:, :4]
                    identities = outputs[:, -1]
                    #ori_im = draw_boxes2(ori_im, bbox_xyxy1, identities, self.class_names[classID])
    
                    for bb_xyxy in bbox_xyxy1:
                        bbox_tlwh.append(self.deepsort1._xyxy_to_tlwh(bb_xyxy))
    
                    results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                    outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
    
    
                outputs = outputs2
                classID = MotoBikeID
                if len(outputs) > 0:
                    bbox_tlwh = []
                    bbox_xyxy2 = outputs[:, :4]
                    identities = outputs[:, -1]
                    #ori_im = draw_boxes2(ori_im, bbox_xyxy2, identities, self.class_names[classID])
    
                    for bb_xyxy in bbox_xyxy2:
                        bbox_tlwh.append(self.deepsort2._xyxy_to_tlwh(bb_xyxy))
    
                    results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                    outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                    
                outputs = outputs3
                classID = BigCarID
                if len(outputs) > 0:
                    bbox_tlwh = []
                    bbox_xyxy3 = outputs[:, :4]
                    identities = outputs[:, -1]
                    #ori_im = draw_boxes2(ori_im, bbox_xyxy3, identities, self.class_names[classID])
    
                    for bb_xyxy in bbox_xyxy3:
                        bbox_tlwh.append(self.deepsort3._xyxy_to_tlwh(bb_xyxy))
    
                    results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                    outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                    
                    
                outputs = outputs4
                classID = PeopleID
                if len(outputs) > 0:
                    bbox_tlwh = []
                    bbox_xyxy4 = outputs[:, :4]
                    identities = outputs[:, -1]
                    #ori_im = draw_boxes2(ori_im, bbox_xyxy4, identities, self.class_names[classID])
                    for bb_xyxy in bbox_xyxy4:
                        bbox_tlwh.append(self.deepsort4._xyxy_to_tlwh(bb_xyxy))
                    results.append((idx_frame - 1, bbox_tlwh, identities, classID))
                    outputs_one_frame.append((idx_frame - 1, bbox_tlwh, identities, classID))
                    
                    
            
                   
            
            self.checkCarList(outputs_one_frame, idx_frame, fps, frame_count, mat_inside_lane, mat_outside_lane)  
            
            
            if test_split_road:
                #20220510車道判定搬到 checkCarList 裡面，這邊只剩畫圖
                for key, car in self.carList.items():
                    if car.lastFrame == idx_frame:
                        if len(car.tracks) > 1:
                            track = car.tracks[len(car.tracks) - 1]
                            xc = track[0] + track[2] // 2
                            yc = track[1] + track[3] // 2
                            label = ""
                            if car.out_lane_type == 1:
                                label = "inside"
                                cv2.putText(ori_im, label,(xc, yc), cv2.FONT_HERSHEY_PLAIN, 1.2, [255, 255, 255], 2)
                                cv2.putText(ori_im, label,(xc, yc), cv2.FONT_HERSHEY_PLAIN, 1.2, [0, 0, 255], 1)
                            elif car.out_lane_type == 2:
                                label = "outside"
                                cv2.putText(ori_im, label,(xc, yc), cv2.FONT_HERSHEY_PLAIN, 1.2, [255, 255, 255], 2)
                                cv2.putText(ori_im, label,(xc, yc), cv2.FONT_HERSHEY_PLAIN, 1.2, [0, 255, 0], 1) 

            #20220503 雅筑回報停等車輛數失效，檢查後是上面允許type3也計算車間距後，下面的elif忘了改if
            if jsonData["type"] == 1 or jsonData["type"] == 3: #20220320新增停等車輛數
                #20220505更新：增加限制車輛在warp區域內的，才計算車間距(因為它)
                #self.getSpeed(idx_frame, warpM, warpPixelRate)#Merge acc
                self.getSpeed(idx_frame, warpM, tarW, tarH, warpPixelRate)
                self.getMaSpeed(idx_frame, int(self.fps/2)) #移動平均的窗格改為fps，也就是1秒內
                #self.getFilteredSpeed(idx_frame, 35)
                #this method kinda lagging, disabled
                '''
                print("Filtered--")
                print(self.speedFilteredDict)
                print("Filtered--")
                '''
                try:
                    self.getAccelerationSpeed(idx_frame)
                except Exception as e:
                    print(str(e))
                    # print("We Do not Enter Acc!!" + str(idx_frame))
                    pass
                if debug_draw_car_spacing:
                    #要畫debug圖用這個
                    carSpacings, warp2 = self.getCarsSpacing(idx_frame, warpM, tarW, tarH, warpPixelRate, matWarp, True)
                    cv2.imwrite(self.args.save_path + "temp/%d_carSpacing.jpg" % (idx_frame), warp2)
                else:
                    #通常用這個
                    carSpacings, _ = self.getCarsSpacing(idx_frame, warpM, tarW, tarH, warpPixelRate)
                '''
                print("FilteredACC--")
                print(self.accDict)
                print("FilteredACC--")#Merge acc end
                '''                
                
                if len(carSpacings) > 0:
                    carSpacing_OneMin.extend(carSpacings)
                    carSpacing_Total.extend(carSpacings)
                    #drawstr = str(np.mean(carSpacings))
                    #cv2.putText(matWarp, drawstr,(20, 20), cv2.FONT_HERSHEY_PLAIN, 1.2, [255,0,0], 1)
                for key, car in self.carList.items():
                    if car.lastFrame == idx_frame:
                        if len(car.tracks) > 1:
                            if car.isLeave == False and car.isReverse == False:
                                #座標抓左上角而非中心點，避免部分遮擋導致中心點偏移
                                car.nowPoint = [car.tracks[-1][0], car.tracks[-1][1]]
                                diffX = car.nowPoint[0] - car.stopPoint[0]
                                diffY = car.nowPoint[1] - car.stopPoint[1]
                                diff = diffX * diffX + diffY * diffY
                                
                                #20220409 增加反向車過濾
                                #由於攝影機架設在路口，所以正向車一定是往下開
                                #也就是說，y座標越來越小的就是反向車
                                if len(car.tracks) > 10:#路徑點數大於10才開始計算反向車
                                    if car.tracks[0][1] - car.tracks[-1][1] > 10:
                                        #起點比目前位置更下面10個pixel，判定為反向車
                                        car.isReverse = True
                                        continue
                                
                                if car.isWaiting == False: #如果車輛沒在停等
                                    #20220503發現camera8偵測框抖動有點大，移動距離判定改15平方
                                    if diff <= 225: #如果移動距離小於10，代表可能開始停等了
                                        if car.waitSec == 0:
                                            car.stopPoint = car.nowPoint #如果是剛開始停等，紀錄座標點
                                        car.waitSec += 1 / float(self.fps)
                                        if car.waitSec > 1:
                                            car.isWaiting = True #超過1秒幾乎沒在移動，判定為停等車
                                    else:
                                        #如果距離大於100，更新最後的停等參考點
                                        car.stopPoint = car.nowPoint
                                else: #car.isWaiting == True # 車輛停等中
                                    if diff > 225: #如果移動距離大於10，代表開始移動了，停等結束
                                        car.isWaiting = False
                                        if car.waitSec > 1:
                                            #如果這一輪停等超過1秒，把這一輪停等時間加到total
                                            car.totalWaitSec += car.waitSec 
                                        car.waitSec = 0 #清空這一輪的停等計時
                                        car.stopPoint = car.nowPoint #更新最後的停等參考點
                                    else:
                                        car.waitSec += 1 / float(self.fps)
                


            #self.checkArea(ori_im, outputs_one_frame)
            
            ori_im = self.drawCarList(ori_im, idx_frame)             
            #ori_im = self.test_drawLossCar(ori_im, idx_frame)
            
            #show(self.trackMap, "tracks")
            #show(ori_im, "videos")
            #cv2.waitKey(15)
            
            

                
            
            #畫熱區螢幕太亂...改為只畫前100 frame
            #20210728修改熱區判定方式，目前比較不亂可以畫
            #if idx_frame < 100:
            #ori_im = cv2.add(self.hotAreasMat, ori_im)
            
            
            new_im = np.zeros((ori_im.shape[0], ori_im.shape[1] + self.infoSpaceWidth, ori_im.shape[2]), np.uint8)
            new_im[0:ori_im.shape[0], 0:ori_im.shape[1], :] = ori_im;
            ori_im = new_im
            
            
            # 20231002 畫一下路口點的座標
            '''
            textSize = 2
            tarp = road_points[0]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'R', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            tarp = road_points[1]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'L', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            tarp = road_points[2]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'U', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            tarp = road_points[3]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'D', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            
            tarp = road_end_points[0]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'RE', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            tarp = road_end_points[1]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'LE', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            tarp = road_end_points[2]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'UE', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            tarp = road_end_points[3]
            tx = int(tarp[0])
            ty = int(tarp[1])
            cv2.putText(ori_im, 'DE', (tx, ty), cv2.FONT_HERSHEY_PLAIN, textSize, [0, 255, 255], 2)
            '''
            
            
            
            
            if jsonData["type"] == 2: #十字路口才需要統計轉彎數量
                textColor = [0, 0, 0]
                textColorB = [0, 255, 255]
                textSize = 1
                
                tx = self.im_width + 10
                ty = 20
                
                px = tx
                py = ty
                text = "Road 1(Right in):"
                cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "Small Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[0][2][0])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[0][0][0])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[0][1][0])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[0][4][0])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                
                px = tx
                py = ty + 80
                text = "Big Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[0][2][2])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[0][0][2])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[0][1][2])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[0][4][2])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                px = tx
                py = ty + 160
                text = "Motorbike:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[0][2][1])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[0][0][1])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[0][1][1])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[0][4][1])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "twoPartLeft: %s"  % showCountByLane(self.roads[0][3][1])
                cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                
                tx = self.im_width + 10
                ty = 300
                if self.im_height < 500: #20220225 新camera是寬畫面，高度太短output的字會出界，要把infoSpaceWidth加寬
                    tx = self.im_width + 180
                    ty = 20
                
                
                px = tx
                py = ty
                text = "Road 2(Left in):"
                cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "Small Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[1][2][0])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[1][0][0])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[1][1][0])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[1][4][0])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                
                px = tx
                py = ty + 80
                text = "Big Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[1][2][2])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[1][0][2])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[1][1][2])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[1][4][2])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                px = tx
                py = ty + 160
                text = "Motorbike:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[1][2][1])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[1][0][1])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[1][1][1])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[1][4][1])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "twoPartLeft: %s"  % showCountByLane(self.roads[1][3][1])
                cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
    
                tx = self.im_width + 180
                ty = 20
                if self.im_height < 500: #20220225 新camera是寬畫面，高度太短output的字會出界，要把infoSpaceWidth加寬
                    tx = self.im_width + 350
                    ty = 20
                px = tx
                py = ty
                text = "Road 3(Up in):"
                cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "Small Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[2][2][0])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[2][0][0])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[2][1][0])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[2][4][0])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                px = tx
                py = ty + 80
                text = "Big Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[2][2][2])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[2][0][2])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[2][1][2])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[2][4][2])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                px = tx
                py = ty + 160
                text = "Motorbike:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[2][2][1])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[2][0][1])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[2][1][1])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[2][4][1])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "twoPartLeft: %s"  % showCountByLane(self.roads[2][3][1])
                cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                tx = self.im_width + 180
                ty = 300
                if self.im_height < 500: #20220225 新camera是寬畫面，高度太短output的字會出界，要把infoSpaceWidth加寬
                    tx = self.im_width + 520
                    ty = 20
                px = tx
                py = ty
                text = "Road 4(Down in):"
                cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "Small Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[3][2][0])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[3][0][0])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[3][1][0])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[3][4][0])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                px = tx
                py = ty + 80
                text = "Big Car:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[3][2][2])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[3][0][2])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[3][1][2])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[3][4][2])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                px = tx
                py = ty + 160
                text = "Motorbike:"
                cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnRight: %s"  % showCountByLane(self.roads[3][2][1])
                cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "straight: %s"  % showCountByLane(self.roads[3][0][1])
                cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnLeft: %s"  % showCountByLane(self.roads[3][1][1])
                cv2.putText(ori_im, text,(px, py+60), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "turnBack: %s"  % showCountByLane(self.roads[3][4][1])
                cv2.putText(ori_im, text,(px, py+75), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                text = "twoPartLeft: %s"  % showCountByLane(self.roads[3][3][1])
                cv2.putText(ori_im, text,(px, py+90), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                
                
            elif jsonData["type"] == 3 or jsonData["type"] == 1:
                #20220323 停等車統計，顯示
                #20220510 內外車道分別統計，統計值改用list，0放內車道，1放外車道
                smallCar_total = [0, 0] #計算總共幾台小車
                motorBike_total = [0, 0] #計算總共幾台機車
                bigCar_total = [0, 0] #計算總共幾台大車
                
                smallCar_isWaiting = [0, 0] #計算有幾台小車正在停等中
                motorBike_isWaiting = [0, 0] #計算有幾台機車正在停等中
                bigCar_isWaiting = [0, 0] #計算有幾台大車正在停等中
                
                smallCar_totalWaitTime = [0, 0] #記錄所有小車停等秒數的加總
                motorBike_totalWaitTime = [0, 0] #記錄所有機車停等秒數的加總
                bigCar_totalWaitTime = [0, 0] #記錄所有大車停等秒數的加總
                
                smallCar_avgWaitTime = [0, 0] #記錄小車平均停等秒數
                motorBike_avgWaitTime = [0, 0] #記錄機車平均停等秒數
                bigCar_avgWaitTime = [0, 0] #記錄大車平均停等秒數
                if test_split_road:
                    for key, car in self.carList.items(): #對於每輛車
                        #20220409增加反向車過濾
                        if car.isReverse:
                            continue
                        if len(car.tracks) > int(self.fps):#發現有斷掉的短軌跡被誤統計，改為軌跡長度大於10才統計
                            target_lane_index = 0
                            if car.out_lane_type == 2:
                                target_lane_index = 1
                            if car.out_lane_type == -1:
                                continue #若車輛不在車道範圍就不計算
                            if car.carType == SmallCarID: #如果車種是小車
                                smallCar_total[target_lane_index] += 1 #小車總數加1
                                if car.isWaiting: #如果該車子停等中
                                    smallCar_isWaiting[target_lane_index] += 1 #停等中的小車，數輛加一
                                smallCar_totalWaitTime[target_lane_index] += (car.totalWaitSec + car.waitSec) #把該車輛的停等時間加總出來
                            elif car.carType == MotoBikeID: #同上，這邊做機車
                                motorBike_total[target_lane_index] += 1
                                if car.isWaiting:
                                    motorBike_isWaiting[target_lane_index] += 1
                                motorBike_totalWaitTime[target_lane_index] += (car.totalWaitSec + car.waitSec)
                            elif car.carType == BigCarID: #同上，這邊做大車
                                bigCar_total[target_lane_index] += 1
                                if car.isWaiting:
                                    bigCar_isWaiting[target_lane_index] += 1
                                bigCar_totalWaitTime[target_lane_index] += (car.totalWaitSec + car.waitSec)
                                                
                    for target_lane_index in range(2):
                        if smallCar_total[target_lane_index] > 0: #計算平均停等時間，很單純就是總停等時間除以車輛數
                            smallCar_avgWaitTime[target_lane_index] = smallCar_totalWaitTime[target_lane_index] / (float)(smallCar_total[target_lane_index])
                        if motorBike_total[target_lane_index] > 0:
                            motorBike_avgWaitTime[target_lane_index] = motorBike_totalWaitTime[target_lane_index] / (float)(motorBike_total[target_lane_index])
                        if bigCar_total[target_lane_index] > 0:
                            bigCar_avgWaitTime[target_lane_index] = bigCar_totalWaitTime[target_lane_index] / (float)(bigCar_total[target_lane_index])
                    
                    
                    textColorB = [0, 255, 255]
                    textSize = 1
                    
                    tx = self.im_width + 10
                    ty = 20
                    
                    px = tx
                    py = ty
                    
                    text = "Small Car (inside, outside):" #把小車的總數輛、停等中數量、平均停等時間show出來
                    cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "Total: %d, %d" % (smallCar_total[0], smallCar_total[1])
                    cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "isWaiting: %d, %d"  % (smallCar_isWaiting[0], smallCar_isWaiting[1])
                    cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "avgWaitTime: %.2f, %.2f sec" % (smallCar_avgWaitTime[0], smallCar_avgWaitTime[1])
                    cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    
                    py = ty + 100
                    text = "Big Car (inside, outside):" #把大車的總數輛、停等中數量、平均停等時間show出來
                    cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "Total: %d, %d" % (bigCar_total[0], bigCar_total[1])
                    cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "isWaiting: %d, %d"  % (bigCar_isWaiting[0], bigCar_isWaiting[1])
                    cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "avgWaitTime: %.2f, %.2f sec" % (bigCar_avgWaitTime[0], bigCar_avgWaitTime[1])
                    cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    
                    
                    py = ty + 200
                    text = "MotorBike (inside, outside):" #把機車的總數輛、停等中數量、平均停等時間show出來
                    cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "Total: %d, %d" % (motorBike_total[0], motorBike_total[1])
                    cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "isWaiting: %d, %d"  % (motorBike_isWaiting[0], motorBike_isWaiting[1])
                    cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "avgWaitTime: %.2f, %.2f sec" % (motorBike_avgWaitTime[0], motorBike_avgWaitTime[1])
                    cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                else:
                    for key, car in self.carList.items(): #對於每輛車
                        #20220409增加反向車過濾
                        if car.isReverse:
                            continue
                        if len(car.tracks) > int(self.fps):#發現有斷掉的短軌跡被誤統計，改為軌跡長度大於10才統計
                            if car.carType == SmallCarID: #如果車種是小車
                                smallCar_total[0] += 1 #小車總數加1
                                if car.isWaiting: #如果該車子停等中
                                    smallCar_isWaiting[0] += 1 #停等中的小車，數輛加一
                                smallCar_totalWaitTime[0] += (car.totalWaitSec + car.waitSec) #把該車輛的停等時間加總出來
                            elif car.carType == MotoBikeID: #同上，這邊做機車
                                motorBike_total[0] += 1
                                if car.isWaiting:
                                    motorBike_isWaiting[0] += 1
                                motorBike_totalWaitTime[0] += (car.totalWaitSec + car.waitSec)
                            elif car.carType == BigCarID: #同上，這邊做大車
                                bigCar_total[0] += 1
                                if car.isWaiting:
                                    bigCar_isWaiting[0] += 1
                                bigCar_totalWaitTime[0] += (car.totalWaitSec + car.waitSec)
                            
                    
                                
                    
                    
                    if smallCar_total[0] > 0: #計算平均停等時間，很單純就是總停等時間除以車輛數
                        smallCar_avgWaitTime[0] = smallCar_totalWaitTime[0] / (float)(smallCar_total[0])
                    if motorBike_total[0] > 0:
                        motorBike_avgWaitTime[0] = motorBike_totalWaitTime[0] / (float)(motorBike_total[0])
                    if bigCar_total[0] > 0:
                        bigCar_avgWaitTime[0] = bigCar_totalWaitTime[0] / (float)(bigCar_total[0])
                    
                    
                    textColorB = [0, 255, 255]
                    textSize = 1
                    
                    tx = self.im_width + 10
                    ty = 20
                    
                    px = tx
                    py = ty
                    
                    text = "Small Car:" #把小車的總數輛、停等中數量、平均停等時間show出來
                    cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "Total: %d" % (smallCar_total[0])
                    cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "isWaiting: %d"  % (smallCar_isWaiting[0])
                    cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "avgWaitTime: %.2f sec" % (smallCar_avgWaitTime[0])
                    cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    
                    py = ty + 100
                    text = "Big Car:" #把大車的總數輛、停等中數量、平均停等時間show出來
                    cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "Total: %d" % (bigCar_total[0])
                    cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "isWaiting: %d"  % (bigCar_isWaiting[0])
                    cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "avgWaitTime: %.2f sec" % (bigCar_avgWaitTime[0])
                    cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    
                    
                    py = ty + 200
                    text = "MotorBike:" #把機車的總數輛、停等中數量、平均停等時間show出來
                    cv2.putText(ori_im, text,(px, py), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "Total: %d" % (motorBike_total[0])
                    cv2.putText(ori_im, text,(px, py+15), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "isWaiting: %d"  % (motorBike_isWaiting[0])
                    cv2.putText(ori_im, text,(px, py+30), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
                    text = "avgWaitTime: %.2f sec" % (motorBike_avgWaitTime[0])
                    cv2.putText(ori_im, text,(px, py+45), cv2.FONT_HERSHEY_PLAIN, textSize, textColorB, 2)
            
            
            for key, car in self.carList.items():#Merge acc
                if car.lastFrame == idx_frame:
                    track = car.tracks[-1]
                    className = self.class_names[car.carType]
                    x = track[0] + track[2]/2
                    y = track[1] + track[3]/2
                    if className in self.speedMaDict:
                        if key in self.speedMaDict[className]:
                            if len(self.speedMaDict[className][key]) > 0:
                                if self.speedMaDict[className][key][-1][0] == idx_frame:
                                    #print(self.speedMaDict[className][key][-1][0], "frame")
                                    cv2.circle(ori_im, (int(x), int(y)), 5, (255, 160, 200), -1)
                                    putOnText = "%.1f" % self.speedMaDict[className][key][-1][1]
                                    #print(self.speedMaDict[className][key][-1][1], "MaSpeed")
                                    cv2.putText(ori_im, putOnText,(int(x), int(y)), cv2.FONT_HERSHEY_PLAIN, 1.5, (100, 255, 130), 1, cv2.LINE_AA)
                                    if className in self.accDict:
                                        if key in self.accDict[className]:
                                            if len(self.accDict[className][key]) > 0:
                                                if self.accDict[className][key][-1][0] == idx_frame:
                                                    #print(self.accDict[className][key][-1][0], "frame")
                                                    putOnText = "%.1f" % self.accDict[className][key][-1][1]
                                                    #print(self.accDict[className][key][-1][1], "acc")
                                                    cv2.putText(ori_im, putOnText,(int(x), int(y)+10), cv2.FONT_HERSHEY_PLAIN, 1.5, (100, 255, 130), 1, cv2.LINE_AA)
                                                    #Merge acc end
                                    

            

            end = time.time()
            
            
            #cv2.imshow("test", ori_im)
            #cv2.waitKey(0)

            if self.args.display:
                cv2.imshow("test", ori_im)
                cv2.waitKey(1)

            if self.args.save_path:
                if debug_output_video:
                    self.writer.write(ori_im)
                

            
            

            # logging
            self.logger.info("frame: {}, time: {:.03f}s, fps: {:.03f}, detection numbers: {}, tracking numbers: {}" \
                            .format(idx_frame, end - start, 1 / (end - start), bbox_xywh.shape[0], len(outputs_one_frame)))
                
            #20211015 依會議討論交通量改為每分鐘輸出
            #20220324 發現有影片fps不是整數....修正公式加上強制型態轉換
            #20220505 切分上限改為4，當分段為每15分鐘，影片全長100分鐘時，第一段0~15，然後16~30、31~45、46到100
            #if (idx_frame % ((int)(fps * 60 * 15)) == 0) or (idx_frame == frame_count):
            if ((idx_frame % ((int)(fps * 60 * self.timeStoreSegment)) == 0) and 
                (idx_frame // ((int)(fps * 60 * self.timeStoreSegment)) < 4)) or (idx_frame == frame_count):
                #20211014 依昨天會議討論，改為每分鐘輸出一次json
                #把交通量寫入json
                #standardCounting
                bigCarNum = 0
                smallCarNum = 0
                motorbikeNum = 0
                peopleNum = 0
                for key, car in self.carList.items():
                    if car.isCounted:
                        continue
                    if car.carType == SmallCarID:
                        smallCarNum += 1
                    elif car.carType == MotoBikeID:
                        motorbikeNum += 1
                    elif car.carType == BigCarID:
                        bigCarNum += 1
                    elif car.carType == PeopleID:
                        peopleNum += 1
                jsonData["standardCounting"]["bigCarNum"] = bigCarNum
                jsonData["standardCounting"]["smallCarNum"] = smallCarNum
                jsonData["standardCounting"]["motorbikeNum"] = motorbikeNum
                jsonData["standardCounting"]["peopleNum"] = peopleNum
                
                #turnCounting
                #應該有4個路口，右 straight_R 左 straight_L 上 cross_U 下 cross_D
                #20211012 增加迴轉車 turnBack
                #20220510 車道區分
                jsonData["turnCounting"]["straight_R"]["smallCar"]["straight"] = getCountByLane(self.temproads[0][0][0])
                jsonData["turnCounting"]["straight_R"]["smallCar"]["turnLeft"] = getCountByLane(self.temproads[0][1][0])
                jsonData["turnCounting"]["straight_R"]["smallCar"]["turnRight"] = getCountByLane(self.temproads[0][2][0])
                jsonData["turnCounting"]["straight_R"]["smallCar"]["turnBack"] = getCountByLane(self.temproads[0][4][0])         
                jsonData["turnCounting"]["straight_R"]["motorbike"]["straight"] = getCountByLane(self.temproads[0][0][1])
                jsonData["turnCounting"]["straight_R"]["motorbike"]["turnLeft"] = getCountByLane(self.temproads[0][1][1])
                jsonData["turnCounting"]["straight_R"]["motorbike"]["turnRight"] = getCountByLane(self.temproads[0][2][1])
                jsonData["turnCounting"]["straight_R"]["motorbike"]["turnBack"] = getCountByLane(self.temproads[0][4][1])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["straight"] = getCountByLane(self.temproads[0][0][2])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["turnLeft"] = getCountByLane(self.temproads[0][1][2])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["turnRight"] = getCountByLane(self.temproads[0][2][2])
                jsonData["turnCounting"]["straight_R"]["bigCar"]["turnBack"] = getCountByLane(self.temproads[0][4][2])
                
                jsonData["turnCounting"]["straight_L"]["smallCar"]["straight"] = getCountByLane(self.temproads[1][0][0])
                jsonData["turnCounting"]["straight_L"]["smallCar"]["turnLeft"] = getCountByLane(self.temproads[1][1][0])
                jsonData["turnCounting"]["straight_L"]["smallCar"]["turnRight"] = getCountByLane(self.temproads[1][2][0])
                jsonData["turnCounting"]["straight_L"]["smallCar"]["turnBack"] = getCountByLane(self.temproads[1][4][0])        
                jsonData["turnCounting"]["straight_L"]["motorbike"]["straight"] = getCountByLane(self.temproads[1][0][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["turnLeft"] = getCountByLane(self.temproads[1][1][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["turnRight"] = getCountByLane(self.temproads[1][2][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["turnBack"] = getCountByLane(self.temproads[1][4][1]) 
                jsonData["turnCounting"]["straight_L"]["bigCar"]["straight"] = getCountByLane(self.temproads[1][0][2])
                jsonData["turnCounting"]["straight_L"]["bigCar"]["turnLeft"] = getCountByLane(self.temproads[1][1][2])
                jsonData["turnCounting"]["straight_L"]["bigCar"]["turnRight"] = getCountByLane(self.temproads[1][2][2])
                jsonData["turnCounting"]["straight_L"]["bigCar"]["turnBack"] = getCountByLane(self.temproads[1][4][2]) 
                
                jsonData["turnCounting"]["cross_U"]["smallCar"]["straight"] = getCountByLane(self.temproads[2][0][0])
                jsonData["turnCounting"]["cross_U"]["smallCar"]["turnLeft"] = getCountByLane(self.temproads[2][1][0])
                jsonData["turnCounting"]["cross_U"]["smallCar"]["turnRight"] = getCountByLane(self.temproads[2][2][0])
                jsonData["turnCounting"]["cross_U"]["smallCar"]["turnBack"] = getCountByLane(self.temproads[2][4][0])        
                jsonData["turnCounting"]["cross_U"]["motorbike"]["straight"] = getCountByLane(self.temproads[2][0][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["turnLeft"] = getCountByLane(self.temproads[2][1][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["turnRight"] = getCountByLane(self.temproads[2][2][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["turnBack"] = getCountByLane(self.temproads[2][4][1])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["straight"] = getCountByLane(self.temproads[2][0][2])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["turnLeft"] = getCountByLane(self.temproads[2][1][2])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["turnRight"] = getCountByLane(self.temproads[2][2][2])
                jsonData["turnCounting"]["cross_U"]["bigCar"]["turnBack"] = getCountByLane(self.temproads[2][4][2])
                      
                jsonData["turnCounting"]["cross_D"]["smallCar"]["straight"] = getCountByLane(self.temproads[3][0][0])
                jsonData["turnCounting"]["cross_D"]["smallCar"]["turnLeft"] = getCountByLane(self.temproads[3][1][0])
                jsonData["turnCounting"]["cross_D"]["smallCar"]["turnRight"] = getCountByLane(self.temproads[3][2][0])   
                jsonData["turnCounting"]["cross_D"]["smallCar"]["turnBack"] = getCountByLane(self.temproads[3][4][0])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["straight"] = getCountByLane(self.temproads[3][0][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["turnLeft"] = getCountByLane(self.temproads[3][1][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["turnRight"] = getCountByLane(self.temproads[3][2][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["turnBack"] = getCountByLane(self.temproads[3][4][1])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["straight"] = getCountByLane(self.temproads[3][0][2])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["turnLeft"] = getCountByLane(self.temproads[3][1][2])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["turnRight"] = getCountByLane(self.temproads[3][2][2])
                jsonData["turnCounting"]["cross_D"]["bigCar"]["turnBack"] = getCountByLane(self.temproads[3][4][2])
                
                #20211012 兩段式左轉
                jsonData["turnCounting"]["straight_R"]["motorbike"]["hookTurn"] = getCountByLane(self.temproads[0][3][1])
                jsonData["turnCounting"]["straight_L"]["motorbike"]["hookTurn"] = getCountByLane(self.temproads[1][3][1])
                jsonData["turnCounting"]["cross_U"]["motorbike"]["hookTurn"] = getCountByLane(self.temproads[2][3][1])
                jsonData["turnCounting"]["cross_D"]["motorbike"]["hookTurn"] = getCountByLane(self.temproads[3][3][1])
                
                
                #20211229 車間距
                if jsonData["type"] == 1 or jsonData["type"] == 3:
                    avgCarSpacing = 0
                    if len(carSpacing_OneMin) > 0:
                        avgCarSpacing = np.mean(carSpacing_OneMin)
                    jsonData["extraCounting"]["averageCarSpacing"] = avgCarSpacing
                    
                    
                    carSpacing_OneMin = []
                
                    #20220223 安全距離
                    with open(self.args.save_path + str(cameraNo) + ("_%s_" % videoStartTime_ori) +'axbx.csv', 'w', newline='') as csvfile:
                        writer = csv.writer(csvfile)
                        writer.writerow(['i', 'ax', 'bx_add', 'bx_mult', 'minD', 'maxD'])
                        for i in range(len(self.carSpacings_D_OneMin)):
                            oneMin_D = self.carSpacings_D_OneMin[i]
                            oneMin_V = self.carSpacings_V_OneMin[i]
                            
                            
                            
                            # d = ax + (bx_add + bx_mult*z) * sqrt(v)
                            ax = 2.1
                            if i == 1: #機車的AX是0.4
                                ax = 0.4
                                
                            bx_add = 0
                            bx_mult = 0
                            minD = 0
                            maxD = 0
                            
                            if len(oneMin_D) == 0:
                                writer.writerow([i, ax, bx_add, bx_mult, minD, maxD])
                                self.setAXBXToJson(jsonData, i, ax, bx_add, bx_mult, minD, maxD)
                                continue
                            
                            
                            if len(oneMin_D) > 20:
                                #20220603 當樣本大於20個時，過濾5%離群值
                                findN = (int)(len(oneMin_D) * 0.05)
                                
                                minD = heapq.nsmallest(findN, oneMin_D)[-1]
                                minIndex = oneMin_D.index(minD)
                                minV = oneMin_V[minIndex]
                                
                                maxD = heapq.nlargest(findN, oneMin_D)[-1]
                                maxIndex = oneMin_D.index(maxD)
                                maxV = oneMin_V[maxIndex]
                            else:
                                minD = min(oneMin_D)
                                minIndex = oneMin_D.index(minD)
                                minV = oneMin_V[minIndex]
                                
                                maxD = max(oneMin_D)
                                maxIndex = oneMin_D.index(maxD)
                                maxV = oneMin_V[maxIndex]
                            
                            
                            if minV <= 0:
                                writer.writerow([i, ax, bx_add, bx_mult, minD, maxD])
                                self.setAXBXToJson(jsonData, i, ax, bx_add, bx_mult, minD, maxD)
                                continue
                            if minV == maxV:
                                writer.writerow([i, ax, bx_add, bx_mult, minD, maxD])
                                self.setAXBXToJson(jsonData, i, ax, bx_add, bx_mult, minD, maxD)
                                continue
                            
                            
                            # 將max(d )及min(d )分別取z值為1、0，代入公式
                            '''
                            minD，z為0
                            minD = ax + (bx_add + bx_mult*0) * sqrt(minV)
                            minD = ax + (bx_add) * sqrt(minV)
                            bx_add = (minD - ax) / (sqrt(minV))
                            '''
                            bx_add = (minD - ax) / (math.sqrt(minV))
                            
                            '''
                            maxD，z為1
                            maxD = ax + (bx_add + bx_mult*1) * sqrt(maxV)
                            (bx_add + bx_mult) = (maxD - ax) / sqrt(maxV)
                            bx_mult = (maxD - ax) / sqrt(maxV) - bx_add
                            '''
                            bx_mult = ( (maxD - ax) / math.sqrt(maxV) ) - bx_add
                            writer.writerow([i, ax, bx_add, bx_mult, minD, maxD])
                            self.setAXBXToJson(jsonData, i, ax, bx_add, bx_mult, minD, maxD)
                                
                        
                            
                    '''
                    print(self.carSpacings_D_OneMin)
                    print(self.carSpacings_V_OneMin)
                    print(jsonData["extraCounting"]["smallCar"]["ax"])
                    print(jsonData["extraCounting"]["smallCar"]["bx_add"])
                    print(jsonData["extraCounting"]["smallCar"]["bx_mult"])
                    print(jsonData["extraCounting"]["motorbike"]["ax"])
                    print(jsonData["extraCounting"]["motorbike"]["bx_add"])
                    print(jsonData["extraCounting"]["motorbike"]["bx_mult"])
                    '''
                    
                    self.carSpacings_D_OneMin = [[],[],[]]
                    self.carSpacings_V_OneMin = [[],[],[]]
                    
                    
                    #20220506 增加速度輸出，正反向道路分開統計
                    
                    carTypeLabel = {"MotorBike":0, "SmallCar":1, "BigCar":2}
                    carSpeedList_positive = [[], [], []]
                    carSpeedList_negative = [[], [], []]
                    for carType, value in self.speedDict_OneQuarter.items():
                        #print(carType)
                        if carType not in carTypeLabel:
                            continue
                        #print(value)
                        for carKey, speedTupel in value.items():
                            #print(carKey)
                            #print(speedTupel)
                            speedList = [x[2] for x in speedTupel]
                            #print(speedList)
                            #然後，停等狀態不要算成樣本， 我算速度絕對值小於 3 的是停等
                            speedList = [x for x in speedList if (abs(x) > 3)]
                            #print(np.average(speedList))
                            if len(speedList) > 0:
                                #avgSpeedOneCar = np.average(speedList)
                                #取用平均值還是會低估，因為同一台車的採樣點，也是開越慢的時候樣本越多
                                #嘗試改用75分位數
                                speedList.sort(key = lambda x: abs(x))
                                avgSpeedOneCar = speedList[int(len(speedList) * 0.75)]
                            else:
                                continue
                                
                            if avgSpeedOneCar > 0:
                                carSpeedList_positive[carTypeLabel[carType]].append(avgSpeedOneCar)
                            else:
                                carSpeedList_negative[carTypeLabel[carType]].append(avgSpeedOneCar)
                                
                    #print(carSpeedList_positive)
                    #print(carSpeedList_negative)
                    
                    carSpeedList_positive_avg = [0, 0, 0]
                    carSpeedList_negative_avg = [0, 0, 0]
                    
                    for i in range(len(carSpeedList_positive_avg)):
                        if len(carSpeedList_positive[i]) > 0:
                            carSpeedList_positive_avg[i] = np.average(carSpeedList_positive[i])
                        if len(carSpeedList_negative[i]) > 0:
                            carSpeedList_negative_avg[i] = np.abs(np.average(carSpeedList_negative[i]))
                    
                    jsonData["extraCounting"]["motorbike"]["speed_positive"] = carSpeedList_positive_avg[0]
                    jsonData["extraCounting"]["motorbike"]["speed_negative"] = carSpeedList_negative_avg[0]
                    jsonData["extraCounting"]["smallCar"]["speed_positive"] = carSpeedList_positive_avg[1]
                    jsonData["extraCounting"]["smallCar"]["speed_negative"] = carSpeedList_negative_avg[1]
                    jsonData["extraCounting"]["bigCar"]["speed_positive"] = carSpeedList_positive_avg[2]
                    jsonData["extraCounting"]["bigCar"]["speed_negative"] = carSpeedList_negative_avg[2]
                    
                    self.speedDict_OneQuarter = {}
                    
                    
                    def Percentile75thAccSpeed(AccList): #Merge acc
                        return int(len(AccList)*0.75)
                    def negtiveOne(AccList):
                        return -1
                    def Zero(AccList):
                        return 0
                    #accelerationCounting
                    indicationLabelIndexes = ['accelerationCountingOverall','nagativeaccelerationCountingOverall']
                    percentageAccIndexes = ['minAccSpeed', 'maxAccSpeed', '75thPercentileAccSpeed']
                    percentageNegAccIndexes = ['minNegAccSpeed', 'maxNegAccSpeed', '75thPercentileNegAccSpeed']
                    percentageAccIndexesToNum = {'minAccSpeed':Zero, 'maxAccSpeed':negtiveOne, '75thPercentileAccSpeed':Percentile75thAccSpeed}
                    percentageNegAccIndexesToNum = {'minNegAccSpeed':Zero, 'maxNegAccSpeed':negtiveOne, '75thPercentileNegAccSpeed':Percentile75thAccSpeed}
                    accOverallDict = {}
                    negAccOverallDict = {}
                    for objectType, objectList in self.accDictQuarter[outputJsonTimes-1].items():
                        self.accDictQuarter[outputJsonTimes-1][objectType] = sorted(objectList,key=lambda tup: tup[1])
                        if objectType not in accOverallDict:
                            accOverallDict[objectType] = {}
                        
                        for accIndex in percentageAccIndexes:
                            if accIndex not in accOverallDict[objectType]:
                                #self.accDictQuarter[outputJsonTimes-1][objectType][percentageAccIndexesToNum[accIndex](self.accDictQuarter[outputJsonTimes-1][objectType])][1]
                                '''
                                print(len(self.accDictQuarter)) #4
                                print(outputJsonTimes-1) #0
                                print(self.accDictQuarter) #[{'MotorBike': []}, {}, {}, {'MotorBike': []}]
                                print("=====")
                                print(self.accDictQuarter[outputJsonTimes-1]) #{'MotorBike': []}
                                print(len(self.accDictQuarter[outputJsonTimes-1])) #1
                                print(objectType) #MotorBike
                                print("=====")
                                print(percentageAccIndexesToNum[accIndex](self.accDictQuarter[outputJsonTimes-1][objectType]))#0
                                print(self.accDictQuarter[outputJsonTimes-1][objectType]) # []
                                '''
                                
                                #20220505 這邊有可能沒樣本然後 IndexError: list index out of range，修正
                                if len(self.accDictQuarter[outputJsonTimes-1][objectType]) == 0:
                                    accOverallDict[objectType][accIndex] = 0
                                else:
                                    #print(self.accDictQuarter[outputJsonTimes-1][objectType][percentageAccIndexesToNum[accIndex](self.accDictQuarter[outputJsonTimes-1][objectType])])
                                    accOverallDict[objectType][accIndex] = self.accDictQuarter[outputJsonTimes-1][objectType][percentageAccIndexesToNum[accIndex](self.accDictQuarter[outputJsonTimes-1][objectType])][1]

                    for objectType, objectList in self.negAccDictQuarter[outputJsonTimes-1].items():
                        self.negAccDictQuarter[outputJsonTimes-1][objectType] = sorted(objectList,key=lambda tup: tup[1])
                        if objectType not in negAccOverallDict:
                            negAccOverallDict[objectType] = {}
                        for negAccIndex in percentageNegAccIndexes:
                            if negAccIndex not in negAccOverallDict[objectType]:
                                #20220505 這邊有可能沒樣本然後 IndexError: list index out of range，修正
                                if len(self.negAccDictQuarter[outputJsonTimes-1][objectType]) == 0:
                                    negAccOverallDict[objectType][negAccIndex] = 0
                                else:
                                    negAccOverallDict[objectType][negAccIndex] = self.negAccDictQuarter[outputJsonTimes-1][objectType][percentageNegAccIndexesToNum[negAccIndex](self.negAccDictQuarter[outputJsonTimes-1][objectType])][1]

                    jsonData[indicationLabelIndexes[0]] = accOverallDict
                    jsonData[indicationLabelIndexes[1]] = negAccOverallDict
                    jsonData['speedToAccCSV'] = self.speedLevelToAccCsv.getvalue()
                    jsonData['speedToNegAccCSV'] = self.speedLevelToNegAccCsv.getvalue() #Merge acc end
                    
                    #,'speedToAccCSV','speedToNegAccCSV'

                
                
                
                if jsonData["type"] == 1 or jsonData["type"] == 3:#現在Type1也要記錄停等車
                    #20220409統計每15分鐘的停等車輛數並作為json回傳
                    #停等車輛定義為在這15分鐘期間，停等超過5秒以上的車輛
                    bigCarWaitNum = [0, 0]
                    smallCarWaitNum = [0, 0]
                    motorbikeWaitNum = [0, 0]
                    for key, car in self.carList.items():
                        if car.isCounted:
                            continue
                        
                        if car.totalWaitSec + car.waitSec > 5:#停等超過5秒
                            idx = 0
                            if test_split_road:
                                if car.out_lane_type == 2:
                                    idx = 1
                            
                            if car.carType == SmallCarID:
                                smallCarWaitNum[idx] += 1
                            elif car.carType == MotoBikeID:
                                motorbikeWaitNum[idx] += 1
                            elif car.carType == BigCarID:
                                bigCarWaitNum[idx] += 1
                    #waitingCount
                    #20220510車道區分
                    if test_split_road:
                        jsonData["extraCounting"]["smallCar"]["waitingCount"] = [smallCarWaitNum[0], smallCarWaitNum[1]]
                        jsonData["extraCounting"]["motorbike"]["waitingCount"] = [motorbikeWaitNum[0], motorbikeWaitNum[1]]
                        jsonData["extraCounting"]["bigCar"]["waitingCount"] = [bigCarWaitNum[0], bigCarWaitNum[1]]
                    else:
                        jsonData["extraCounting"]["smallCar"]["waitingCount"] = smallCarWaitNum[0]
                        jsonData["extraCounting"]["motorbike"]["waitingCount"] = motorbikeWaitNum[0]
                        jsonData["extraCounting"]["bigCar"]["waitingCount"] = bigCarWaitNum[0]
                
                
                #20211005 停等時間
                #20211006 更新，平均停等時間似乎還要依照車種分開計算
                #20211124 更新，平均停等時間還得依轉向分開
                '''
                waitTime_R = [0, 0, 0, 0]
                waitTime_L = [0, 0, 0, 0]
                waitTime_U = [0, 0, 0, 0]
                waitTime_D = [0, 0, 0, 0]
                carNum_R = [0, 0, 0, 0]
                carNum_L = [0, 0, 0, 0]
                carNum_U = [0, 0, 0, 0]
                carNum_D = [0, 0, 0, 0]
                tar = 0
                for key, car in self.carList.items():
                    if car.isCounted:
                        continue
                    if car.carType == SmallCarID:
                        tar = 0
                    elif car.carType == MotoBikeID:
                        tar = 1
                    elif car.carType == BigCarID:
                        tar = 2
                    elif car.carType == PeopleID:
                        tar = 3
                        
                    if car.roadID == 0:
                        carNum_R[tar] += 1
                        waitTime_R[tar] += car.maxWaitSec
                    elif car.roadID == 1:
                        carNum_L[tar] += 1
                        waitTime_L[tar] += car.maxWaitSec
                    elif car.roadID == 2:
                        carNum_U[tar] += 1
                        waitTime_U[tar] += car.maxWaitSec
                    elif car.roadID == 3:
                        carNum_D[tar] += 1
                        waitTime_D[tar] += car.maxWaitSec
                    self.carList[key].isCounted = True
                    
                for i in range(4):
                    if carNum_R[i] > 0:
                        waitTime_R[i] /= carNum_R[i]
                    if carNum_L[i] > 0:
                        waitTime_L[i] /= carNum_L[i]
                    if carNum_U[i] > 0:
                        waitTime_U[i] /= carNum_U[i]
                    if carNum_D[i] > 0:
                        waitTime_D[i] /= carNum_D[i]
                
                jsonData["turnCounting"]["straight_R"]["smallCar"]["averageStoppedDelayTime"] = waitTime_R[0]
                jsonData["turnCounting"]["straight_R"]["bigCar"]["averageStoppedDelayTime"] = waitTime_R[1]
                jsonData["turnCounting"]["straight_R"]["motorbike"]["averageStoppedDelayTime"] = waitTime_R[2]
                
                jsonData["turnCounting"]["straight_L"]["smallCar"]["averageStoppedDelayTime"] = waitTime_L[0]
                jsonData["turnCounting"]["straight_L"]["bigCar"]["averageStoppedDelayTime"] = waitTime_L[1]
                jsonData["turnCounting"]["straight_L"]["motorbike"]["averageStoppedDelayTime"] = waitTime_L[2]
                
                jsonData["turnCounting"]["cross_U"]["smallCar"]["averageStoppedDelayTime"] = waitTime_U[0]
                jsonData["turnCounting"]["cross_U"]["bigCar"]["averageStoppedDelayTime"] = waitTime_U[1]
                jsonData["turnCounting"]["cross_U"]["motorbike"]["averageStoppedDelayTime"] = waitTime_U[2]
                
                jsonData["turnCounting"]["cross_D"]["smallCar"]["averageStoppedDelayTime"] = waitTime_D[0]
                jsonData["turnCounting"]["cross_D"]["bigCar"]["averageStoppedDelayTime"] = waitTime_D[1]
                jsonData["turnCounting"]["cross_D"]["motorbike"]["averageStoppedDelayTime"] = waitTime_D[2]
                '''
                
                
                
                waitTimes = resetWaitCounting()
                carNums = resetWaitCounting()
                '''
                WaitCounting順序同self.roads:
                第一層路口:右、左、上、下
                第二層轉向:直、左、右、兩、迴
                第三層車種:小車、機車、大車、行人
                '''
                
                tarRoadType = 0
                tarTurnType = 0
                tarCarType = 0
                tarRoadType_toJson = ["straight_R", "straight_L", "cross_U", "cross_D"]
                tarCarType_toJson = ["smallCar", "motorbike", "bigCar", "people"]
                tarTurnType_toJson = ["averageStoppedDelayTime_Straight", "averageStoppedDelayTime_TurnLeft", 
                                      "averageStoppedDelayTime_TurnRight", "averageStoppedDelayTime_HookTurn", 
                                      "averageStoppedDelayTime_TurnBack"]
                for key, car in self.carList.items():
                    if car.isCounted:
                        continue
                    if car.carType == SmallCarID:
                        tarCarType = 0
                    elif car.carType == MotoBikeID:
                        tarCarType = 1
                    elif car.carType == BigCarID:
                        tarCarType = 2
                    elif car.carType == PeopleID:
                        tarCarType = 3
                        
                    tarRoadType = car.roadID
                    tarTurnType = car.turnID
                    
                    if tarRoadType == -1 or tarTurnType == -1:
                        continue
                    idx = 0
                    if car.out_lane_type == 2:
                        idx = 1
                    waitTimes[tarRoadType][tarTurnType][tarCarType][idx] += car.maxWaitSec
                    carNums[tarRoadType][tarTurnType][tarCarType][idx] += 1

                    self.carList[key].isCounted = True
                

                for i in range(len(waitTimes)):
                    for j in range(len(waitTimes[i])):
                        for k in range(len(waitTimes[i][j])):
                            for l in range(len(waitTimes[i][j][k])):
                                if carNums[i][j][k][l] > 0:
                                    waitTimes[i][j][k][l] /= carNums[i][j][k][l]
                
                for i in range(len(waitTimes)):
                    for j in range(len(waitTimes[i])):
                        for k in range(len(waitTimes[i][j])):
                            #路口i，轉向j，車種k
                            #機車以外沒有HookTurn
                            if k != 1:
                                if j == 4:
                                    continue
                            #20220510 車道區分
                            if test_split_road:
                                jsonData["turnCounting"][tarRoadType_toJson[i]][tarCarType_toJson[k]][tarTurnType_toJson[j]] = [waitTimes[i][j][k][0], waitTimes[i][j][k][1]]
                            else:
                                jsonData["turnCounting"][tarRoadType_toJson[i]][tarCarType_toJson[k]][tarTurnType_toJson[j]] = waitTimes[i][j][k][0]


                #sqlite_insert(jsonData, outputJsonTimes)
                
                #{"minute":1,"data":{camera........},{"minute":2,"data":{camera........}
                tempJson = {}
                tempJson["minute"] = outputJsonTimes * self.timeStoreSegment #20220324 現在變成15分鐘切一段
                tempJson["data"] = copy.deepcopy(jsonData)
                outputJsonTimes += 1
                totalJsonData.append(tempJson)
                self.temproads = resetRoadCounting()
        
        #for target in self.turnRightCar:
        #    print(target)
        #self.countLossTracks()
        

        
        
        

        #self.logger.info(f'{frame_count/(time.time() - self.startTime)}') #Merge acc
        #To sort the carAccList
        # 20220813 經討論不對speedMa設上限
        # idxFrameMaSpeedDict = {}
        # idxFrameWholeMaSpeedAvgDict = {}
        # idxFrameWholeMaSpeedAvgList = []
        # maSpeedLowerLimit = 0
        # idxFramesOutsideRange = []
        # for objectType, objectSpeedMaDict in self.speedMaDict.items():
        #     for objectId, speedMaList in objectSpeedMaDict.items():
        #         for speedMaTuple in speedMaList:
        #             idxFrame = speedMaTuple[0]
        #             if idxFrame not in idxFrameMaSpeedDict:
        #                 idxFrameMaSpeedDict[idxFrame] = []
        #             idxFrameMaSpeedDict[idxFrame].append(speedMaTuple[1])
        # for idxFrame, maSpeedList in idxFrameMaSpeedDict.items():
        #     wholeMaSpeedAvg = sum(maSpeedList) / len(maSpeedList)
        #     idxFrameWholeMaSpeedAvgDict[idxFrame] = wholeMaSpeedAvg
        #     idxFrameWholeMaSpeedAvgList.append(wholeMaSpeedAvg)
        # idxFrameWholeMaSpeedAvgList.sort()
        # idxFrameWholeMaSpeedAvgListLen = len(idxFrameWholeMaSpeedAvgList)
        # print(self.speedMaDict)
        # if idxFrameWholeMaSpeedAvgListLen == 0:
        #     maSpeedLowerLimit = 0
        # else:
        #     maSpeedLowerLimit = idxFrameWholeMaSpeedAvgList[int(idxFrameWholeMaSpeedAvgListLen * 0.25)]
        # for idxFrame, wholeMaSpeedAvg in idxFrameWholeMaSpeedAvgDict.items():
        #     if wholeMaSpeedAvg < maSpeedLowerLimit:
        #         idxFramesOutsideRange.append(idxFrame)
        # for objectType, accDict in self.accDict.items():
        #     for objectId, accList in accDict.items():
        #         for accTuple in accList:
        #             idxFrame = accTuple[0]
        #             if idxFrame in idxFramesOutsideRange:
        #                 self.accDict[objectType][objectId].remove(accTuple)
        # for objectType, negAccDict in self.negAccDict.items():
        #     for objectId, negAccList in negAccDict.items():
        #         for negAccTuple in negAccList:
        #             idxFrame = negAccTuple[0]
        #             if idxFrame in idxFramesOutsideRange:
        #                 self.negAccDict[objectType][objectId].remove(negAccTuple)
        # for objectType, objectSpeedMaDict in self.speedMaDict.items():
        #     for objectId, speedMaList in objectSpeedMaDict.items():
        #         for speedMaTuple in speedMaList:
        #             idxFrame = speedMaTuple[0]
        #             if idxFrame in idxFramesOutsideRange:
        #                 self.speedMaDict[objectType][objectId].remove(speedMaTuple)
        # for count, accDict in enumerate(self.accDictQuarter):
        #     for objectType, accList in accDict.items():
        #         for accTuple in accList:
        #             if accTuple[0] in idxFramesOutsideRange:
        #                 self.accDictQuarter[count][objectType].remove(accTuple)
        # for count, negAccDict in enumerate(self.negAccDictQuarter):
        #     for objectType, negAccList in negAccDict.items():
        #         for negAccTuple in negAccList:
        #             if negAccTuple[0] in idxFramesOutsideRange:
        #                 self.negAccDictQuarter[count][objectType].remove(negAccTuple)
        # for objectId, speedToAccList in self.speedLevelToAccDict.items():
        #     for speedLevel, accList in enumerate(speedToAccList):
        #         for accTuple in accList:
        #             if accTuple[0] in idxFramesOutsideRange:
        #                 self.speedLevelToAccDict[objectId][speedLevel].remove(accTuple)
        # for objectId, speedToNegAccList in self.speedLevelToNegAccDict.items():
        #     for speedLevel, negAccList in enumerate(speedToNegAccList):
        #         for negAccTuple in negAccList:
        #             if negAccTuple[0] in idxFramesOutsideRange:
        #                 self.speedLevelToNegAccDict[objectId][speedLevel].remove(negAccTuple)
        
        differencialAccDict = {}
        for objectType, objectAccDict in self.accDict.items():
            for objectId, accList in objectAccDict.items():
                if objectId in self.negAccDict[objectType]:
                    merged_acc_list = self.accDict[objectType][objectId] + self.negAccDict[objectType][objectId]
                    merged_acc_list.sort(key=lambda x:x[0])
                    for index, accTuple in enumerate(merged_acc_list):
                        if index == 0:
                            continue
                        else:
                            accDifferencial = abs(accTuple[1] - merged_acc_list[index-1][1]) / (accTuple[0] - merged_acc_list[index-1][0])
                            if objectType not in differencialAccDict:
                                differencialAccDict[objectType] = {}
                            if objectId not in differencialAccDict[objectType]:
                                differencialAccDict[objectType][objectId] = []
                            differencialAccDict[objectType][objectId].append((accTuple[0], accDifferencial))
        for objectType, objectAccDict in self.negAccDict.items():
            for objectId, accList in objectAccDict.items():
                if objectId not in self.accDict[objectType]:
                    merged_acc_list = self.negAccDict[objectType][objectId]
                    merged_acc_list.sort(key=lambda x:x[0])
                    for index, accTuple in enumerate(merged_acc_list):
                        if index == 0:
                            continue
                        else:
                            accDifferencial = abs(accTuple[1] - merged_acc_list[index-1][1]) / (accTuple[0] - merged_acc_list[index-1][0])
                            if objectType not in differencialAccDict:
                                differencialAccDict[objectType] = {}
                            if objectId not in differencialAccDict[objectType]:
                                differencialAccDict[objectType][objectId] = []
                            differencialAccDict[objectType][objectId].append((accTuple[0] ,accDifferencial))
        with open(self.args.save_path + "%s_%s_differencialAccDict.p" % (cameraNo, videoStartTime_ori), "wb") as f:
            pickle.dump(differencialAccDict, f)
        
        
        for carType in self.speedLevelToAccDict:
            for speedLevel, AccList in enumerate(self.speedLevelToAccDict[carType]):
                self.speedLevelToAccDict[carType][speedLevel] = sorted(AccList, key=lambda x: x[1])
                self.speedLevelToNegAccDict[carType][speedLevel] = sorted(self.speedLevelToNegAccDict[carType][speedLevel], key=lambda x:x[1], reverse=True)
        #print(self.speedLevelToAccDict)
        #print(self.speedLevelToNegAccDict)
        
        
        
        
        #store the data to csv file
        for carType, speedLevelList in self.speedLevelToAccDict.items():
            #print(carType)
            if carType == "BigCar" or carType == "SmallCar" or carType == "MotorBike":
                with open(self.args.save_path + str(cameraNo) + ("_%s_" % videoStartTime_ori) + carType +'AccIndication.csv', 'w', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    writer2 = csv.writer(self.speedLevelToAccCsv)
                    writer.writerow(['X','Y','Ymin','Ymax'])
                    writer2.writerow(['X','Y','Ymin','Ymax'])
                    for speedLevelCount , AccList in enumerate(speedLevelList):
                        if len(AccList) == 0:
                            writer.writerow([speedLevelCount*10,0,0,0])
                            writer2.writerow([speedLevelCount*10,0,0,0])
                        else:
                            writer.writerow([speedLevelCount*10,AccList[int(len(AccList)*0.7)][1],AccList[0][1],AccList[-1][1]])
                            writer2.writerow([speedLevelCount*10,AccList[int(len(AccList)*0.7)][1],AccList[0][1],AccList[-1][1]])
        for carType, speedLevelList in self.speedLevelToNegAccDict.items():
            #print(carType)
            if carType == "BigCar" or carType == "SmallCar" or carType == "MotorBike":
                with open(self.args.save_path + str(cameraNo) + ("_%s_" % videoStartTime_ori) + carType +'NegAccIndication.csv', 'w', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    writer2 = csv.writer(self.speedLevelToNegAccCsv)
                    writer.writerow(['X','Y','Ymin','Ymax'])
                    writer2.writerow(['X','Y','Ymin','Ymax'])
                    for speedLevelCount , negAccList in enumerate(speedLevelList):
                        if len(negAccList) == 0:
                            writer.writerow([speedLevelCount*10,0,0,0])
                            writer2.writerow([speedLevelCount*10,0,0,0])
                        else:
                            writer.writerow([speedLevelCount*10,negAccList[int(len(negAccList)*0.7)][1],negAccList[0][1],negAccList[-1][1]])
                            writer2.writerow([speedLevelCount*10,negAccList[int(len(negAccList)*0.7)][1],negAccList[0][1],negAccList[-1][1]])
        #Merge acc end
        with open(self.args.save_path + str(cameraNo) + ("_%s_" % videoStartTime_ori) +'carSpacingsRaw.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile)
            writer.writerow(['frameCount', 'carID', 'carType', 'backCarID', 'backCarType', 'x2w', 'x3w', 'centerX', 'centerY', 'backCarX2w', 'backCarX3w', 'backCarCenterX', 'backCarCenterY', 'backCarSpeed', 'distance'])
            #20240313 輸出影片長度不足找半天原來是這個...
            #for idx_frame, carSpacingsList in self.carSpacingsTotalDict.items():
            for idx_f, carSpacingsList in self.carSpacingsTotalDict.items():
                for carSpacing in carSpacingsList:
                    #writer.writerow([idx_frame] + carSpacing)
                    writer.writerow([idx_f] + carSpacing)
            
        
        
        
        #20220505 輸出速度的csv
        '''
        speedMaDict 結構是 各種className(Dict) -> 各種car的key(Dict) -> 該車輛各frame的ma速度(List) -> 每筆速度值用 frame,MaSpeed 組成(Tuple)
        要輸出的內容，參照 車流影像Dashboard構想(v6)對進度用.pptx p25
        要各車種的正反向平均行駛速率
        速度採樣點如果所有車的加總在一起取平均，會低估平均速度(因為開得快的車子，比較早離開畫面，採樣的點數就少)
        需要同一輛車的Speed採樣取平均，算出單一車輛平均速度，然後再依車種，把平均速度加總，除以該車種車輛數
        
        '''
        carTypeLabel = {"MotorBike":0, "SmallCar":1, "BigCar":2}
        carSpeedList_positive = [[], [], []]
        carSpeedList_negative = [[], [], []]
        for carType, value in self.speedDict.items():
            #print(carType)
            if carType not in carTypeLabel:
                continue
            #print(value)
            for carKey, speedTupel in value.items():
                #print(carKey)
                #print(speedTupel)
                speedList = [x[2] for x in speedTupel]
                #print(speedList)
                #然後，停等狀態不要算成樣本， 我算速度絕對值小於 3 的是停等
                speedList = [x for x in speedList if (abs(x) > 3)]
                
                if len(speedList) > 0:
                    #avgSpeedOneCar = np.average(speedList)
                    #取用平均值還是會低估，因為同一台車的採樣點，也是開越慢的時候樣本越多
                    #嘗試改用75分位數
                    speedList.sort(key = lambda x: abs(x))
                    avgSpeedOneCar = speedList[int(len(speedList) * 0.75)]
                else:
                    continue

                if avgSpeedOneCar > 0:
                    carSpeedList_positive[carTypeLabel[carType]].append(avgSpeedOneCar)
                else:
                    carSpeedList_negative[carTypeLabel[carType]].append(avgSpeedOneCar)
                    
        #print(carSpeedList_positive)
        #print(carSpeedList_negative)
        
        carSpeedList_positive_avg = [0, 0, 0]
        carSpeedList_negative_avg = [0, 0, 0]
        
        for i in range(len(carSpeedList_positive_avg)):
            if len(carSpeedList_positive[i]) > 0:
                carSpeedList_positive_avg[i] = np.average(carSpeedList_positive[i])
            if len(carSpeedList_negative[i]) > 0:
                carSpeedList_negative_avg[i] = np.abs(np.average(carSpeedList_negative[i]))
        #print(carSpeedList_positive_avg)
        #print(carSpeedList_negative_avg)
        
        file = None
        if self.task != None and self.task.setting['road_mode'] == 'straight':
            file = open(settings.FILE_PATH + f'{self.task.id}' + 'AvgSpeed.csv', 'w')
        else:
            file = open(self.args.save_path + str(cameraNo) + ("_%s_" % videoStartTime_ori) +'AvgSpeed.csv', 'w')

        file.write("Direction,MotorBike_AvgSpeed,SmallCar_AvgSpeed,BigCar_AvgSpeed\n")
        file.write("positive,%f,%f,%f\n" % (carSpeedList_positive_avg[0], carSpeedList_positive_avg[1], carSpeedList_positive_avg[2]))
        file.write("negative,%f,%f,%f" % (carSpeedList_negative_avg[0], carSpeedList_negative_avg[1], carSpeedList_negative_avg[2]))
        file.close()
        
        
        minD_list = [0, 0, 0]
        maxD_list = [0, 0, 0]
        ax_list = [0.4, 2.1, 2.0] # ax 根據公式為定值(機車0.4其他2.1)
        bx_add_list = [0, 0, 0]
        bx_mult_list = [0, 0, 0]
        
        
        #20220506 安全距離輸出csv
        for i in range(len(self.carSpacings_D)):
            one_D = self.carSpacings_D[i]
            one_V = self.carSpacings_V[i]
            
            # d = ax + (bx_add + bx_mult*z) * sqrt(v)
            ax = 2.1
            if i == 1: #機車的AX是0.4
                ax = 0.4
                
            bx_add = 0
            bx_mult = 0
            minD = 0
            maxD = 0
            
            if len(one_D) == 0:
                continue
            
            if len(one_D) > 20:
                #20220603 當樣本大於20個時，過濾5%離群值
                findN = (int)(len(one_D) * 0.05)
                minD = heapq.nsmallest(findN, one_D)[-1]
                minIndex = one_D.index(minD)
                minV = one_V[minIndex]
                
                maxD = heapq.nlargest(findN, one_D)[-1]
                maxIndex = one_D.index(maxD)
                maxV = one_V[maxIndex]
            else:
                minD = min(one_D)
                minIndex = one_D.index(minD)
                minV = one_V[minIndex]
                
                maxD = max(one_D)
                maxIndex = one_D.index(maxD)
                maxV = one_V[maxIndex]
            
            minD_list[i] = minD
            maxD_list[i] = maxD
            
            if minV <= 0:
                continue
            if minV == maxV:
                continue
            
            # 將max(d )及min(d )分別取z值為1、0，代入公式
            '''
            minD，z為0
            minD = ax + (bx_add + bx_mult*0) * sqrt(minV)
            minD = ax + (bx_add) * sqrt(minV)
            bx_add = (minD - ax) / (sqrt(minV))
            '''
            bx_add = (minD - ax) / (math.sqrt(minV))
            
            '''
            maxD，z為1
            maxD = ax + (bx_add + bx_mult*1) * sqrt(maxV)
            (bx_add + bx_mult) = (maxD - ax) / sqrt(maxV)
            bx_mult = (maxD - ax) / sqrt(maxV) - bx_add
            '''
            bx_mult = ( (maxD - ax) / math.sqrt(maxV) ) - bx_add
            
            bx_add_list[i] = bx_add
            bx_mult_list[i] = bx_mult
        
        file = None
        if self.task != None and self.task.setting['road_mode'] == 'straight':
            file = open(settings.FILE_PATH + f'{self.task.id}' + 'CarSpacing.csv', 'w')
        else:
            file = open(self.args.save_path + str(cameraNo) + ("_%s_" % videoStartTime_ori) +'CarSpacing.csv', 'w')
        car_type_list = ["MotorBike", "SmallCar", "BigCar"]
        
        file.write("CarType,minD,maxD,ax,bx_add,bx_mult\n")
        for i in range(3):
            file.write("%s,%f,%f,%f,%f,%f" % (car_type_list[i], minD_list[i], maxD_list[i], ax_list[i], bx_add_list[i], bx_mult_list[i]))
            if i < 3:
                file.write("\n")
        file.close()
        
        
        if debug_save_car_tracking:
            self.saveCarList()
        
        #20211123路口資訊寫出到excel
        wb = load_workbook(excel_format_file_path)
        excel_save_path = self.args.save_path + "camera%d_%s.xlsx" % (cameraNo, videoStartTime_ori)
        sheet = wb["sheet"]
        
        #以下這5行來自 https://stackoverflow.com/questions/38734044/losing-merged-cells-border-while-editing-excel-file-with-openpyxl/46757594
        #修正框線格式遺失問題
        for merged_cells in sheet.merged_cells.ranges:
            style = sheet.cell(merged_cells.min_row, merged_cells.min_col)._style
            for col in range(merged_cells.min_col, merged_cells.max_col + 1):
                for row in range(merged_cells.min_row, merged_cells.max_row + 1): 
                    sheet.cell(row, col)._style = style
        
        
        #抓全部左轉/右轉/直行 的 大車/小車/機車/行人，依路口分開

        #這邊輸出轉彎量
        targetRoad = 0 #Right in
        targetTurnType = 0#straight
        targetCarType = 0 #smallCar
        
        #行人/機車/小客車/大客車 對應row為 K L M N ，然後 self.roads 順序是小車/機車/大車/行人，弄個轉換表
        carType_toExcel = ["M", "L", "N", "K"]
        #同理，路口轉換對應右左上下分別轉5, 11, 14, 8
        roadType_toExcel = [5, 11, 14, 8]
        #同理，轉彎轉換，直左右兩迴，對應 1, 0, 2, X, X
        turnType_toExcel = [1, 0, 2, -1, -1]
        
        for targetRoad in range(len(self.roads)):
            for targetTurnType in range(len(self.roads[targetRoad])):
                for targetCarType in range(len(self.roads[targetRoad][targetTurnType])):
                    #20220510 分車道，excel還是填在同一格，依序填內車道/外車道
                    if test_split_road:
                        countNum0 = len(self.roads[targetRoad][targetTurnType][targetCarType][0])
                        #機車要處理兩段式左轉
                        if targetCarType == 1:#如果是機車，把兩段式左轉的count也加上去
                            countNum0 += len(self.roads[targetRoad][3][targetCarType][0])
                            
                        countNum1 = len(self.roads[targetRoad][targetTurnType][targetCarType][1])
                        #機車要處理兩段式左轉
                        if targetCarType == 1:
                            countNum1 += len(self.roads[targetRoad][3][targetCarType][1])
                        
                        #然後excel表只要輸出直行/左轉/右轉
                        if targetTurnType <= 2:
                            sheet["%s%d" % (carType_toExcel[targetCarType], roadType_toExcel[targetRoad] 
                                            + turnType_toExcel[targetTurnType])] = "%d, %d" % (countNum0, countNum1)
                    else:
                        countNum = len(self.roads[targetRoad][targetTurnType][targetCarType][0])
                        #機車要處理兩段式左轉
                        if targetCarType == 1:#如果是機車，把兩段式左轉的count也加上去
                            countNum += len(self.roads[targetRoad][3][targetCarType][0])
                        #然後excel表只要輸出直行/左轉/右轉
                        if targetTurnType <= 2:
                            sheet["%s%d" % (carType_toExcel[targetCarType], roadType_toExcel[targetRoad] 
                                            + turnType_toExcel[targetTurnType])] = countNum
                        

        #路口停等時間輸出excel

        waitTimes = resetWaitCounting()
        carNums = resetWaitCounting()
        '''
        WaitCounting順序同self.roads:
        第一層路口:右、左、上、下
        第二層轉向:直、左、右、兩、迴
        第三層車種:小車、機車、大車、行人
        '''
        
        tarRoadType = 0
        tarTurnType = 0
        tarCarType = 0
        #車種轉excel對應row更換，路口/轉彎對應相同
        carType_toExcel = ["I", "H", "J", "G"]
        
        for key, car in self.carList.items():
            if car.carType == SmallCarID:
                tarCarType = 0
            elif car.carType == MotoBikeID:
                tarCarType = 1
            elif car.carType == BigCarID:
                tarCarType = 2
            elif car.carType == PeopleID:
                tarCarType = 3
                
            tarRoadType = car.roadID
            tarTurnType = car.turnID
            
            if tarRoadType == -1 or tarTurnType == -1:
                continue
            idx = 0
            if car.out_lane_type == 2:
                idx = 1
            waitTimes[tarRoadType][tarTurnType][tarCarType][idx] += car.maxWaitSec
            carNums[tarRoadType][tarTurnType][tarCarType][idx] += 1
        
        for i in range(len(waitTimes)):
            for j in range(len(waitTimes[i])):
                for k in range(len(waitTimes[i][j])):
                    for l in range(len(waitTimes[i][j][k])):
                        if carNums[i][j][k][l] > 0:
                            waitTimes[i][j][k][l] /= carNums[i][j][k][l]

        for targetRoad in range(len(waitTimes)):
            for targetTurnType in range(len(waitTimes[targetRoad])):
                for targetCarType in range(len(waitTimes[targetRoad][targetTurnType])):
                    #excel表只要輸出直行/左轉/右轉
                    if targetTurnType <= 2:
                        if test_split_road:
                            targetValue0 = waitTimes[targetRoad][targetTurnType][targetCarType][0]
                            targetValue1 = waitTimes[targetRoad][targetTurnType][targetCarType][1]
                            sheet["%s%d" % (carType_toExcel[targetCarType], roadType_toExcel[targetRoad] 
                                        + turnType_toExcel[targetTurnType])] = "%.2f, %.2f" % (targetValue0, targetValue1)
                        else:
                            targetValue = waitTimes[targetRoad][targetTurnType][targetCarType][0]
                            sheet["%s%d" % (carType_toExcel[targetCarType], roadType_toExcel[targetRoad] 
                                        + turnType_toExcel[targetTurnType])] = "%.2f" % (targetValue)
                        
        #抓上午/下午/尖峰/離峰 2021-09-20-09-00-00
        #videoStartTime_ori
        time_hour = int(videoStartTime_ori.split("-")[3])
        tempString = ""
        if time_hour < 12:
            tempString = "上午"
        else:
            tempString = "下午"
        #尖峰時段定義為8點到10點與18點到20點
        if time_hour in [8,9,10,18,19,20]:
            tempString += "尖峰"
        else:
            tempString += "離峰"
        sheet["B3"] = tempString
        
        
        
        #輸出路口，從camera編號對應
        roadName = "camera%d" % cameraNo
        if cameraNo == 3:
            sheet["C6"] = "中正路"
            sheet["B8"] = "大興西路"
            roadName = "中正大興路口"
        elif cameraNo == 14:
            sheet["C6"] = "中正路"
            sheet["B8"] = "同德六街"
            roadName = "中正同德六街路口"
        if self.task != None:
            task = self.task
            if False:
                if 'road_4_name' in task.setting:
                    sheet["C6"] = task.setting['road_4_name']
                if 'road_1_name' in task.setting:
                    sheet["D8"] = task.setting['road_1_name']
                if 'road_2_name' in task.setting:
                    sheet["C10"] = task.setting['road_2_name']
                if 'road_3_name' in task.setting:
                    sheet["B8"] = task.setting['road_3_name']
            if 'road_1_name' in task.setting and 'road_2_name' in task.setting and 'road_3_name' in task.setting and 'road_4_name' in task.setting:
                roadName = task.setting['road_4_name'] + task.setting['road_3_name']
                roadName = roadName.replace('(東)', '').replace('(南)', '').replace('(西)', '').replace('(北)', '').replace('路', '')
                roadName = roadName.replace('（東）', '').replace('（南）', '').replace('（西）', '').replace('（北）', '')
                roadName = roadName + '路口'
                tempNameList = []
                tempNameList.append((task.setting['road_1_name'], '1'))
                tempNameList.append((task.setting['road_2_name'], '2'))
                tempNameList.append((task.setting['road_3_name'], '3'))
                tempNameList.append((task.setting['road_4_name'], '4'))
                for tempObj in tempNameList:
                    tempName = tempObj[0]
                    tempRoadNo = tempObj[1]
                    if tempName.find('(北)') > -1 or tempName.find('（北）') > -1:
                        sheet["C6"] = tempName
                        sheet["C7"] = tempRoadNo
                    elif tempName.find('(東)') > -1 or tempName.find('（東）') > -1:
                        sheet["D8"] = tempName
                        sheet["D9"] = tempRoadNo
                    elif tempName.find('(南)') > -1 or tempName.find('（南）') > -1:
                        sheet["C10"] = tempName
                        sheet["C11"] = tempRoadNo
                    elif tempName.find('(西)') > -1 or tempName.find('（西）') > -1:
                        sheet["B8"] = tempName
                        sheet["B9"] = tempRoadNo

            #if 'videoStartTime' in task.setting and 'videoEndTime' in task.setting:
            #    sheet["B3"] = f'{task.setting["videoStartTime"]}-{task.setting["videoEndTime"]}'
            if 'videoDate' in task.setting:
                sheet["B3"] = f'{task.setting["videoDate"]}'
        sheet.title = "%s(%s)" % (roadName, tempString)
        
        wb.save(excel_save_path)
        

        #excel輸出結束
        sqlite_insert(totalJsonData)
        

        #將車輛之速度、加速度序列化
        with open(self.args.save_path + "%s_%s_speed.p" % (cameraNo, videoStartTime_ori), "wb") as f:
            pickle.dump(self.speedDict, f)
        with open(self.args.save_path + "%s_%s_speedMa.p" % (cameraNo, videoStartTime_ori), "wb") as f:
            pickle.dump(self.speedMaDict, f)
        with open(self.args.save_path + "%s_%s_speedFiltered.p" % (cameraNo, videoStartTime_ori), "wb") as f:
            pickle.dump(self.speedFilteredDict, f)
        with open(self.args.save_path + "%s_%s_acc.p" % (cameraNo, videoStartTime_ori), "wb") as f:
            pickle.dump(self.accDict, f)
        with open(self.args.save_path + "%s_%s_negAcc.p" % (cameraNo, videoStartTime_ori), "wb") as f:
            pickle.dump(self.negAccDict, f)
        

        cv2.destroyAllWindows()
        
        #20220119 debug用，把車間距的jpg圖轉影片 
        video_name = ''
        if debug_draw_car_spacing and jsonData["type"] in [1, 3]:
            
            if debug_save_fomat_is_mp4:
                video_name = self.args.save_path + 'temp/carSpacing.mp4'
            else:
                video_name = self.args.save_path + 'temp/carSpacing.avi'
            frame = cv2.imread(self.args.save_path + "temp/1_carSpacing.jpg")
            height, width, layers = frame.shape
            if debug_save_fomat_is_mp4:
                video = cv2.VideoWriter(video_name,  cv2.VideoWriter_fourcc(*'MP4V'), self.fps, (width,height))
            else:
                video = cv2.VideoWriter(video_name,  cv2.VideoWriter_fourcc(*'MJPG'), self.fps, (width,height))
            
            if debug_test_n_sec:
                idx_frame = int(debug_test_secs * self.fps)
            if self.task != None and 'debug_run_sec' in self.task.setting and self.task.setting['debug_run_sec'] not in [None, 0]:
                idx_frame = int(self.task.setting['debug_run_sec'] * self.fps)
            for i in range(1, idx_frame - 1, 1):
                video.write(cv2.imread(self.args.save_path + f"temp/{i}_carSpacing.jpg"))
                #print(f'idx_frame={idx_frame} write {i}_carSpacing.jpg')
            video.release()
        

        if self.args.save_path:
            if debug_output_video:
                self.writer.release()
                
        # save results
        write_results(self.save_results_path, results, 'mot')
        if self.task_id != None:
            task = self.task
            task.message = 'encode output video to x264'
            task.save()
            video_id = task.setting['video_id']
            video = Video.objects.filter(id=video_id).first()
            wb.save(settings.FILE_PATH + f'{task.id}_output.xlsx')
            dbResult = Result.objects.create(video=video, task=task)
            dbResult.data['result'] = totalJsonData
            dbResult.data['excel_path'] = settings.FILE_PATH + f'{task.id}_output.xlsx'
            dbResult.data['track_map_path'] = settings.FILE_PATH + f'{task.id}/'
            if not os.path.isdir(dbResult.data['track_map_path']):
                os.mkdir(dbResult.data['track_map_path'])
            org_mp4_path = settings.FILE_PATH + f'output_{self.task.id}_t.mp4'
            out_mp4_path = settings.FILE_PATH + f'output_{self.task.id}.mp4'
            os.system(f"ffmpeg -i {org_mp4_path} -vcodec libx264 {out_mp4_path}")
            dbResult.data['save_video_path'] = out_mp4_path
            dbResult.save()
            if os.path.exists(org_mp4_path):
                os.remove(org_mp4_path)
            if debug_draw_car_spacing and jsonData["type"] in [1, 3]:
                out_mp4_path2 = settings.FILE_PATH + f'output_warp_{self.task.id}.mp4'
                os.system(f"ffmpeg -i {video_name} -vcodec libx264 {out_mp4_path2}")
                dbResult.data['save_video_path2'] = out_mp4_path2
                dbResult.save()
            

            #20211109暫時使用：從輸出影片看哪輛車被判定成轉哪邊有難度(在車子多時，看不出哪輛車被判定往哪轉)，把路徑畫出來看
            if debug_draw_track and self.task.setting['road_mode'] == 'cross':
                trun =["straight", "turnLeft", "turnRight", "hookTurn", "turnBack", "Unknow"]
                road =["R", "L", "U", "D", "Unknow"]
                backgroundImg = cv2.imread(self.args.save_path + "temp%s.png" % cameraNo)
                img_count = 0
                dbResult.data['track_map_count'] = 0
                for key, car in self.carList.items():
                    tracks = car.tracks;
                    #trackMap = np.zeros((self.im_height, self.im_width, 3), np.uint8)
                    trackMap = backgroundImg.copy()
                    pxc = 0
                    pyc = 0
                    for i in range(len(tracks)):
                        track = tracks[i]
                        xc = track[0] + track[2] // 2
                        yc = track[1] + track[3] // 2
                        if i == 0:
                            cv2.circle(trackMap, (xc, yc), 20, (255, 0, 0), -1)
                        else:
                            cv2.line(trackMap, (xc, yc), (pxc, pyc), (255, 0, 0), 3)
                        cv2.circle(trackMap, (xc, yc), 5, (0, 0, 255), -1)
                        pxc = xc
                        pyc = yc
                    cv2.putText(trackMap, "Road=%s" % (road[car.roadID]) , (50, 50), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 2)
                    cv2.putText(trackMap, "Turn=%s" % (trun[car.turnID]) , (50, 100), cv2.FONT_HERSHEY_PLAIN, 3, (0, 255, 0), 2)
                    #cv2.imwrite(self.args.save_path + "temp/%d_%d.png" % (car.carID, car.carType), trackMap)

                    cv2.imwrite(dbResult.data['track_map_path'] + f'{img_count}.png', trackMap)
                    img_count += 1

                    #img_b = car.trackToMat(self.im_width, self.im_height)
                    #cv2.imwrite(self.args.save_path + "temp/%d_%d_.png" % (car.carID, car.carType), img_b)
                dbResult.data['track_map_count'] = img_count
                dbResult.save()
            task.status = 'success'
            task.message = 'OK'
            task.save()





def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("VIDEO_PATH", type=str)
    parser.add_argument("--config_detection", type=str, default="./configs/yolov7.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    # parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
    parser.add_argument("--display", action="store_true")
    parser.add_argument("--frame_interval", type=int, default=1)
    parser.add_argument("--display_width", type=int, default=800)
    parser.add_argument("--display_height", type=int, default=600)
    parser.add_argument("--save_path", type=str, default="./output/offPeak/")
    parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
    parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
    return parser.parse_args()


def resetRoadCounting():    
    roads = []
    for i in range(4):#雙向道分開算，有四條道路
        roads.append([])
    for i in range(len(roads)):
        for j in range(5):#轉彎有5種狀況
            roads[i].append([])
    for i in range(len(roads)):
        for j in range(len(roads[i])):
            for k in range(4):#大車小車機車行人，有四種
                roads[i][j].append([])
    #20220510格式增加一層車道區分，變成 [roadID][trunID][carTypeID][idx] idx=0內車道 idx=1外車道            
    for i in range(len(roads)):
        for j in range(len(roads[i])):
            for k in range(len(roads[i][j])):
                for l in range(2):#內外車道有2種
                    roads[i][j][k].append(set())
    return roads

def resetWaitCounting():
    roads = []
    for i in range(4):#雙向道分開算，有四條道路
        roads.append([])
    for i in range(len(roads)):
        for j in range(5):#轉彎有5種狀況
            roads[i].append([])
    for i in range(len(roads)):
        for j in range(len(roads[i])):
            for k in range(4):#大車小車機車行人，有四種
                roads[i][j].append([])
    #20220510格式增加一層車道區分，變成 [roadID][trunID][carTypeID][idx] idx=0內車道 idx=1外車道 
    for i in range(len(roads)):
            for j in range(len(roads[i])):
                for k in range(len(roads[i][j])):
                    for l in range(2):#內外車道有2種
                        roads[i][j][k].append(0)    
    return roads

def zeroValue():
    global test_split_road
    if test_split_road:
        return [0, 0]
    else:
        return 0
def reset_jsonData():
    '''
    20211124 停等時間格式更新：
    對照要求輸出的excel格式，停等時間也得依左轉/右轉/直行分開統計
    原本
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "averageStoppedDelayTime": 0 },
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "averageStoppedDelayTime": 0 },
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, "averageStoppedDelayTime": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "averageStoppedDelayTime": 0 }
    
    改為
    "bigCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "smallCar": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0},
    
    "motorbike": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0, "hookTurn": 0, 
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0, "averageStoppedDelayTime_HookTurn": 0},
    
    "people": { "straight": 0, "turnRight": 0, "turnLeft": 0, "turnBack": 0,
               "averageStoppedDelayTime_Straight": 0, "averageStoppedDelayTime_TurnRight": 0, 
               "averageStoppedDelayTime_TurnLeft": 0, "averageStoppedDelayTime_TurnBack": 0}
    
    20211229增加平均車間距 averageCarSpacing ，只在直線道路計算，單位為公尺，紀錄在 extraCounting，
    
    20220409增加停等車輛記數，針對type3的CCTV(單一車道)計算，紀錄在 extraCounting，欄位為waitingCount
    '''
    global jsonData
    
    
        
    
    jsonData = {
        "cameraNo":0, 
        "videoStartTime": "2000-00-00 00:00:00", 
        "videoEndTime": "2000-00-00 00:00:00", 
        "videoLength":0, 
        "type": 0,
    
    "standardCounting" : {
    
    "bigCarNum": 0, "smallCarNum": 0, "motorbikeNum": 0, "peopleNum": 0
    
    },
    
    "turnCounting": {
    
    "straight_R": {
    
    "bigCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "smallCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "motorbike": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), "hookTurn": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue(), "averageStoppedDelayTime_HookTurn": zeroValue()},
    
    "people": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(),
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()}
    
    },
    
    "straight_L": {
    
    "bigCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "smallCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "motorbike": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), "hookTurn": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue(), "averageStoppedDelayTime_HookTurn": zeroValue()},
    
    "people": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(),
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()}
    
    },
    
    "cross_U": {
    
    "bigCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "smallCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "motorbike": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), "hookTurn": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue(), "averageStoppedDelayTime_HookTurn": zeroValue()},
    
    "people": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(),
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()}
    
    },
    
    "cross_D": {
    
    "bigCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "smallCar": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()},
    
    "motorbike": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(), "hookTurn": zeroValue(), 
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue(), "averageStoppedDelayTime_HookTurn": zeroValue()},
    
    "people": { "straight": zeroValue(), "turnRight": zeroValue(), "turnLeft": zeroValue(), "turnBack": zeroValue(),
               "averageStoppedDelayTime_Straight": zeroValue(), "averageStoppedDelayTime_TurnRight": zeroValue(), 
               "averageStoppedDelayTime_TurnLeft": zeroValue(), "averageStoppedDelayTime_TurnBack": zeroValue()}
    
    }
    
    },
    
    #place each 15 minutes' acceleration and negative acceleration
    "accelerationCountingOverall": {# Merge acc
    
    "bigCar": { "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0 },
    
    "smallCar": {  "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0},
    
    "motorbike": { "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0}
    
    },
    
    "nagativeaccelerationCountingOverall": {
        "bigCar": { "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0 },
    
        "smallCar": {  "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0},
        
        "motorbike": { "maxAccSpeed": 0, "minAccSpeed": 0, "75thPercentileAccSpeed": 0, "avgAccSpeed": 0}
        
    },
    
    #Store string formated CSV
    "speedToAccCSV": "",
    "speedToNegAccCSV": "", #Merge acc end
    
    
    "extraCounting": {
        
    "averageCarSpacing": 0,
    #20220505 對照車流影像dashboard構想投影片，好像d也要輸出，所以增加minD與maxD
    #20220506 增加輸出速度，正向/反向道路分開統計 speed_positive speed_negative
    "bigCar": { "ax": 0, "bx_add": 0, "bx_mult": 0, "minD": 0, "maxD": 0, "waitingCount": 0, 
               "speed_positive": 0, "speed_negative": 0},
    
    "smallCar": { "ax": 0, "bx_add": 0, "bx_mult": 0, "minD": 0, "maxD": 0, "waitingCount": 0,
               "speed_positive": 0, "speed_negative": 0},
    
    "motorbike": { "ax": 0, "bx_add": 0, "bx_mult": 0, "minD": 0, "maxD": 0, "waitingCount": 0,
               "speed_positive": 0, "speed_negative": 0}
    
    }
    
    }
    return

def sqlite_createTable():
    #交通量輸出到sqlite
    conn = sqlite3.connect('data/db/data.db')
    
    #Table不存在則建立 https://stackoverflow.com/questions/4098008/create-table-in-sqlite-only-if-it-doesnt-exist-already
    #目前Table定義為 NAME,RECORD,CAMERA_NO,TYPE ，NAME為主鍵    
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS `RECORD`
       (`NAME`          CHAR(64) PRIMARY KEY   NOT NULL,
       `RECORD`         TEXT    NOT NULL,
       `CAMERA_NO`      INT     NOT NULL,
       `TYPE`           INT     NOT NULL);''')
    conn.commit()
    conn.close()
    return

def sqlite_insert(jsonData):
    
    conn = sqlite3.connect('data/db/data.db')
    dataOut = json.dumps(jsonData)
    #輸出到Table
    name = "CAMERA_" + str(cameraNo) + "_" + videoStartTime_ori
    #insert or update 語法參照 https://stackoverflow.com/questions/3634984/insert-if-not-exists-else-update
    if len(jsonData) > 0: # Merge acc
        c = conn.cursor()
        sqlstr = 'INSERT OR REPLACE INTO RECORD VALUES (\'%s\', \'%s\', %d, %d);' % (name, dataOut, cameraNo, jsonData[0]["data"]["type"])
        #print(sqlstr)
        c.execute(sqlstr)
        conn.commit()
        conn.close()
    else:
        pass #Merge acc end
    
    return


def parse_args2(path):
    #print(sys.argv)
    parser = argparse.ArgumentParser()
    parser.add_argument("VIDEO_PATH", type=str, default=path)
    parser.add_argument("--config_detection", type=str, default="./configs/yolov7.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    # parser.add_argument("--ignore_display", dest="display", action="store_false", default=True)
    parser.add_argument("--display", action="store_true")
    parser.add_argument("--frame_interval", type=int, default=1)
    parser.add_argument("--display_width", type=int, default=800)
    parser.add_argument("--display_height", type=int, default=600)
    parser.add_argument("--save_path", type=str, default="./output/offPeak/")
    parser.add_argument("--cpu", dest="use_cuda", action="store_false", default=True)
    parser.add_argument("--camera", action="store", dest="cam", type=int, default="-1")
    # 新增一个参数，用于处理未知的命令行参数
    parser.add_argument("unknown_args", nargs=argparse.REMAINDER)
    return parser.parse_args()


def readSetting2(args, path, cid, tid):
    #global trunGate
    global cameraNo
    global roadPointDataPath
    global roadEndPointDataPath
    global saveTrackDataPath
    global saveTrackDataWithLabelPath
    global videoStartTime_ori
    global jsonData
    global video_day
    
    global road_points
    global road_end_points
    global minStartX
    global minStartY
    global maxStartX
    global maxStartY
    global minEndX
    global minEndY
    global maxEndX
    global maxEndY
    
    videoPath = path
    cameraNo = int(cid)
    
    
    videoStartTime_ori = '2023-09-29-07-00-00'
    videoStartTime = '2023_09_29 07:00:00'

    jsonData["cameraNo"] = cameraNo
    #print(cameraNo)
    jsonData["videoStartTime"] = ""
    
    #20220310 舊影片的十字路口
    '''
    type2Nums = [3, 10, 11, 14]
    type3Nums = [-1]
    if videoNameMode == 2:
        #20220310 對應新影片的十字路口
        type2Nums = [1, 2, 10, 12]
        type3Nums = [3, 4, 5, 6, 8, 9, 11, 13]  #20220320 新影片有要抓單一車道停等區的，定義為type3
    '''
    #20220324 現在都用新的CCTV影像了，路口編號直接用這組
    type2Nums = [1, 2, 10, 12, 106, 107]
    type3Nums = [3, 4, 5, 6, 8, 9, 11, 13]
    
    #暫時要測舊影片切這組
    #type2Nums = [3, 10, 11, 14]
    #type3Nums = [-1]
    
    if cameraNo in type2Nums:
        jsonData["type"] = 2
    elif cameraNo in type3Nums:
        jsonData["type"] = 3
    else:
        jsonData["type"] = 1
    task = Task.objects.filter(id=tid).first()
    if 'road_mode' in task.setting:
        if task.setting['road_mode'] == 'cross':
            jsonData["type"] = 2
        elif task.setting['road_mode'] == 'straight':
            jsonData["type"] = 1
        
    roadPointDataPath = "data/road_camera" + str(cameraNo) + ".csv"
    roadEndPointDataPath = "data/road_end_camera" + str(cameraNo) + ".csv"
    saveTrackDataPath = "data/tracks" + str(cameraNo) + ".csv"
    saveTrackDataWithLabelPath = "data/tracks" + str(cameraNo) + "_with_label.csv"
    
    
    if jsonData["type"] == 2: #轉彎車才要road_points
        if task != None:
            road_points = None
            road_end_points = None
            if 'road_in_points' in task.setting:
                road_points = task.setting['road_in_points']
            if 'road_out_points' in task.setting:
                road_end_points = task.setting['road_out_points']
        if road_points == None or road_end_points == None:
            try:
                road_points = np.loadtxt(roadPointDataPath, dtype=np.float64,delimiter=',')
                road_end_points = np.loadtxt(roadEndPointDataPath, dtype=np.float64,delimiter=',')
            except:
                #20220225 產生座標參考點需要跑 track_create_road_point.ipynb
                #但 track_create_road_point.ipynb 需要車輛路徑紀錄 data/tracks%d.csv 要跑過這邊才會有
                #因此當座標參考點的檔案不存在時，預設使用camera3的值
                road_points = [[698.97437, 182.46745], 
                                    [409.28937,538.0], 
                                    [5.2457047,178.84816], 
                                    [699.0,273.52292]]

                road_end_points = [[697.86664,232.1254], 
                                        [5.0,407.64438], 
                                        [5.669643,140.2039], 
                                        [698.0,414.52475 ]]
    minStartX = min(np.array(road_points)[:,0])
    minStartY = min(np.array(road_points)[:,1])
    maxStartX = max(np.array(road_points)[:,0])
    maxStartY = max(np.array(road_points)[:,1])
    minEndX = min(np.array(road_end_points)[:,0])
    minEndY = min(np.array(road_end_points)[:,1])
    maxEndX = max(np.array(road_end_points)[:,0])
    maxEndY = max(np.array(road_end_points)[:,1])

    if task != None:
        '''
        'videoDate': '2023-11-29',
        'videoStartTime': '08:00:00',
        'videoEndTime': '09:00:00',
        '''
        if 'videoDate' in task.setting and 'videoStartTime' in task.setting:
            jsonData["videoStartTime"] = f'{task.setting["videoDate"]} {task.setting["videoStartTime"]}'
            jsonData["videoEndTime"] = f'{task.setting["videoDate"]} {task.setting["videoEndTime"]}'
        if 'road_1_name' in task.setting:
            jsonData['road_1_name'] = task.setting['road_1_name']
        if 'road_2_name' in task.setting:
            jsonData['road_2_name'] = task.setting['road_2_name']
        if 'road_3_name' in task.setting:
            jsonData['road_3_name'] = task.setting['road_3_name']
        if 'road_4_name' in task.setting:
            jsonData['road_4_name'] = task.setting['road_4_name']

    return

def run_for_task(path, cid, tid):
    args = parse_args2(path)
    #print(args)
    '''
    parser.add_argument("--config_detection", type=str, default="./configs/yolov7.yaml")
    parser.add_argument("--config_deepsort", type=str, default="./configs/deep_sort.yaml")
    '''
    cfg = get_config()
    cfg.merge_from_file(args.config_detection)
    cfg.merge_from_file(args.config_deepsort)
    reset_jsonData()
    readSetting2(args, path, cid, tid)
    
    sqlite_createTable()

    with torch.no_grad(): #發現在colab運行時，沒加這行會OOM，這邊也加一下避免浪費記憶體
        with VideoTracker(cfg, args, video_path=path, task_id=tid) as vdo_trk:
            vdo_trk.run()
            
if __name__ == "__main__":
    '''
    car = Car(0, 2)
    car.add(50, 100)
    car.add(50, 98)
    car.add(50, 95)
    car.add(50, 90)
    car.add(48, 85)
    car.add(46, 80)
    car.add(42, 70)
    car.add(40, 60)
    car.add(35, 55)
    car.add(30, 50)
    car.add(20, 50)
    car.add(10, 50)
    car.add(0, 50)
    r1 = car.predict()
    print(r1)
    
    
    car = Car(1, 2)
    car.add(50, 100)
    car.add(50, 98)
    car.add(50, 95)
    car.add(50, 90)
    car.add(48, 85)
    car.add(46, 80)
    car.add(48, 70)
    car.add(50, 60)
    car.add(51, 50)
    car.add(50, 40)
    car.add(50, 30)
    car.add(50, 20)
    car.add(50, 10)
    r1 = car.predict()
    print(r1)

    car = Car(2, 2)
    car.add(50, 100)
    car.add(50, 98)
    car.add(50, 95)
    car.add(50, 90)
    car.add(52, 85)
    car.add(58, 80)
    car.add(60, 70)
    car.add(65, 60)
    car.add(72, 55)
    car.add(80, 50)
    car.add(85, 50)
    car.add(90, 50)
    car.add(100, 50)
    r1 = car.predict()
    print(r1)
    
    
    car = Car(3, 2)
    car.add(50, 100)
    car.add(52, 98)
    car.add(54, 90)
    car.add(60, 80)
    car.add(62, 70)
    car.add(68, 60)
    car.add(70, 50)
    car.add(75, 60)
    car.add(77, 70)
    car.add(80, 80)
    car.add(80, 90)
    car.add(80, 95)
    car.add(40, 100)
    r1 = car.predict()
    print(r1)
    '''

    
    args = parse_args()
    cfg = get_config()
    cfg.merge_from_file(args.config_detection)
    cfg.merge_from_file(args.config_deepsort)



    reset_jsonData()
    readSetting(args)
    

    sqlite_createTable()

    with torch.no_grad(): #發現在colab運行時，沒加這行會OOM，這邊也加一下避免浪費記憶體
        with VideoTracker(cfg, args, video_path=args.VIDEO_PATH) as vdo_trk:
            vdo_trk.run()
    #print(jsonData)
    
    #print(dataOut)
    

    

    
    #jsonOutputPath = self.args.save_path + "json/"
    #outputFileName = jsonOutputPath + str(cameraNo) + "_" + videoStartTime_ori + ".txt"
    #file = open(outputFileName, 'w')
    #file.write(dataOut)
    #file.close()
    #test_draw_track()




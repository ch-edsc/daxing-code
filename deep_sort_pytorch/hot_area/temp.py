# -*- coding: utf-8 -*-
import os
import sys
import cv2 as cv
import numpy as np


def show(mat, name='___', scale = 1, pause = True):
    #運算過程的 show 圖放這邊，到時候要關掉直接把下面改 False 就好
    if True:
        if scale != 1:
            h, w, c = mat.shape
            scaleMat = cv.resize(mat, (int(w * scale), int(h * scale)))
            cv.imshow(name, scaleMat)
        else:
            cv.imshow(name, mat)
        if pause :
            cv.waitKey(0)  
    return

def exit(code=0):
    # OpenCV 結束前不把它產生的視窗關閉的話會 crash
    cv.destroyAllWindows()
    sys.exit(code) 
    
def matToHotArea(mat1, mat2):
    gray1 = cv.cvtColor(mat1, cv.COLOR_BGR2GRAY)
    __, gray1 = cv.threshold(gray1, 10, 255, cv.THRESH_BINARY)
    gray2 = cv.cvtColor(mat2, cv.COLOR_BGR2GRAY)
    __, gray2 = cv.threshold(gray2, 10, 255, cv.THRESH_BINARY)
    area1 = np.zeros(gray1.shape, np.bool_)
    area1[gray1 > 10] = 1 
    
    area2 = np.zeros(gray1.shape, np.bool_)
    area2[gray2 > 10] = 1 
    arr =  []
    arr.append(area1.tolist())
    arr.append(area2.tolist())
    return arr
    
tar = ["in1" , "in2", "in3", "in4", "out1", "out2", "out3", "out4"]
for t in tar:
    img = cv.imread("../output/%s.png" %t)
    R = np.zeros(img.shape, np.uint8)
    
    R = cv.add(R, (0, 0, 255, 0))           
    R = cv.bitwise_and(R, img)
    R[R < 200] = 0
    
    cv.imwrite("../output/%s_a.png" %t, R)
    G = np.zeros(img.shape, np.uint8)
    
    G = cv.add(G, (0, 255, 0, 0))           
    G = cv.bitwise_and(G, img)
    G[G < 200] = 0
    
    cv.imwrite("../output/%s_b.png" %t, G)
print("end..")
'''
arr = matToHotArea(R, G)

print(len(arr))
print(len(arr[0]))
print(len(arr[0][0]))
exit(0)
'''





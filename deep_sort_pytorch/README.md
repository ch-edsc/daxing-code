## 安裝 requirements  
cd xxx\daxing-code  
pip3 install -r requirements.txt  
*** 如果主機有 GPU 且有安裝 CUDA ，可以先編輯 daxing-code\requirements.txt  ，加上 torch==1.10.0+cu102 torchvision==0.10.0+cu102 torchaudio==0.10.0+cu102  (cu102 請依照主機的 cuda 版本修改)  

cd xxx\daxing-code\deep_sort_pytorch  
pip3 install -r requirements.txt  


## 設定檔：
請先把 .env.example 複製一份命名為 .env  
MINIO_SERVER填 monio bucket url 例如 127.0.0.1:9020
MINIO_ACCESS_KEY、MINIO_SECRET_KEY 填帳密

## 下載模型檔
請到 https://drive.google.com/drive/folders/1ZOdW2mmkTXL4uOuQ0PTy9tRAFpx2rilf?usp=drive_link 下載模型檔與模型設定檔  
yolov7-w6.pt 放到  daxing-code\deep_sort_pytorch\detector\YOLOv7\data\yolov7-daxing.pt  
ckpt.t7 放到 daxing-code\deep_sort_pytorch\deep_sort\deep\checkpoint\ckpt.t7  
以及把 db.sqlite3 放到 daxing-code\deep_sort_pytorch 資料夾  

## 如果DB要清掉重建
砍掉 daxing-code\deep_sort_pytorch\db.sqlite3  
把 xxx\daxing-code\deep_sort_pytorch\app\migrations 資料夾砍掉，然後  
```
cd xxx\daxing-code\deep_sort_pytorch  
python3 manage.py makemigrations app
python3 manage.py migrate app
```

## 啟動
cd xxx\daxing-code\deep_sort_pytorch  
```
#直接運行，port 7100 可依需求更換
python3 manage.py runserver 0.0.0.0:7100
#背景執行
nohup python3 manage.py runserver 0.0.0.0:7100 &
#若要關閉背景執行的程式
ps aux | grep 'python3 manage.py runserver 0.0.0.0:7100'
會看到
主機名稱  程序ID  0.9  9.0 17436576 2961112 ?    Sl   2月27  94:33 python3 manage.py runserver 0.0.0.0:7100
則
kill -9 程序ID
其中，程序ID是一串數字例如 97165
```
from .YOLOv3 import YOLOv3
from .YOLOR import YOLOR
from .YOLOv7 import YOLOv7


__all__ = ['build_detector', 'build_yolor_detector', 'build_yolov7_detector']

def build_detector(cfg, use_cuda):
    return YOLOv3(cfg.YOLOV3.CFG, cfg.YOLOV3.WEIGHT, cfg.YOLOV3.CLASS_NAMES, 
                    score_thresh=cfg.YOLOV3.SCORE_THRESH, nms_thresh=cfg.YOLOV3.NMS_THRESH, 
                    is_xywh=True, use_cuda=use_cuda)


def build_yolor_detector(opt, use_cuda):
    return YOLOR(opt)

def build_yolov7_detector(opt, use_cuda):
    return YOLOv7(opt)
    
#def build_yolor_detector(cfg, use_cuda):
#    return YOLOR(cfg.YOLOV3.CFG, cfg.YOLOV3.WEIGHT, cfg.YOLOV3.CLASS_NAMES, 
#                    score_thresh=cfg.YOLOV3.SCORE_THRESH, nms_thresh=cfg.YOLOV3.NMS_THRESH, 
#                    is_xywh=True, use_cuda=use_cuda)
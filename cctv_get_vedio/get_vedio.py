# -*- coding: utf-8 -*-
import requests
import re
import time
import os

cctv_host = "http://cctv.focgc03.com.tw/hls/"
cctv_url = cctv_host + "camera3.m3u8" #抓哪個攝影機， camera1.m3u8 camera2.m3u8 ... etc
save_path = "data/vedio/"

if not os.path.exists(save_path):
    os.makedirs(save_path)


# m3u8 格式參照 https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/435249/


# 目前片段位置起點是這個標籤 #EXT-X-MEDIA-SEQUENCE:
seqNumber = 0
nowGetNumber = 0 # 現在抓到哪段影像

getVediosCount = 0 # 總共抓幾個片段了
totalVedioList = [] # 所有片段的檔名，最後組裝 m3u8 用

getHowMany = 60 # 要抓多少片段

vedioSeqStart = -1 # 紀錄 EXT-X-MEDIA-SEQUENC


# ...嗯...抓太快會被 ban 掉
# ConnectionRefusedError: [WinError 10061] 無法連線，因為目標電腦拒絕連線。
# 設置36秒抓一次->會漏抓...改10秒問一次
while getVediosCount < getHowMany:
    # 首先，抓播放列表回來
    r = requests.get(cctv_url)
    lines = r.text.split("\n")
    print(r.text)
    vedioList = []
    for i in range(len(lines)):
        #print("line:" + str(i))
        #print(lines[i])
        if(len(lines[i]) > 0):
            if lines[i].find('#EXT-X-MEDIA-SEQUENCE:') != -1:
                seqNumber = int(re.sub("\D","", lines[i])) # 用 re 把該行非數字(\D)通通換成""，也就是砍掉非數字
                print('get MEDIA-SEQUENCE: ' + str(seqNumber))
                if vedioSeqStart == -1:
                    vedioSeqStart = seqNumber
            if lines[i][0] != '#':
                vedioList.append(lines[i])
                
    if nowGetNumber < seqNumber: # 如果有新影片就抓下來
        for i in range(len(vedioList)):
            print('download: ' + vedioList[i])
            downloadUrl = cctv_host + vedioList[i]
            r = requests.get(downloadUrl)
            file = open(save_path + vedioList[i], "wb")
            file.write(r.content)
            file.close()
            getVediosCount += 1
            targetNumber = int(re.sub("\D","", vedioList[i].split('-')[1]))
            if nowGetNumber < targetNumber:
                nowGetNumber = targetNumber
            totalVedioList.append(vedioList[i])
    #time.sleep(36) # 每36秒抓一輪
    time.sleep(10) # 每36秒抓一輪會漏抓，改成10秒問一次
    
#然後組 m3u8
head = '''
#EXTM3U
#EXT-X-VERSION:3
#EXT-X-MEDIA-SEQUENCE:%d
#EXT-X-TARGETDURATION:12
''' % (vedioSeqStart)
            
extinf = "#EXTINF:12.000,\n"

file = open(save_path + "camera.m3u8", "w")
file.write(head)
for name in totalVedioList:
    file.write(extinf)
    file.write(name + '\n')
    
file.close()
print("end")
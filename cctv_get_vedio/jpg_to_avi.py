# -*- coding: utf-8 -*-

#from https://stackoverflow.com/questions/44947505/how-to-make-a-movie-out-of-images-in-python
import cv2
import os

image_folder = "data/img"
video_name = 'data/camera.avi'

images = [img for img in os.listdir(image_folder) if img.endswith(".jpg")]
frame = cv2.imread(os.path.join(image_folder, images[0]))
height, width, layers = frame.shape

video = cv2.VideoWriter(video_name, 0, 7, (width,height))

#for image in images:
for i in range(852):
    video.write(cv2.imread(os.path.join(image_folder, str(i) + ".jpg")))

cv2.destroyAllWindows()
video.release()

print('end')
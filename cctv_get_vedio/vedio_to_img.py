# -*- coding: utf-8 -*-
import os
import cv2 as cv

vedio_path = "data/vedio/"
fileList = [f for f in os.listdir(vedio_path) if f.endswith('.ts')]
imgNumber = 0
image_save_path = "data/img/"
if not os.path.exists(image_save_path):
    os.makedirs(image_save_path)


for filePath in fileList:
    print(filePath)
    cap = cv.VideoCapture(vedio_path + filePath)
    captureName = filePath.split('.')[0]
    if cap.isOpened():
        while(True):
            isOK, frame = cap.read()
            if isOK:
                #cv.imshow('frame', frame)
                #cv.waitKey(30)
                #cv.imwrite(image_save_path + captureName + '_' + str(imgNumber) + '.jpg', frame)
                cv.imwrite(image_save_path + str(imgNumber) + '.jpg', frame)
                imgNumber += 1
            else:
                break
    cap.release()

cv.destroyAllWindows()
print("end")

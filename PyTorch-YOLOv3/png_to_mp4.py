# -*- coding: utf-8 -*-

#from https://stackoverflow.com/questions/44947505/how-to-make-a-movie-out-of-images-in-python
import cv2
import os

image_folder = "output/"
#video_name = 'output/detect_by_yolo_pretrain_model.mp4'
video_name = 'output/detect_by_yolo_custom_model.mp4'
#image_folder = "data/samples_b/"
#video_name = 'data/camera3_10min.mp4'
images = [img for img in os.listdir(image_folder) if img.endswith(".png")]
frame = cv2.imread(os.path.join(image_folder, images[0]))
height, width, layers = frame.shape

#output 時，設置FPS 4.8 出來的影像時長才正常...否則變成了快轉影片？
video = cv2.VideoWriter(video_name,  cv2.VideoWriter_fourcc(*'MP4V'), 7, (width,height))


#video_name = 'output/detect_by_yolo_pretrain_model.avi'
#video = cv2.VideoWriter(video_name,  cv2.VideoWriter_fourcc(*'XVID'), 4.8, (width,height))
#for image in images:
for i in range(4186):
    path = os.path.join(image_folder, "frame_%06d.png" % (i))
#for i in range(4200):
#    path = os.path.join(image_folder, "%d.png" % (i))
    print("encode: " + path)
    video.write(cv2.imread(path))

cv2.destroyAllWindows()
video.release()

print('end')
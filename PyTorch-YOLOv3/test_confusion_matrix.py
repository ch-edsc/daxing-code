#! /usr/bin/env python3
from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from utils.augmentations import *
from utils.transforms import *
from utils.parse_config import *

import os
import argparse
import tqdm

from terminaltables import AsciiTable

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable

from raiwidgets import ErrorAnalysisDashboard
import cv2 as cv
import sys
import numpy as np


# python raiwidgets_test.py --model config/yolov3.cfg --data config/custom.data --weights checkpoints/yolov3_ckpt_200.weights
# python raiwidgets_test.py --model config/yolov3.cfg --data config/custom2.data --weights weights/yolov3.weights
def exit(code=0):
    # OpenCV 結束前不把它產生的視窗關閉的話會 crash
    cv.destroyAllWindows()
    sys.exit(code) 

def show(mat, name='___', scale = 1, pause = True):
    #運算過程的 show 圖放這邊，到時候要關掉直接把下面改 False 就好
    if True:
        if scale != 1:
            h, w, c = mat.shape
            scaleMat = cv.resize(mat, (int(w * scale), int(h * scale)))
            cv.imshow(name, scaleMat)
        else:
            cv.imshow(name, mat)
        if pause :
            cv.waitKey(0)  
    return

def evaluate_model_file(model_path, weights_path, img_path, class_names,
    batch_size=8, img_size=416, n_cpu=8,
    iou_thres=0.5, conf_thres=0.5, nms_thres=0.5, verbose=True):
    """Evaluate model on validation dataset.

    :param model_path: Path to model definition file (.cfg)
    :type model_path: str
    :param weights_path: Path to weights or checkpoint file (.weights or .pth)
    :type weights_path: str
    :param img_path: Path to file containing all paths to validation images.
    :type img_path: str
    :param class_names: List of class names
    :type class_names: [str]
    :param batch_size: Size of each image batch, defaults to 8
    :type batch_size: int, optional
    :param img_size: Size of each image dimension for yolo, defaults to 416
    :type img_size: int, optional
    :param n_cpu: Number of cpu threads to use during batch generation, defaults to 8
    :type n_cpu: int, optional
    :param iou_thres: IOU threshold required to qualify as detected, defaults to 0.5
    :type iou_thres: float, optional
    :param conf_thres: Object confidence threshold, defaults to 0.5
    :type conf_thres: float, optional
    :param nms_thres: IOU threshold for non-maximum suppression, defaults to 0.5
    :type nms_thres: float, optional
    :param verbose: If True, prints stats of model, defaults to True
    :type verbose: bool, optional
    :return: Returns precision, recall, AP, f1, ap_class
    """
    dataloader = _create_validation_data_loader(img_path, batch_size, img_size, n_cpu)
    model = load_model(model_path, weights_path)
    metrics_output = _evaluate(
        model,
        dataloader,
        class_names,
        img_size,
        iou_thres,
        conf_thres,
        nms_thres,
        verbose)
    return metrics_output

def print_eval_stats(metrics_output, class_names, verbose):
    if metrics_output is not None:
        precision, recall, AP, f1, ap_class = metrics_output

        if verbose:
            # Prints class AP and mean AP
            #ap_table = [["Index", "Class", "AP"]]
            ap_table = [["Index", "Class", "AP", "Precision", "Recall", "F1"]]
            for i, c in enumerate(ap_class):
                #ap_table += [[c, class_names[c], "%.5f" % AP[i]]]
                ap_table += [[c, class_names[c], "%.5f" % AP[i], "%.5f" % precision[i], "%.5f" % recall[i], "%.5f" % f1[i]]]
            print(AsciiTable(ap_table).table)
        print(f"---- mAP {AP.mean():.5f} ----")
    else:
        print( "---- mAP not measured (no detections found by model) ----")

def _evaluate(model, dataloader, class_names, img_size, iou_thres, conf_thres, nms_thres, verbose):
    """Evaluate model on validation dataset.

    :param model: Model to evaluate
    :type model: models.Darknet
    :param dataloader: Dataloader provides the batches of images with targets
    :type dataloader: DataLoader
    :param class_names: List of class names
    :type class_names: [str]
    :param img_size: Size of each image dimension for yolo
    :type img_size: int
    :param iou_thres: IOU threshold required to qualify as detected
    :type iou_thres: float
    :param conf_thres: Object confidence threshold
    :type conf_thres: float
    :param nms_thres: IOU threshold for non-maximum suppression
    :type nms_thres: float
    :param verbose: If True, prints stats of model
    :type verbose: bool
    :return: Returns precision, recall, AP, f1, ap_class
    """
    
    isNoBike = True
    
    model.eval()  # Set model to evaluation mode

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    labels = []
    sample_metrics = []  # List of tuples (TP, confs, pred)
    
    true_classNum = len(class_names) + 1
    classNum = 6
    total_confusion_matrix = []
    for i in range(classNum):
        tempArray = []
        for j in range(classNum):
            tempArray.append(0)
        total_confusion_matrix.append(tempArray)
    
    
    x_test = []
    feature_names = []
    classifyLists = [[],[]]
    #跑全部太花時間，測試時跑少一點，若要全跑就把 breakCount 設 -1
    breakCount = -1
    for _, imgs, targets in tqdm.tqdm(dataloader, desc="Validating"):
        # Extract labels
        labels += targets[:, 1].tolist()
        # Rescale target
        targets[:, 2:] = xywh2xyxy(targets[:, 2:])
        targets[:, 2:] *= img_size
        
        #print("imageSize:")
        #print(len(imgs))
        #print(imgs[0].shape)
        for img in imgs:  
            #把輸入轉numpy
            #x_one_test = img.cpu().detach().numpy()
            
            # ValueError: too many values to unpack (expected 2)
            # 看來 input 的 feature 塞太多 (3*416*416) 會跑不了...那我 resize 再傳
            # OpenCV 吃 W,H,CH 而 torch 吃 CH, W, H，所以要先轉換維度順序
            x_one_test = img.permute(1, 2, 0)
            x_one_test = x_one_test.cpu().detach().numpy()
            #然後若要 show 影像，OpenCV 吃 BGR 
            #x_one_test = cv.cvtColor(x_one_test, cv.COLOR_RGB2BGR)
            #show(x_test)
            x_one_test = cv.resize(x_one_test, (8, 8))
            #x_one_test = x_one_test.reshape((8, -1))
            x_one_test = x_one_test.flatten()

            if len(feature_names) == 0:
                #print("feature_names init...")
                #print(x_one_test)
                for i in range(x_one_test.size):
                    feature_names.append("feature_%d" % i)
                #print("feature_names init OK")
            
            
        imgs = Variable(imgs.type(Tensor), requires_grad=False)

        with torch.no_grad():
            outputs = to_cpu(model(imgs))
            outputs = non_max_suppression(outputs, conf_thres=conf_thres, iou_thres=nms_thres)
        if isNoBike:
            '''
            print("outs")
            print(outputs)
            print("tars")
            print(targets)
            print("extra")
            print(outputs[0][0])
            print(outputs[0][1])
            print(targets[0])
            print(targets[1])
            print("===")
            '''
            #print(outputs)
            for i in range(len(outputs)):
                for j in range(len(outputs[i])):
                    #outputs[i][j, 5] = 3
                    if outputs[i][j, 5] == 0:
                        #print("FIND")
                        outputs[i][j, 5] = 3
            #print(outputs)
            for i in range(len(targets)):
                    if targets[i, 1] == 0:
                        #print("FIND")
                        targets[i, 1] = 3
        temp_target_metrics, temp_confusion_matrix, classifyList = get_batch_statistics_with_confusion_matrix(outputs, targets, iou_threshold=iou_thres, class_num=true_classNum, isNeedSwapClass=False)
        sample_metrics += temp_target_metrics
        
        for i in range(len(temp_confusion_matrix)):
            for j in range(len(temp_confusion_matrix[i])):
                total_confusion_matrix[i][j] += temp_confusion_matrix[i][j]
                

        #for i in range(len(classifyList[0])):
        #    x_test.append(x_one_test)
        classifyLists[0] += classifyList[0]
        classifyLists[1] += classifyList[1]

        if breakCount == 0:
            break
        breakCount -= 1
        
    
    if len(sample_metrics) == 0:  # No detections over whole validation set.
        print("---- No detections over whole validation set ----")
        return None
    '''
    print("sample_metrics")
    print(sample_metrics[0])
    print("labels")
    print(labels)
    '''
    # Concatenate sample statistics
    true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_metrics))]
    metrics_output = ap_per_class(true_positives, pred_scores, pred_labels, labels)
    '''
    print("true_positives")
    print(true_positives)
    print(len(true_positives))
    print("pred_scores")
    print(pred_scores)
    print("pred_labels")
    print(pred_labels)
    print(len(pred_labels))
    print("labels")
    print(labels)
    print(len(labels))
    
    print("class_names")
    print(class_names)
    '''

    #把結果轉 numpy 
    #y_test = np.array(classifyLists[0])
    #predictions = np.array(classifyLists[1])
    #x_test = np.array(x_test)
    '''
    print("y_true")
    print(y_test)
    print("y_pred")
    print(predictions)
    print(len(y_test))
    print(len(predictions))
    print(len(x_test))
    print(len(feature_names))
    print(len(x_test[0]))
    '''
    # ValueError: too many values to unpack (expected 2)
    # 看來 input 的 feature 塞太多 (3*416*416) 會跑不了...那我 resize 再傳
    

    
    #ErrorAnalysisDashboard(dataset=x_test, true_y=y_test, features=feature_names, pred_y=predictions)
    #.....咦？ 出現 <IPython.core.display.HTML object>
    #ErrorAnalysisDashboard 只能用 Ipython 跑喔 = = ....
    #好吧我改寫到 jupyter notebook 去


    if True:
        print_eval_stats(metrics_output, class_names, verbose)
        #print("metrics_output")
        #print(metrics_output)
        total_confusion_matrix = np.array(total_confusion_matrix)
        if isNoBike:
            total_confusion_matrix = total_confusion_matrix[1:, 1:]
            cmName = ['BigCar','SmallCar','MotorBike','Person']
            cmName.append("Background")
        else:
            cmName = ['Bike','BigCar','SmallCar','MotorBike','Person']
            cmName.append("Background")
        #print("total_confusion_matrix")
        #print(total_confusion_matrix)
        plt.matshow(total_confusion_matrix, cmap = plt.cm.Reds)
        #plt.colorbar()
        for i in range(len(total_confusion_matrix)):
            for j in range(len(total_confusion_matrix)):
                plt.annotate(total_confusion_matrix[j,i], xy = (i, j), horizontalalignment='center', verticalalignment='center')
        plt.xlabel('Predicted')
        plt.ylabel('True Label')
    
        #print(cmName)
        index_ls = []
        for i in range(len(cmName)):
            index_ls.append(i)
        plt.xticks(index_ls, cmName)
        plt.yticks(index_ls, cmName)    
        plt.show()
        
        
    return metrics_output

def _create_validation_data_loader(img_path, batch_size, img_size, n_cpu):
    """Creates a DataLoader for validation.

    :param img_path: Path to file containing all paths to validation images.
    :type img_path: str
    :param batch_size: Size of each image batch
    :type batch_size: int
    :param img_size: Size of each image dimension for yolo
    :type img_size: int
    :param n_cpu: Number of cpu threads to use during batch generation
    :type n_cpu: int
    :return: Returns DataLoader
    :rtype: DataLoader
    """
    dataset = ListDataset(img_path, img_size=img_size, multiscale=False, transform=DEFAULT_TRANSFORMS)
    dataloader = DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=False,
        num_workers=n_cpu,
        pin_memory=True,
        collate_fn=dataset.collate_fn)
    return dataloader

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate validation data.")
    parser.add_argument("-m", "--model", type=str, default="config/yolov3.cfg", help="Path to model definition file (.cfg)")
    parser.add_argument("-w", "--weights", type=str, default="weights/yolov3.weights", help="Path to weights or checkpoint file (.weights or .pth)")
    parser.add_argument("-d", "--data", type=str, default="config/coco.data", help="Path to data config file (.data)")
    parser.add_argument("-b", "--batch_size", type=int, default=8, help="Size of each image batch")
    parser.add_argument("-v", "--verbose", action='store_true', help="Makes the validation more verbose")
    parser.add_argument("--img_size", type=int, default=416, help="Size of each image dimension for yolo")
    parser.add_argument("--n_cpu", type=int, default=8, help="Number of cpu threads to use during batch generation")
    parser.add_argument("--iou_thres", type=float, default=0.5, help="IOU threshold required to qualify as detected")
    parser.add_argument("--conf_thres", type=float, default=0.5, help="Object confidence threshold")
    parser.add_argument("--nms_thres", type=float, default=0.5, help="IOU threshold for non-maximum suppression")
    args = parser.parse_args()
    print(args)

    # Load configuration from data file
    data_config = parse_data_config(args.data)
    valid_path = data_config["valid"]  # Path to file containing all images for validation
    class_names = load_classes(data_config["names"])  # List of class names

    precision, recall, AP, f1, ap_class = evaluate_model_file(
        args.model,
        args.weights,
        valid_path,
        class_names,
        batch_size=args.batch_size,
        img_size=args.img_size,
        n_cpu=args.n_cpu,
        iou_thres=args.iou_thres,
        conf_thres=args.conf_thres,
        nms_thres=args.nms_thres,
        verbose=True)
    # OpenCV 結束前不把它產生的視窗關閉的話會 crash
    # cv.destroyAllWindows()

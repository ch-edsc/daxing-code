下載 pretrained weight

https://drive.google.com/uc?export=download&id=1Tdn3yqpZ79X7R1Ql0zNlNScB1Dv9Fp76

https://drive.google.com/uc?export=download&id=1UflcHlN5ERPdhahMivQYCbWWw7d2wY7U

有 yolor_p6.pt 與 yolor_w6.pt ， w6 大約300mb，比p6大一倍


-------
訓練


比較訓練用的設定檔
coco.yaml
內容是
# train and val datasets (image directory or *.txt file with image paths)
train: ../coco/train2017.txt  # 118k images
val: ../coco/val2017.txt  # 5k images
test: ../coco/test-dev2017.txt  # 20k images for submission to https://competitions.codalab.org/competitions/20794

# number of classes
nc: 80

# class names
names: ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
        'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird', 'cat', 'dog', 'horse', 'sheep', 'cow',
        'elephant', 'bear', 'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee',
        'skis', 'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove', 'skateboard', 'surfboard',
        'tennis racket', 'bottle', 'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
        'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'chair', 'couch',
        'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard', 'cell phone',
        'microwave', 'oven', 'toaster', 'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors', 'teddy bear',
        'hair drier', 'toothbrush']


依該格式建立一個 car.yaml

# train and val datasets (image directory or *.txt file with image paths)
train: data/custom/train.txt 
val: data/custom/valid.txt 
test: data/custom/test.txt 

# number of classes
nc: 80

# class names
names: ['Bike', 'BigCar', 'SmallCar', 'MotorBike', 'Person', 'null', 'null', 'null', 'null', 'null',
        'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null',
        'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null',
        'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null',
        'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null',
        'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null',
        'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null',
        'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null', 'null']



然後是
cfg/yolor_p6.cfg
內容是模型設置，大致不用改，只把batch size從64改2


hyp.scratch.1280.yaml
裡面是超參數，不用改
cd C:\Users\leo_1\Documents\GitHub\daxing-code\yolor
python train.py --batch-size 2 --img 1280 1280 --data car.yaml --cfg cfg/yolor_p6.cfg --weights '' --device 0 --name yolor_p6 --hyp hyp.scratch.1280.yaml --epochs 10

========
ModuleNotFoundError: No module named 'pycocotools'
有 module 沒安裝
cd C:\Users\leo_1\Documents\GitHub\daxing-code\yolor
pip install -qr requirements.txt
跑不了...那手動安裝
pip install pycocotools


=====
AttributeError: module 'torch.nn' has no attribute 'SiLU'
看來是 pyTorch 版本不夠新
pip install torch==1.7.0

ERROR: torch has an invalid wheel, .dist-info directory not found
參照 https://www.796t.com/article.php?id=211043
無效

參照 https://pytorch.org/get-started/locally/
conda install pytorch torchvision torchaudio cudatoolkit=10.1 -c pytorch-lts
嗯，這次可以了
=====

運行成功，接下來正式跑300個epoch，
python train.py --batch-size 2 --img 1280 1280 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights '' --device 0 --name yolor_p6 --hyp hyp.scratch.1280.yaml --epochs 300
....一個epoch大約要10分鐘，跑下去要大約2天，太久
先跑100 epoch 就好
python train.py --batch-size 2 --img 1280 1280 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights '' --device 0 --name yolor_p6 --hyp hyp.scratch.1280.yaml --epochs 100

========
訓練結果為
     Epoch   gpu_mem       box       obj       cls     total   targets  img_size
     99/99     4.99G   0.01007  0.008521 0.0005831   0.01917        25      1280: 100%|█| 1800/1800 [09:17<00:00,  3.23
               Class      Images     Targets           P           R      mAP@.5  mAP@.5:.95: 100%|█| 147/147 [00:25<00
                 all         586    9.33e+03       0.832        0.77       0.843       0.575

mAP除了.5還多一個.5:.95，查詢說明
https://blog.csdn.net/ruyingcai666666/article/details/109670567
指IOU， .5 表示重疊50%就算對，.95表示重疊95%才算對(也就是要抓得非常準)
.5:.95 表示從 50%、55%、60%....一直到95%所有mAP的平均

與YOLOv3比較的話應該是看 .5 的 mAP

然後跑test
python test.py --data data\custom\car.yaml --img 1280 --batch 2 --conf 0.001 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_p6/weights/best_ap50.pt --name car_val
嗯？label對應怪怪的，標籤顯示成coco80類的了

翻設定
parser.add_argument('--names', type=str, default='data/coco.names', help='*.cfg path')
嗯，我需要建立一個 car.names

python test.py --data data/custom/car.yaml --names data/custom/car.names --img 1280 --batch 2 --conf 0.001 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_p6/weights/best_ap50.pt --name car_val
然後他跑的好像是 val 的 dataset

從這邊設
parser.add_argument('--task', default='val', help="'val', 'test', 'study'")

python test.py --data data/custom/car.yaml --names data/custom/car.names --task test --img 1280 --batch 2 --conf 0.001 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_p6/weights/best_ap50.pt --name car_test


測試結果， val 比 yolov3好很多(從 0.336提升到0.905)，然後 test 一樣慘(是有好一點...從0.014提升到0.114)。很合理，因為test攝影機視角不同

然後跑detect

python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_p6/weights/best_ap50.pt --conf-thres 0.5 --img-size 1280 --device 0 --names data\custom\car.names

再來轉mp4，把以前跑YOLOv3的 png_to_mp4.py 拿過來用

....哇，效果好到我懷疑它是不是把val的data一起拿進去train了 = =....檢查一下我有沒有設錯
嗯，確認訓練是0~3599，評估是3600~4185，沒設錯

再來先去研究Paper與弄投影片，若還有時間的話，再改寫程式碼增加輸出混淆矩陣的功能

投影片弄完已經是6月30下午4點了，混淆矩陣趕不上這次開會，下週再弄了


------
做混淆矩陣
從test.py改，複製一份更名cm.py
python cm.py --data data/custom/car10.yaml --names data/custom/car.names --task val --img 1280 --batch 2 --conf-thres 0.4 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_p6/weights/best_ap50.pt --name car_test

重點位置看起來在
ious, i = box_iou(pred[pi, :4], tbox[ti]).max(1) 

for j in (ious > iouv[0]).nonzero(as_tuple=False):
這一帶

要算AP50所以 iouv[0]

pred[pi, :4]

tbox[ti]
數量差蠻多對不太起來..？
查一下 ap_per_class
裡面已經 sum 了不能產生混淆矩陣

喔.... conf-thres 的問題，設detect時的預設值0.4數量就比較對了
------
                            print("pre")
                            print(pred[pi[j], :4])
                            print("tar")
                            print(tbox[ti[i[j]]])
這樣可以抓到對應的框框

tcls_tensor 應該是目標類別



TODO: 加上輸出個別AP功能/混淆矩陣功能

模組化

1. 相同情況下 YOLOv3的輸出轉到YOLOR
2. pipeline 要分工好
3. 新的標記繼續訓練模型

======
總之先搞模組化的混淆矩陣
需要的fuction有
cm_init
input:
classNums
classNames
iouTh=0.5
內容為建立空的混淆矩陣用陣列

cm_add
input:
predictArray
targetArray
輸入一個畫面的標記以及預測結果，依據IOU計算重疊度，得到單一畫面的混淆矩陣統計

cm_output
輸出混淆矩陣
output就是混淆矩陣

cm_reset
清空混淆矩陣

=======
labels
[[          3     0.96274     0.30752    0.026352    0.068131]
 [          3     0.44832     0.87516     0.22511     0.21199]
 [          2     0.21147     0.24848    0.056358    0.049586]
 [          2     0.73609     0.29212     0.10778    0.080093]
 [          3     0.55971     0.29381    0.042168    0.059961]
 [          3     0.12212     0.37918    0.045049     0.10888]

pred
[[     451.75         839         751        1066     0.99219           3]
 [        917      273.75        1062      360.75     0.99072           2]
 [     246.38      242.62       321.5      296.75     0.98975           2]
 [      723.5       286.5       780.5       351.5     0.98193           3]
 [       32.5      311.75      118.12      424.25     0.97363           2]
 [       1278       296.5        1312       369.5      0.9668           3]
格式不太一樣，pred有乘影像長寬，而label是整張畫面為範圍0~1之間，另外類別一個在0一個在5

YOLOv3格式還沒看，先假設它和YOLOR相同吧


=====
python cm.py --data data/custom/car.yaml --names data/custom/car.names --task val --img 1280 --batch 2 --conf-thres 0.4 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_p6/weights/best_ap50.pt --name cm_val

python cm.py --data data/custom/car.yaml --names data/custom/car.names --task test --img 1280 --batch 2 --conf-thres 0.4 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_p6/weights/best_ap50.pt --name cm_test

=====
然後寫一個 create_datalist.py，根據資料夾中的圖片label與切分比例，自動產生train.txt/valid.txt/test.txt
=====
然後訓練camera3+camera9，與銜接mlflow
總之用 mlflow.log_param 存超參數
mlflow.log_metric 存量測值(準確率之類的)
mlflow.pytorch.log_model存模型
這次dataset比上週多一倍，訓練1個ep大約要20min，我希望一天訓練完，先train 50 ep 就好
python train.py --batch-size 2 --img 1280 1280 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights '' --device 0 --hyp hyp.scratch.1280.yaml --epochs 50 --name yolor_20210705_train


----
Traceback (most recent call last):
  File "train.py", line 551, in <module>
    train(hyp, opt, device, tb_writer, wandb)
  File "train.py", line 372, in train
    mlflow.log_metric(tag, x)
  File "C:\Users\leo_1\.conda\envs\colab\lib\site-packages\mlflow\tracking\fluent.py", line 434, in log_metric
    MlflowClient().log_metric(run_id, key, value, int(time.time() * 1000), step or 0)
  File "C:\Users\leo_1\.conda\envs\colab\lib\site-packages\mlflow\tracking\client.py", line 649, in log_metric
    self._tracking_client.log_metric(run_id, key, value, timestamp, step)
  File "C:\Users\leo_1\.conda\envs\colab\lib\site-packages\mlflow\tracking\_tracking_service\client.py", line 180, in log_metric
    _validate_metric(key, value, timestamp, step)
  File "C:\Users\leo_1\.conda\envs\colab\lib\site-packages\mlflow\utils\validation.py", line 87, in _validate_metric
    INVALID_PARAMETER_VALUE,
mlflow.exceptions.MlflowException: Got invalid value tensor(0.11100, device='cuda:0') for metric 'train/box_loss' (timestamp=1625481952617). Please specify value as a valid double (64-bit floating point)
----
喔...數值是tensor需要取出來
改 mlflow.log_metric(tag, x.cpu().detach().numpy())
然後先跑10張圖
python train.py --batch-size 2 --img 1280 1280 --data data/custom/car10.yaml --cfg cfg/yolor_p6.cfg --weights '' --device 0 --hyp hyp.scratch.1280.yaml --epochs 50 --name yolor_20210705_train


====
mlflow.exceptions.MlflowException: Invalid metric name: 'metrics/mAP_0.5:0.95'. Names may only contain alphanumerics, underscores (_), dashes (-), periods (.), spaces ( ), and slashes (/).
====
tags有mlflow不吃的字元
tag = tag.replace(":", "_")

====
Traceback (most recent call last):
  File "train.py", line 558, in <module>
    train(hyp, opt, device, tb_writer, wandb)
  File "train.py", line 435, in train
    mlflow.pytorch.log_model(ckpt, wdir / 'best_ap50.pt')
  File "C:\Users\leo_1\.conda\envs\colab\lib\site-packages\mlflow\pytorch\__init__.py", line 288, in log_model
    **kwargs,
  File "C:\Users\leo_1\.conda\envs\colab\lib\site-packages\mlflow\models\model.py", line 182, in log
    flavor.save_model(path=local_path, mlflow_model=mlflow_model, **kwargs)
  File "C:\Users\leo_1\.conda\envs\colab\lib\site-packages\mlflow\pytorch\__init__.py", line 450, in save_model
    raise TypeError("Argument 'pytorch_model' should be a torch.nn.Module")
TypeError: Argument 'pytorch_model' should be a torch.nn.Module
====
這YOLOR的model格式非torch.nn.Module

mlflow.pytorch.log_model(ckpt['model'], wdir / 'epoch_{:03d}.pt'.format(epoch))
改
mlflow.pytorch.log_model(model, wdir / 'epoch_{:03d}.pt'.format(epoch))
試試
=====
yaml.representer.RepresenterError: ('cannot represent an object', WindowsPath('runs/train/yolor_20210705_train12/weights/best_ap50.pt'))
====
檔名不能帶路徑？改
mlflow.pytorch.log_model(model, 'mlflow_best_ap50.pt')
似乎正常了，跑全部樣本
python train.py --batch-size 2 --img 1280 1280 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights '' --device 0 --hyp hyp.scratch.1280.yaml --epochs 50 --name yolor_20210705_train
======
RuntimeError: CUDA out of memory. Tried to allocate 2.00 MiB (GPU 0; 8.00 GiB total capacity; 3.95 GiB already allocated; 0 bytes free; 4.61 GiB reserved in total by PyTorch)
不確定是GPU記憶體不足還是電腦不穩，再跑一次看看
還是在epoch5 oom.....batch size 再減半吧，嗯....剩1，batch norm 層變空氣好像不太好
那改成 image size 減半....640
python train.py --batch-size 2 --img 640 640 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights '' --device 0 --hyp hyp.scratch.640.yaml --epochs 50 --name yolor_20210705_train

python train_org.py --batch-size 2 --img 1280 1280 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights 'pretrain/yolor_p6.pt' --device 0 --hyp hyp.scratch.1280.yaml --epochs 50 --name yolor_20210706B_train

python train.py --batch-size 2 --img 640 640 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights 'pretrain/yolor_p6.pt' --device 0 --hyp hyp.scratch.640.yaml --epochs 50 --name yolor_20210706B_train



python cm.py --data data/custom/car.yaml --names data/custom/car.names --task val --img 640 --batch 2 --conf-thres 0.4 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --name val_0706

20210706接到指示要跑yolor原始模型做比較
python cm.py --data data/custom/car.yaml --names data/custom/car.names --task val --img 640 --batch 2 --conf-thres 0.4 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights pretrain/yolor_p6.pt --name val_coco_0706

======

python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --conf-thres 0.5 --img-size 640 --device 0 --names data/custom/car.names --source data/camera3/obj_train_data --output inference/output_camera3

python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --conf-thres 0.5 --img-size 640 --device 0 --names data/custom/car.names --source data/camera9/obj_train_data --output inference/output_camera9


python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --conf-thres 0.5 --img-size 640 --device 0 --names data/custom/car.names --source data/camera14/obj_train_data --output inference/output_camera14


python cm.py --data data/custom/car.yaml --names data/custom/car.names --task test --img 640 --batch 2 --conf-thres 0.4 --iou 0.5 --device 0 --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --name test_0706



==========
上週的mlflow啟動方式，沒辦法註冊模型....啟動mlflow時要改成
mlflow server --backend-store-uri sqlite:///mlflow.db --default-artifact-root ./mlrun --host 127.0.0.1

然後以後跑training/detect時都要先啟動 mlflow server
另外 training 時程式碼要一開始加上

mlflow.set_tracking_uri("http://localhost:5000")
try:
    experiment_id = mlflow.create_experiment(name = "yolor")
except:
    print("Experiment test_iris already exists")
    experiment_id = '1'


訓練時加上
with mlflow.start_run(experiment_id = experiment_id):


======
python train.py --batch-size 2 --img 640 640 --data data/custom/car10.yaml --cfg cfg/yolor_p6.cfg --weights 'runs/train/yolor_20210705_train/weights/best_ap50.pt' --device 0 --hyp hyp.scratch.640.yaml --epochs 5 --name yolor_20210720


python train.py --batch-size 2 --img 640 640 --data data/custom/car.yaml --cfg cfg/yolor_p6.cfg --weights 'runs/train/yolor_20210705_train/weights/best_ap50.pt' --device 0 --hyp hyp.scratch.640.yaml --epochs 5 --name yolor_20210720

======
然後訓練完，瀏覽器開啟 127.0.0.1:5000 ，在網頁端註冊模型
模型name 我註冊 yolor_car

預測時




python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights mlflow --conf-thres 0.5 --img-size 640 --device 0 --names data/custom/car.names --source data/custom/test10 --output inference/0720


python detect.py --source data/obj_train_data/ --cfg cfg/yolor_p6.cfg --weights runs/train/yolor_20210705_train/weights/best_ap50.pt --conf-thres 0.5 --img-size 640 --device 0 --names data/custom/car.names --source data/custom/test10 --output inference/0720
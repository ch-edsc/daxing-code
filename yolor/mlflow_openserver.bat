set root=C:\ProgramData\Anaconda3
call %root%\Scripts\activate.bat %root%
call conda activate colab
call mlflow server --backend-store-uri sqlite:///mlflow.db --default-artifact-root ./mlruns --host 127.0.0.1
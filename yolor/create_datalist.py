# -*- coding: utf-8 -*-

import os
import math
import random

file = open("data/custom/setting.txt", "r", encoding="utf-8")
outputDir = 'data/custom/'
train = []
val = []
test = []
line = file.readline()
while line:
    args = line.split("#")[0].replace(" ", "").replace("\r", "").replace("\n", "").replace("\t", "").split(":")
    if len(args) > 0:
        action = args[0]
        if action == "dataPath":
            if len(args) > 1:
                dataPath = args[1]
        if action == "split":
            splitRates = args[1].split(",")
            #print(splitRates)
            trainRate = float(splitRates[0])
            valRate = float(splitRates[1])
            if len(splitRates) > 2:
                testRate = float(splitRates[2])
            else:
                testRate = 0
            if len(splitRates) > 3:
                skipRate = float(splitRates[3])
            else:
                skipRate = 0
            totalRate = trainRate + valRate + testRate + skipRate
            trainRate = trainRate / totalRate
            valRate = valRate / totalRate
            testRate = testRate / totalRate
            skipRate = skipRate / totalRate
            #print(totalRate)
            imgs = [img for img in os.listdir(dataPath) if img.endswith((".png", ".PNG"))]
            for i in range(len(imgs)):
                imgs[i] = dataPath + "/" + imgs[i]
            totalSize = len(imgs)
            trainSplitPoint = math.ceil(totalSize * trainRate)
            valSplitPoint = math.ceil(totalSize * (trainRate + valRate))
            testSplitPoint = math.ceil(totalSize * (trainRate + valRate + testRate))
            if trainSplitPoint > 0:
                train.extend(imgs[0:(trainSplitPoint)])
            if valSplitPoint - trainSplitPoint > 0:
                val.extend(imgs[trainSplitPoint:(valSplitPoint)])
            if testSplitPoint - valSplitPoint > 0:
                test.extend(imgs[valSplitPoint:(testSplitPoint)])
    #print("dataPath = " + dataPath)
    line = file.readline()
file.close()

#20210807 依上次開會時老師的建議，dataset做 shuffle
random.seed(1) #為了再現性，指定seed
random.shuffle(train)
random.shuffle(val)
random.shuffle(test)
print(train)

file = open(outputDir + "train.txt", "w")
for tar in train:
    file.write(tar + "\n")
file.close()

file = open(outputDir + "valid.txt", "w")
for tar in val:
    file.write(tar + "\n")
file.close()

file = open(outputDir + "test.txt", "w")
for tar in test:
    file.write(tar + "\n")
file.close()

print('end')
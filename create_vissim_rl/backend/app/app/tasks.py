from django.contrib.auth import get_user_model

from django.utils import timezone as datetime
from datetime import datetime as n_datetime
from datetime import timedelta, timezone
from django.forms.models import model_to_dict
from dateutil import parser
from dateutil.relativedelta import relativedelta
from app import settings

import os
import time

from dotenv import load_dotenv
import copy
import math
import base64
import requests
import logging

import json
import threading
import random
import string

import schedule
import io
import sys
import traceback
import base64
import openpyxl

from urllib.parse import urlparse
from urllib.parse import parse_qsl
from urllib.parse import urlencode
from pathlib import Path
import subprocess

import numpy as np

load_dotenv()

def json_dumps(data):
  try:
    return json.dumps(data, ensure_ascii=False, indent=2)
  except:
    return 'Error in json_dumps'

# 隨機字串，來源 https://shazi.info/python3-%E7%94%A8-random-%E5%92%8C-string-%E5%BB%BA%E7%AB%8B%E9%9A%A8%E6%A9%9F%E5%AD%97%E5%85%83-%E5%AD%B8%E7%BF%92%E7%AD%86%E8%A8%98/
def getRandomStr(str_len=27, mode=0):
  
  value = ''.join(random.choice(string.ascii_letters + string.digits) for x in range(str_len))
  if mode == 0:
    value = value.upper()
  elif mode == 1:
    value = value.lower()
  return value

def getRandomNumber(str_len=3):
  return ''.join(random.choice(string.digits) for x in range(str_len))

def getErrorLine(e):
  # https://dotblogs.com.tw/caubekimo/2018/09/17/145733
  error_class = e.__class__.__name__ #取得錯誤類型
  detail = ''
  if len(e.args) > 0:
    detail = e.args[0] #取得詳細內容
  cl, exc, tb = sys.exc_info() #取得Call Stack
  lastCallStack = traceback.extract_tb(tb)[-1] #取得Call Stack的最後一筆資料
  fileName = lastCallStack[0] #取得發生的檔案名稱
  lineNum = lastCallStack[1] #取得發生的行號
  funcName = lastCallStack[2] #取得發生的函數名稱
  errMsg = "File \"{}\", line {}, in {}: [{}] {}".format(fileName, lineNum, funcName, error_class, detail)
  return errMsg

def toTimeStr(value):
  #輸入 2022-12-21 20:50:45.748271+08
  #輸出 2022-12-21 20:50:45
  if value is None:
    value = ''
  else:
    value = str(value).split('.')[0]
  return value

def toStr(value):
  if value is None:
    value = ''
  else:
    value = str(value) # + "\t"
  return value

def toInt(value):
  try:
    value = int(float(value))
  except Exception as e:
    #print(f'Exception toInt: {e}, {value}')
    value = 0
  return value

def tryToFloat(value):
  try:
    value = float(value)
  except Exception as e:
    print(f'Exception tryToFloat: {e}, {value}')
  return value

def toFloat(value):
  try:
    value = float(value)
  except Exception as e:
    # print(f'Exception toFloat: {e}, {value}')
    value = 0.0    
  return value

def strToTime_ymdhm(value):
  #輸入 2023/05/01 00:13
  #輸出 datetime
  return n_datetime.strptime(value, '%Y/%m/%d %H:%M')
    
def timeToStr_ymdhm(value):
  #輸入 datetime
  #輸出 2023/05/01 00:13
  return n_datetime.strftime(value, '%Y/%m/%d %H:%M')

def timeToStr_ymd(value):
  #輸入 datetime
  #輸出 2023/05/01 00:13
  return n_datetime.strftime(value, '%Y%m%d')

def strToTime_ymdhms(value):
  #輸入 2023-03-07 10:59:04
  #輸出 datetime
  return n_datetime.strptime(value, '%Y-%m-%d %H:%M:%S')

def timeToStr_tw(value):
  #輸入 datetime
  #輸出 1120325
  value = datetime.now()
  y = toInt(value.year) - 1911
  m = toInt(value.month)
  d = toInt(value.day)
  output = '%03d%02d%02d' % (y, m, d)
  return output

def round_v2(num, decimal=0):
  # python3 的 round 其實不是四捨五入，而是4捨6入，5的話一半進位一半不進位
  # https://medium.com/%E7%A8%8B%E5%BC%8F%E4%B9%BE%E8%B2%A8/python-round-%E5%9B%9B%E6%8D%A8%E4%BA%94%E5%85%A5-%E7%9A%84%E5%B0%8F%E5%9D%91-7ef8accad931
  num = np.round(num, decimal)
  num = float(num)
  return num


def every_day_task():
  try:
    print('every_day_task ++')
    
    print('every_day_task --')
  except Exception as e:
    print(f'Error in every_day_task: {e}')
  return

def one_minutes_task():
  try:
    # sheet_to_ragic_task()
    pass
  except Exception as e:
    print(f'Error in one_minutes_task: {e}')
  return

def five_minutes_task():
  try:
    #print('five_minutes_task++')
    print('每5分鐘更新資料一次...')
    #now_time = datetime.now()
    #now_year = str(now_time.year)


    #print('five_minutes_task--')
  except Exception as e:
    print(f'Error in five_minutes_task: {getErrorLine(e)}')
  return

def ten_minutes_task():
  try:
    print('ten_minutes_task++')

    print('ten_minutes_task--')
    pass
  except Exception as e:
    print(f'Error in ten_minutes_task: {e}')
  return

def one_hour_task():
  try:
    print('one_hour_task++')

    print('one_hour_task--')
    pass
  except Exception as e:
    print(f'Error in one_hour_task: {e}')
  return

def one_week_task():
  try:
    print('one_week_task++')

    print('one_week_task--')
    pass
  except Exception as e:
    print(f'Error in one_week_task: {e}')
  return

def run_schedule():
  while True:
    #print('schedule checking...')
    schedule.run_pending()
    time.sleep(60)


def start_schedule():
  try:

    print('start_schedule ++')
    
    print('start_schedule --')
  except Exception as e:
    print(e)
    
def run_init():

  return

def yyyymm_to_range(start, end):
  #202210 ~  202302
  rangeList = []
  for i in range(start, end, 1):
    m = i % 100
    if m == 0 or m > 12:
      continue
    rangeList.append(i)
  rangeList.append(end)
  return rangeList

def seconds_to_time_format(s):
    hours = s // 3600
    s %= 3600
    minutes = s // 60
    s %= 60
    seconds = s // 1
    milliseconds = round((s % 1) * 1000)
    return f"{int(hours):02d}:{int(minutes):02d}:{int(seconds):02d},{int(milliseconds):03d}"


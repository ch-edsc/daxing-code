
from django.http import HttpResponse
from django.forms.models import model_to_dict
from rest_framework.views import APIView

from app import tasks, settings
from app.tasks import toTimeStr, toStr, toInt, toFloat, strToTime_ymdhm, timeToStr_ymdhm, round_v2

import json
import os
import random
import string
import time
from dotenv import load_dotenv

from rest_framework.response import Response
from rest_framework import status


from django.utils.encoding import escape_uri_path
import zipfile
import threading
from django.utils import timezone as datetime
from datetime import datetime as n_datetime
from datetime import timedelta

import requests
import math

import io
import openpyxl
import base64
import xlsxwriter

load_dotenv()



class test(APIView):
  authentication_classes = []
  permission_classes = []
  def get(self, request):
    try:
      headers = request.headers
      password = self.request.query_params.get('password')
      if password == None:
        if 'Authorization' in headers:
          if headers['Authorization'] != f'Bearer {settings.ACCESS}':
            
            return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'Authorization 驗證未通過'})
        else:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': '請提供 Authorization 以驗證權限'})
      else:
        if password != settings.ACCESS:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'password 驗證未通過'})
      return_data = {}
      return Response(status=status.HTTP_200_OK, data={ 'status': 'OK', 'data': return_data})
    except Exception as e:
      print(tasks.getErrorLine(e))
      return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': tasks.getErrorLine(e)})


class download_train(APIView):
  authentication_classes = []
  permission_classes = []
  def post(self, request):
    try:
      headers = request.headers
      password = self.request.query_params.get('password')
      if password == None:
        if 'Authorization' in headers:
          if headers['Authorization'] != f'Bearer {settings.ACCESS}':
            
            return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'Authorization 驗證未通過'})
        else:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': '請提供 Authorization 以驗證權限'})
      else:
        if password != settings.ACCESS:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'password 驗證未通過'})
      
      
      body_unicode = request.body.decode('utf-8')
      data = json.loads(body_unicode)

      json_str = ''
      if data:
        fdata = data.split(',')[-1]
        binary_data = base64.b64decode(fdata)
        json_file = io.BytesIO(binary_data)
        json_data = json.load(json_file)
        json_str = json.dumps(json_data, ensure_ascii=False, indent=2)

      file_string = ''
      with open('template/train.py', 'r', encoding='utf-8') as file:
        file_string = file.read()
        if len(json_str) > 0:
          json_str = json_str.replace("false", "False").replace("true", "True")
          json_str = 'json_setting = ' + json_str        
          file_string = file_string.replace('json_setting = {}', json_str)

      response = HttpResponse(file_string, content_type='application/octet-stream')
      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path('train.py')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'

      return response
    except Exception as e:
      print(tasks.getErrorLine(e))
      return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': tasks.getErrorLine(e)})

class download_test(APIView):
  authentication_classes = []
  permission_classes = []
  def post(self, request):
    try:
      headers = request.headers
      password = self.request.query_params.get('password')
      if password == None:
        if 'Authorization' in headers:
          if headers['Authorization'] != f'Bearer {settings.ACCESS}':
            
            return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'Authorization 驗證未通過'})
        else:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': '請提供 Authorization 以驗證權限'})
      else:
        if password != settings.ACCESS:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'password 驗證未通過'})
      
      
      body_unicode = request.body.decode('utf-8')
      data = json.loads(body_unicode)

      json_str = ''
      if data:
        fdata = data.split(',')[-1]
        binary_data = base64.b64decode(fdata)
        json_file = io.BytesIO(binary_data)
        json_data = json.load(json_file)
        json_str = json.dumps(json_data, ensure_ascii=False, indent=2)

      file_string = ''
      with open('template/test.py', 'r', encoding='utf-8') as file:
        file_string = file.read()
        if len(json_str) > 0:
          json_str = json_str.replace("false", "False").replace("true", "True")
          json_str = 'json_setting = ' + json_str        
          file_string = file_string.replace('json_setting = {}', json_str)

      response = HttpResponse(file_string, content_type='application/octet-stream')
      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path('test.py')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'

      return response
    except Exception as e:
      print(tasks.getErrorLine(e))
      return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': tasks.getErrorLine(e)})



class download_setting(APIView):
  authentication_classes = []
  permission_classes = []
  def post(self, request):
    try:
      headers = request.headers
      password = self.request.query_params.get('password')
      if password == None:
        if 'Authorization' in headers:
          if headers['Authorization'] != f'Bearer {settings.ACCESS}':
            
            return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'Authorization 驗證未通過'})
        else:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': '請提供 Authorization 以驗證權限'})
      else:
        if password != settings.ACCESS:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'password 驗證未通過'})
      
      file_string = ''
      with open('template/vissim_setting.json', 'r', encoding='utf-8') as file:
        file_string = file.read()
      response = HttpResponse(file_string, content_type='application/octet-stream')
      response['Content-Disposition'] = 'attachment;filename="%s"' % escape_uri_path('vissim_setting.json')          
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'

      return response
    except Exception as e:
      print(tasks.getErrorLine(e))
      return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': tasks.getErrorLine(e)})

class download_setting_explain(APIView):
  authentication_classes = []
  permission_classes = []
  def post(self, request):
    try:
      headers = request.headers
      password = self.request.query_params.get('password')
      if password == None:
        if 'Authorization' in headers:
          if headers['Authorization'] != f'Bearer {settings.ACCESS}':
            
            return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'Authorization 驗證未通過'})
        else:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': '請提供 Authorization 以驗證權限'})
      else:
        if password != settings.ACCESS:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'password 驗證未通過'})
      
      file_data = None
      with open('template/VISSIM_RL_explain.pdf', 'rb') as file:
        file_data = file.read()
          
      response = HttpResponse(file_data, content_type='application/pdf')
      response['Content-Disposition'] = 'attachment; filename="%s"' % escape_uri_path('VISSIM_RL_explain.pdf')
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'

      return response
    except Exception as e:
      print(tasks.getErrorLine(e))
      return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': tasks.getErrorLine(e)})

class download_requirements(APIView):
  authentication_classes = []
  permission_classes = []
  def post(self, request):
    try:
      headers = request.headers
      password = self.request.query_params.get('password')
      if password == None:
        if 'Authorization' in headers:
          if headers['Authorization'] != f'Bearer {settings.ACCESS}':
            
            return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'Authorization 驗證未通過'})
        else:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': '請提供 Authorization 以驗證權限'})
      else:
        if password != settings.ACCESS:
          return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': 'password 驗證未通過'})
      
      file_data = None
      with open('template/requirements.txt', 'rb') as file:
        file_data = file.read()
          
      response = HttpResponse(file_data, content_type='application/octet-stream')
      response['Content-Disposition'] = 'attachment; filename="%s"' % escape_uri_path('requirements.txt')
      response['Access-Control-Expose-Headers'] = 'Content-Disposition'

      return response
    except Exception as e:
      print(tasks.getErrorLine(e))
      return Response(status=status.HTTP_400_BAD_REQUEST, data={ 'status': 'ERROR', 'message': tasks.getErrorLine(e)})
      
      
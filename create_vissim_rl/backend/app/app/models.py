from django.db import models
from django.utils import timezone as datetime

class TempFile(models.Model):
  id = models.CharField(max_length=255, primary_key=True)
  created_at = models.DateTimeField(default=datetime.now)
  target_language = models.CharField(default='EN-US', max_length=255, null=True, blank=True)
  status = models.CharField(default='RUNNING', max_length=255, null=True, blank=True)
  temp_in_paths = models.JSONField(default=list, null=True, blank=True)
  temp_out_paths = models.JSONField(default=list, null=True, blank=True)
  file_names = models.JSONField(default=list, null=True, blank=True)
  message = models.TextField(default='', null=True, blank=True)


from django.apps import AppConfig
import os
class MyAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'

    def ready(self):
        worker_id = int(os.environ.get("GUNICORN_WORKER_ID", 0))
        print('worker_id = ')
        print(worker_id)
        print(os.getpid())
        print('MyAppConfig...')

"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
'''
from django.contrib import admin
from django.urls import path
from app import views

from app.tasks import start_schedule, run_init
'''
from django.contrib import admin
from django.urls import path, include
# from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from app import views
from app.tasks import start_schedule, run_init
# from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

router = DefaultRouter()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/test', views.test.as_view(), name='test'),
    path('api/v1/download_test', views.download_test.as_view(), name='download_test'),
    path('api/v1/download_train', views.download_train.as_view(), name='download_train'),
    path('api/v1/download_setting', views.download_setting.as_view(), name='download_setting'),
    path('api/v1/download_setting_explain', views.download_setting_explain.as_view(), name='download_setting_explain'),
    path('api/v1/download_requirements', views.download_requirements.as_view(), name='download_requirements'),
]

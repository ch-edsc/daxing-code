#有時候VISSIM會開不起來：module win32com.gen_py has no attribute 'CLSIDToPackageMap'
#要把 C:\Users\使用者名稱\AppData\Local\Temp\gen_py\ 資料夾砍掉再重跑
#例如 C:\Users\leo_1\AppData\Local\Temp\gen_py

#現在我電腦有安裝兩個VISSIM版本2021與2022，要指定版本為2022，
#win32com.client.gencache.EnsureDispatch("Vissim.Vissim")
#要改
#win32com.client.gencache.EnsureDispatch("Vissim.Vissim.22")

import numpy as np
import os
import win32com.client 

from collections import namedtuple
from collections import deque
import random
from typing import Union
import numpy as np
import math
import cv2 as cv

import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import transforms as T
import torch.optim as optim
from torch.distributions import Categorical
import torch.multiprocessing as mp

import pickle
import matplotlib.pyplot as plt
import json


print(torch.__version__)
print(torch.cuda.is_available())
print(torch.version.cuda)

def debugprint(*inputs):
    #print(*inputs)
    return

# 20250204 加開到支援8個路口(理論上 GPU、memory 夠力的話多少路口都支援。當然，路口越多，就會吃越多系統資源)，路口數認設定檔 sc_ids 長度，但其他參數也要對應設置(sc_names、sigActions、sigActionsColdTimes、vissim_in_lane_name、vissim_out_lane_name)
# 以及現在 num_intersections 改為依 sc_ids 長度自動變化，設定檔不吃了

# VissimCom不吃相對路徑，要用絕對路徑
#inpxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test/data/綠線模型.inpx"
#layxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test/data/綠線模型.layx"

#inpxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test/data/不同交維模型/05_(0621- )(7a&8)/綠線模型(7a&8).inpx"
#layxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test/data/不同交維模型/05_(0621- )(7a&8)/綠線模型(7a&8).layx"

#inpxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test/data/不同交維模型/04_(0604-0621)(6a-1&8)/綠線模型(6a-1&8).inpx"
#layxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test/data/不同交維模型/04_(0604-0621)(6a-1&8)/綠線模型(6a-1&8).layx"

#inpxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test_v3/data/不同交維階段模型V2/03_(0423-0604)(6a&8)/綠線模型(6a&8).inpx"
#layxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test_v3/data/不同交維階段模型V2/03_(0423-0604)(6a&8)/綠線模型(6a&8).layx"

#inpxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test_new/data/1111109_各交維階段模型(已輸入交通量)/03_(0423-0604)(6a&8)/01_交通量_0514_0700-0800/綠線模型(6a&8)_0514_0700.inpx"
#layxFilePath = "C:/Users/leo_1/Documents/GitHub/VISSIM_test_new/data/1111109_各交維階段模型(已輸入交通量)/03_(0423-0604)(6a&8)/01_交通量_0514_0700-0800/綠線模型(6a&8)_0514_0700.layx"


#超參數

#20250206 依昨天收到的指示， setting 檔案簡化， RL 相關的參數都拿掉

learning_rate = 3e-5#3e-5 #1e-4 #5e-4 # 1e-3 2e-3
batch_size = 32
num_episodes_per_batch = 1
n_step = 10
verbose = 1
gamma = 0.8
memory_max_size = 20000  #1000
#use_cuda = True
use_cuda = torch.cuda.is_available()
#num_episodes = 100 #5
test_epochs = 10
train_stepGap = 10
eval_stepGap = 10

num_intersections = 4
num_neighbors = 3
num_features = 9
num_actions = 16
use_sil = 0

main_path = "C:\\Users\\leo_1\\Documents\\GitHub\\VISSIM_test_2023_v8\\data\\"
main_path += "1120210-V8\\01-績效指標(路口、路段分開)\\05_(0621-0715 )(7a&8)\\01_交通量_0712_0700-0800\\"
main_path += "綠線模型(7a&8)_0712_0700"
inpxFilePath = main_path + ".inpx"
layxFilePath = main_path + ".layx"



#RL通常要跑個幾百幾千輪才會比較有效果
#要截影片時只跑5輪



#然後quickMode設1時跑起來才快，但截影片先設0
#quickMode = 0
quickMode = 1

#現在是用 Colight 的網路架構



#vissim_one_round_simulationTime = 300
vissim_one_round_simulationTime = 3900
#vissim_one_round_simulationTime = 30

YELLOW_TIME = 30
RED_TIME = 30




NO_ENV = False

#綠線模型的路網，依路口由上到下，整理進入路段與離開路段

#路口由上往下依序名稱
sc_names = ["G11-2", "G11-1", "G10.5", "G10"]
sc_ids = [3, 2, 4, 1]

#sc_ids = [8, 7, 6, 5] #這設定檔燈號的id是 8 7 6 5
sigs = ["RED", "GREEN", "AMBER"] #燈號
RED_ID = 0
GREEN_ID = 1
AMBER_ID = 2

sigActions = [
    [(1, 0, 0), (0, 1, 0), (0, 0, 1)], #這是G11-2路口的燈號組合
    [(1, 0, 0, 0), (1, 1, 0, 0), (0, 0, 1, 0), (0, 0, 0, 1)], #這是G11-1路口的燈號組合
    [(1, 0), (0, 1)], #這是G10.5路口的燈號組合
    [(1, 0), (0, 1)] #這是G10路口的燈號組合
]
#增加燈號冷卻時間下限設定，單位0.1秒，長度為觀察燈號時長設置為他的50%
'''
sigActionsColdTimes = [
    [450, 100, 275],
    [75, 410, 70, 250],
    [600, 225],
    [400, 450]
]
'''

sigActionsColdTimes = [
    [200, 15, 100],
    [10, 600, 30, 100],
    [200, 100],
    [200, 100]
]


#這是4個路口的進入車道
vissim_in_lane_name = [
    [
        #這是最上方路口
        set(('57-1', '57-2', '54-1', '54-2', '10073-1', '10010-1', '53-1', '53-2')), #右邊進入車道
        set(('52-1', '52-2', '52-3', '10072-1', '10074-1', '10074-2', '10076-1', '51-1', '51-2', '51-3')), #下方進入車道
        set(('60-1', '60-2', '10082-1', '10082-2', '59-1', '59-2')), #左邊進入車道
        set(('56-1', '56-2', '56-3', '10105-1', '10109-1', '10109-2', '10084-1', '63-1', '63-2', '63-3')) #上方進入車道
    ],
    [
        #這是由上往下數第2個路口
        set(('10053-1', '10054-1', '38-1', '38-2', '37-1', '37-2')),
        set(('2-1', '2-2', '2-3', '10001-1', '10002-1', '10048-1', '1-1', '1-2')),
        set(('41-1',)),
        set(('43-1', '43-2', '10064-1', '10065-1', '42-1', '42-2'))
    ],
    [
        #這是由上往下數第3個路口
        set(('75-1',)),
        set(('28-1', '29-1', '30-1', '10040-1', '10041-1', '10042-1', '86-1', '86-2', '86-3')),
        set(('77-1',)),
        set(('84-1','85-1', '10113-1', '10114-1', '87-1', '87-2'))
    ],
    [
        #這是由上往下數第4個路口
        set(('25-1', '25-2', '10006-1', '10006-2', '10015-1', '24-1', '24-2', '24-3')),
        set(('6-1', '6-2', '7-1', '10005-1', '5-1', '5-2')),
        set(('21-1', '21-2', '10023-1', '10024-1', '32-1', '10077-1', '10078-1', '10079-1', '20-1', '20-2', '20-3')),
        set(('16-1', '16-2', '10012-1', '10012-2', '15-1', '15-2'))
    ]
]
#這是4個路口的離開車道
vissim_out_lane_name = [
    [
        #這是最上方路口
        set(('46-1',)), #右邊離開車道
        set(('50-1', '50-2')), #下方離開車道
        set(('45-1',)), #左邊離開車道
        set(('48-1', '48-2')) #上方離開車道
    ],
    [
        #這是由上往下數第2個路口
        set(('4-1',)),
        set(('10055-1', '10055-2', '40-1', '40-2')),
        set(('35-1', '35-2')),
        set(('3-1', '3-2', '3-3', '10138-1', '10138-2', '10138-3'))
    ],
    [
        #這是由上往下數第3個路口
        set(('76-1',)),
        set(('83-1', '83-2', '10137-1', '10137-2')),
        set(('78-1',)),
        set(('82-1', '82-2', '10136-1', '10136-2'))
    ],
    [
        #這是由上往下數第4個路口
        set(('11-1', '11-2', '10007-1', '10007-2', '12-1', '12-2', '10008-1', '10008-2', '10009-1')),
        set(('18-1', '18-2', '8-1', '10019-1', '10019-2', '10020-1', '10020-2', '33-1', '34-1')),
        set(('9-1', '9-2', '9-3', '10011-1', '10011-2', '10011-3', '17-1', '17-2', '17-3')),
        set(('10-1', '10-2','27-1', '27-2', '10103-1', '10103-2', '10013-1', '10013-2', '10014-1'))
    ]
]

json_setting = {}

#20250206 依昨天收到的指示， setting 檔案簡化， RL 相關的參數都拿掉
if 'inpxFilePath' in json_setting:
    inpxFilePath = json_setting['inpxFilePath']
if 'layxFilePath' in json_setting:
    layxFilePath = json_setting['layxFilePath']
#if 'n_step' in json_setting:
#    n_step = json_setting['n_step']
#if 'learning_rate' in json_setting:
#    learning_rate = json_setting['learning_rate']
#if 'batch_size' in json_setting:
#    batch_size = json_setting['batch_size']

#if 'gamma' in json_setting:
#    gamma = json_setting['gamma']
#if 'memory_max_size' in json_setting:
#    memory_max_size = json_setting['memory_max_size']

#if 'use_cuda' in json_setting:
#    use_cuda = json_setting['use_cuda']

#if 'num_episodes' in json_setting:
#    num_episodes = json_setting['num_episodes']

if 'quickMode' in json_setting:
    quickMode = json_setting['quickMode']

#if 'num_intersections' in json_setting:
#    num_intersections = json_setting['num_intersections']
#if 'num_neighbors' in json_setting:
#    num_neighbors = json_setting['num_neighbors']
#if 'num_features' in json_setting:
#    num_features = json_setting['num_features']
#if 'num_actions' in json_setting:
#    num_actions = json_setting['num_actions']
# 20250204 加開到支援8個路口，num_intersections與num_actions改為從依照設定檔的sc_ids長度切換

if 'vissim_one_round_simulationTime' in json_setting:
    vissim_one_round_simulationTime = json_setting['vissim_one_round_simulationTime']

if 'YELLOW_TIME' in json_setting:
    YELLOW_TIME = json_setting['YELLOW_TIME']

if 'RED_TIME' in json_setting:
    RED_TIME = json_setting['RED_TIME']

#if 'train_stepGap' in json_setting:
#    train_stepGap = json_setting['train_stepGap']

#if 'eval_stepGap' in json_setting:
#    eval_stepGap = json_setting['eval_stepGap']

#if 'NO_ENV' in json_setting:
#    NO_ENV = json_setting['NO_ENV']
#路口由上往下依序名稱 ["G11-2", "G11-1", "G10.5", "G10"]
if 'sc_names' in json_setting:
    sc_names = json_setting['sc_names']
#路口由上往下依序id [3, 2, 4, 1]
if 'sc_ids' in json_setting:
    sc_ids = json_setting['sc_ids']
#燈號 ["RED", "GREEN", "AMBER"]
if 'sigs' in json_setting:
    sigs = json_setting['sigs']


#if 'RED_ID' in json_setting:
#    RED_ID = json_setting['RED_ID']
#if 'GREEN_ID' in json_setting:
#    GREEN_ID = json_setting['GREEN_ID']
#if 'AMBER_ID' in json_setting:
#    AMBER_ID = json_setting['AMBER_ID']

if 'sigActions' in json_setting:
    sigActions = json_setting['sigActions']
if 'sigActionsColdTimes' in json_setting:
    sigActionsColdTimes = json_setting['sigActionsColdTimes']
if 'vissim_in_lane_name' in json_setting:
    vissim_in_lane_name = json_setting['vissim_in_lane_name']
if 'vissim_out_lane_name' in json_setting:
    vissim_out_lane_name = json_setting['vissim_out_lane_name']


# 20250204 加開到支援8個路口，num_intersections與num_actions改為從依照設定檔的sc_ids長度切換
num_intersections = len(sc_ids)
num_actions = 2 ** len(sc_ids)
class Intersection:
    def __init__(self, inLaneNames, outLaneNames):
        # inLaneName/outLaneName 結構為
        #[(右車道1,右車道2...), (下車道1,下車道2...), (左車道1,左車道2...), (上車道1,上車道2...)]
        self.inLaneNames = inLaneNames
        self.outLaneNames = outLaneNames
        return


class VissimEnv:
    def __init__(self, inpxFilePath, layxFilePath, intersectionList,
                 quickMode=0, simulationTime=600, useMaxSimSpeed=True, useAllCores=True,
                stepGap=10):
        #stepGap:run一次step隔幾輪模擬
        self.inpxFilePath = inpxFilePath
        self.layxFilePath = layxFilePath
        self.quickMode = quickMode
        self.simulationTime = simulationTime
        self.useMaxSimSpeed = useMaxSimSpeed
        self.useAllCores = useAllCores
        
        self.intersectionList = intersectionList
        
        self.isVissimOpening = False
        # self.sigStates = [0, 0, 0, 0] #四個路口的目前燈號組id
        # self.sigFreezeTime = [0, 0, 0, 0] #切燈後凍結時間，也就是禁止連切
        self.sigStates = []
        self.sigFreezeTime = []
        for i in range(len(sc_ids)):
            self.sigStates.append(0) # 現在支援8個路口，路口數認傳入參數的 sc_ids 長度
            self.sigFreezeTime.append(0)
        #self.sigTimes = [0, 0, 0, 0] #切燈號倒數計時
        
        
        self.changerList = []
        self.targetIntersections = []
        self.sgs_all = []
        
        
        self.stepGap = stepGap
        
        self.envRunTimes = 0
        
        self.preState = []
        self.preReward = []
        #self.observation_space
        #input_dim=env.observation_space.shape[0],
        #num_actions=env.action_space.n,
        
        #self.LastAvgTravelTimes = []
        self.travelAvgSpeeds = []
        self.vTime = 0
        return
        
    
    #全切綠燈
    def allGreen(self):
        SCs = self.vissim.Net.SignalControllers.GetAll()
        for sc in SCs:
            sgs = sc.SGs.GetAll()
            for sg in sgs:
                sg.SetAttValue("SigState", sigs[GREEN_ID])
        self.vissim.Simulation.RunSingleStep()
        return
    
    #全切紅燈
    #測試時發現，模擬剛啟動後應該要全切紅燈，不然還沒切換燈號時相當於全車到綠燈，會撞車
    def allRed(self):
        SCs = self.vissim.Net.SignalControllers.GetAll()
        for sc in SCs:
            sgs = sc.SGs.GetAll()
            for sg in sgs:
                sg.SetAttValue("SigState", sigs[RED_ID])
        self.vissim.Simulation.RunSingleStep()
        return
        
    
    def sigChanger(self, intersectionIndex, yellowTime = 30, redTime = 10):
        #print("intersectionIndex = ")
        targetIntersection = self.targetIntersections[intersectionIndex]
        sgs = self.sgs_all[intersectionIndex]

        #抓前一輪燈號組
        #print(intersectionIndex)
        preSigIndex = self.sigStates[intersectionIndex] - 1

        if preSigIndex < 0:
            preSigIndex += len(sigActions[intersectionIndex])
        preSig = sigActions[intersectionIndex][preSigIndex]
        nowSig = sigActions[intersectionIndex][self.sigStates[intersectionIndex]]
        #preSig = (1, 0, 0, 0) nowSig = (0, 1, 0, 0)

        for i in range(len(sigActions[intersectionIndex][self.sigStates[intersectionIndex]])):
            if (preSig[i] == GREEN_ID) and (nowSig[i] == RED_ID):
                #綠變紅，要黃燈
                #直接切黃，然後延遲器3秒後切紅
                sgs[i].SetAttValue("SigState", sigs[AMBER_ID])
                #[哪個路口, 第幾組燈號, 延遲幾step, 切什麼燈]
                self.changerList.append([intersectionIndex, i, RED_ID, yellowTime])
            elif (preSig[i] == RED_ID) and (nowSig[i] == GREEN_ID):
                #紅變綠，要延遲1秒
                self.changerList.append([intersectionIndex, i, GREEN_ID, yellowTime + redTime])
                #print("intersectionIndex_%d_sg_%d need change to %s delay %d times " %
                #      (intersectionIndex, i, sigs[GREEN_ID],  redTime) )
            elif (preSig[i] != nowSig[i]): #其他狀況直接切
                sgs[i].SetAttValue("SigState", sigs[nowSig[i]])
        return

    def sigChangeNow(self, iId, sgId, colorId):
        self.sgs_all[sgId].SetAttValue("SigState", sigs[colorId])
        return

    def checkDelayChanger(self):
        if len(self.changerList) == 0:
            return
        for i in range(len(self.changerList)-1, -1, -1): #裡面會做delete所以要倒著抓
            #[哪個路口, 第幾組燈號, 切什麼燈, 延遲幾step]
            self.changerList[i][3] -= 1
            if self.changerList[i][3] <= 0: #如果倒數計時的時間到了，就切換燈號
                index = self.changerList[i][0]
                sg_id = self.changerList[i][1]
                sig_id = self.changerList[i][2]
                #print(f"index={index}, sg_id={sg_id}, sig_id={sig_id}, sigs[sig_id]={sigs[sig_id]}")
                self.sgs_all[index][sg_id].SetAttValue("SigState", sigs[sig_id])
                del self.changerList[i]
        return


    def runVissimForOneStep(self, actions = []):
        done = False
        for i in range(self.stepGap):
            self.vissim.Simulation.RunSingleStep() 
            
            self.vTime = round((self.vissim.Simulation.SimulationSecond) * 10)
            
            if self.vTime != 0:
                self.checkDelayChanger() #延遲變燈號的計時器
                for i in range(len(self.sigFreezeTime)): #燈號凍結計時器
                    self.sigFreezeTime[i] -= 1
            else:
                done = True
                self.envRunTimes += 1
                return done, self.vTime

        while sum(self.sigFreezeTime) > 0:
            self.vissim.Simulation.RunSingleStep() 
            
            self.vTime = round((self.vissim.Simulation.SimulationSecond) * 10)
            
            if self.vTime != 0:
                self.checkDelayChanger() #延遲變燈號的計時器
                for i in range(len(self.sigFreezeTime)): #燈號凍結計時器
                    self.sigFreezeTime[i] -= 1
            else:
                done = True
                self.envRunTimes += 1
                return done, self.vTime
        
        if self.vTime == 1: #如果是第一輪，填入紅綠燈
            self.allRed()
            for i in range(len(sc_ids)):
                sgs = self.sgs_all[i]
                #sigTimes[i] = sigActionTimes[i][sigStates[i]]
                for j in range(len(sgs)):
                    sgs[j].SetAttValue("SigState", sigs[sigActions[i][self.sigStates[i]][j]])    

        
        #看action換燈號
        #self.actions = [0, 0, 0, 0]
        for i in range(len(actions)):
            if self.sigFreezeTime[i] > 0:
                continue #禁止連切同一個燈
            if actions[i] != 0:
                self.sigStates[i] = (self.sigStates[i] + 1) % len(sigActions[i])
                self.sigChanger(i, yellowTime=YELLOW_TIME, redTime=RED_TIME) #目標路口切燈號
                #self.sigFreezeTime[i] = 50 + RED_TIME #切燈後冷卻5秒+紅燈延遲時間
                #i 為第幾個路口, self.sigStates[i] 為 該路口在使用第幾組燈號
                self.sigFreezeTime[i] = RED_TIME + sigActionsColdTimes[i][self.sigStates[i]]
        
        
        if self.vTime != 0:
            self.checkDelayChanger() #延遲變燈號的計時器
            for i in range(len(self.sigFreezeTime)): #燈號凍結計時器
                self.sigFreezeTime[i] -= 1
        else:
            done = True
            self.envRunTimes += 1
            return done, self.vTime
        return done, self.vTime
    
    def sqeezeList(self, data):
        
        oneMoreTime = False
        data2 = []
        if type(data) == type([]):
            for tar in data:
                if type(tar) == type([]):
                    for subTar in tar:
                        data2.append(subTar)
                    oneMoreTime = True
                else:
                    data2.append(tar)
        else:
            data2.append[data]  
        if oneMoreTime:
            data2 = self.sqeezeList(data2)
        return data2
        
    
    def getState(self):
        # colight 的 state 為 燈號+路口鄰接矩陣+各車道車輛數
        '''
        鄰接矩陣為
        路口 1 2 3 4
         1 [[1,1,0,0],
         2  [1,1,1,0],
         3  [0,1,1,1],
         4  [0,0,1,1]]
        每個路口都要餵n*n的矩陣
        這架構路網一大了就很浪費記憶體
        
        改用 Thousand Lights 架構：每個路口只要知道鄰近路口的id就好
        所以分別給鄰近路口(右、下、左、上)的id
               右下左上
        路口1: [0,2,0,0]
        路口2: [0,3,0,1]
        路口3: [0,4,0,2]
        路口4: [0,0,0,3]
        這樣每個路口固定餵4個值就很省空間
        
        '''
        state = []
        #燈號
        #state.append(self.sigStates)
        #替代鄰接矩陣，鄰近路口id值 ...取消
        #state.append([[-1,1,-1,-1],[-1,2,-1,0],[-1,3,-1,1],[-1,-1,-1,2]])
        #燈號改參照colight模式，黃燈-1
        sigStatesWithFreeze = []
        for i, freeze in enumerate(self.sigFreezeTime):
            if freeze > 0:
                sigStatesWithFreeze.append(-1)
            else:
                sigStatesWithFreeze.append(self.sigStates[i])
        state.append(sigStatesWithFreeze)
        
        #各車道車輛數
        vehs = self.vissim.Net.Vehicles.GetAll() #從vissim把車輛都抓出來
        vehInLaneList = []
        for veh in vehs:
            vehInLaneList.append({"lane": veh.AttValue("Lane"), "type": veh.AttValue("VehType")})
            #print(veh.AttValue("VehType"))
        
        
        inCars = []
        outCars = []
        
        
        for i in range(len(self.intersectionList)): #一個路網有n個路口(預設4)
            inCars.append([])
            outCars.append([])
            for j in range(len(self.intersectionList[i].inLaneNames)): #一個路口有4個方向
                inCars[i].append([])
                outCars[i].append([])
                
                inCars[i][j] = 0
                outCars[i][j] = 0
                for obj in vehInLaneList: #對於每輛車
                    lane = obj['lane']
                    if lane in self.intersectionList[i].inLaneNames[j]: #若車在目標車道
                        veh_type = obj['type']
                        equivalentRate = 1 #車當量倍率
                        if veh_type == '700': # 實際跑起來 700: scooter 抓到的值只有 700 沒有後半段
                            equivalentRate = 0.6
                        elif veh_type == '200':
                            equivalentRate = 2
                        inCars[i][j] += equivalentRate #對應的 inCars 統計數字加上車當量
                for obj in vehInLaneList: #對於每輛車
                    lane = obj['lane']
                    if lane in self.intersectionList[i].outLaneNames[j]: #若車在離開車道
                        veh_type = obj['type']
                        equivalentRate = 1 #車當量倍率
                        if veh_type == '700':
                            equivalentRate = 0.6
                        elif veh_type == '200':
                            equivalentRate = 2
                        outCars[i][j] += equivalentRate #對應的 outCars 統計數字加上車當量
        state.append(inCars)
        state.append(outCars)
        
        #print(state)
        '''
        [
         [0, 0, 0, 0], #目前燈號組
         [[-1, 1, -1, -1], [-1, 2, -1, 0], [-1, 3, -1, 1], [-1, -1, -1, 2]], #鄰近路口id值
         [[0, 0, 0.6, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0.6, 0]], #各路口進入車
         [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]] #各路口離開車
        ]
        
        '''
        #先簡易版做1d的測試
        #if True:
        #    state = [self.sqeezeList(state)]
        
        #state對照coLight，改路口為單位
        #shape為(路口, frature)
        state_new = []
        for i in range(len(self.intersectionList)): #一個路網有n個路口(預設4)
            state_new.append([])
        for i, sig in enumerate(state[0]):#燈號
            state_new[i].append(state[0][i])
        for i, cars in enumerate(state[1]):#進入車
            state_new[i].extend(state[1][i])
        for i, cars in enumerate(state[2]):#離開車
            state_new[i].extend(state[2][i])
        #然後是adj，鄰接矩陣
        #shape為(路口, 鄰居數, 路口)
        #綠線模型是1*4直線道路，鄰居為3(要包含自己)

        #up_neighbor_id = [-1, 0, 1, 2]#4路口的上方路口id
        #self_neighbor_id = [0, 1, 2, 3]
        #down_neighbor_id = [1, 2, 3, -1]#4路口的上方路口id
        up_neighbor_id = []
        self_neighbor_id = []
        down_neighbor_id = []
        # 現在支援8個路口，路口數認傳入參數的 sc_ids 長度
        for i in range(len(sc_ids)):
            up_neighbor_id.append(i-1)
            self_neighbor_id.append(i)
            if i < len(sc_ids) - 1:
                down_neighbor_id.append(i+1)
            else:
                down_neighbor_id.append(-1)

        
        #one hot
        one_hot_arrs = {}
        #ids = len(self.intersectionList) + 1 #路口數+1，加上沒有鄰居的狀態
        ids = len(self.intersectionList)#不能+1，沒鄰居的one_hot是全都0
        one_hot_arrs["-1"] = [0]*ids
        for i in range(ids):
            #one_hot_arrs[str(i-1)] = [] #編號從-1開始
            one_hot_arrs[str(i)] = [] #編號從-1開始
            for j in range(ids):
                if i==j:
                    #one_hot_arrs[str(i-1)].append(1)
                    one_hot_arrs[str(i)].append(1)
                else:
                    #one_hot_arrs[str(i-1)].append(0)
                    one_hot_arrs[str(i)].append(0)

        adj = []
        for i in range(len(self.intersectionList)):
            adj.append([])
            adj[i].append(one_hot_arrs[str(up_neighbor_id[i])])
            adj[i].append(one_hot_arrs[str(self_neighbor_id[i])])
            adj[i].append(one_hot_arrs[str(down_neighbor_id[i])])
        #然後新的state與adj都要回傳
        return_state = []
        return_state.append(state_new)
        return_state.append(adj)
        #這data傳過去轉tensor時error了...改成還是1維進去，到神經網路再reshape
        #return_state = [self.sqeezeList(return_state)]
        return return_state, inCars, outCars
    
    def getReward(self, inCars, outCars):
        reward = []
        #算MP
        #路口的壓力(Pressure of an intersection)為所有進入車道的車輛總和與所有出口車道的車輛總和差的絕對值 
        # 20220920更改壓力，[未通過]比重改為2倍
        '''
        一個 intersectionList 裡面有進入路口/離開路口的道路編號，例如離開路口格式如下
        [set(('76-1',)),
        set(('83-1', '83-2', '10137-1', '10137-2')),
        set(('78-1',)),
        set(('82-1', '82-2', '10136-1', '10136-2'))]
        所以依序取出對應車道的車輛數來計算
        '''
        #之前算state時已經統計 inCars outCars 了，這邊可以直接加總
        
        if False:
            mps = []
            for i in range(len(inCars)):
                totalIn = 0
                for j in range(len(inCars[i])):
                    totalIn += inCars[i][j]
                totalOut = 0
                for j in range(len(outCars[i])):
                    totalOut += outCars[i][j]
                mp = -abs(totalIn - totalOut)
                mps.append(mp)
        
            
        #按照論文的reward設定，是 abs(進入路口車輛數 - 離開路口車輛數)
        #他可能是希望讓各個路口平衡(也就是，讓每個時間點的進入車輛與離開車輛的數量越相同越好)
        #但我們跑模型，會希望"路口的車輛，越快通過路口越好"
        #也就是說，如果離開車輛的數量大於進入車輛，路口的效率會更好
        #修改公式，不取絕對值
        mps = []
        for i in range(len(inCars)):
            totalIn = 0
            for j in range(len(inCars[i])):
                totalIn += inCars[i][j]
            totalOut = 0
            for j in range(len(outCars[i])):
                totalOut += outCars[i][j]
            mp = totalOut - totalIn * 2
            mp += 45
            mps.append(mp)
            
        #取得平均旅行時間，平均旅行時間跑完一輪才會有一個
        self.vTime = round((self.vissim.Simulation.SimulationSecond) * 10)
        if self.vTime == 0:
            #self.LastAvgTravelTimes = []
            self.travelAvgSpeeds = []
            for travelTimeMeasurement in self.vissim.Net.VehicleTravelTimeMeasurements.GetAll():
                #self.LastAvgTravelTimes.append(travelTime.AttValue("TravTm(Current, Avg, All)"))
                
                travelTime = travelTimeMeasurement.AttValue("TravTm(Current, Avg, All)")
                travelDist = travelTimeMeasurement.AttValue("Dist")
                speed = 0.0
                
                if travelTime != None and travelDist != None:
                    speed = (travelDist / 1000) / (travelTime / 3600)
                    print('')
                    print(travelTimeMeasurement.AttValue("Name") + ":")
                    print(f"travelTime = {travelTime} s")
                    print(f"travelDist = {travelDist} m")
                    print(f"travelAvgSpeed = {speed} km/h")
                self.travelAvgSpeeds.append(speed)                
        return mps
    
    
    def reset(self):
        #reset時，把變數初始化開一個環境，回傳起始的state
        
        #變數初始化
        #self.state = []
        
        #self.sigStates = [0, 0, 0, 0] #四個路口的目前燈號組id
        self.changerList = []
        #self.sigFreezeTime = [0, 0, 0, 0]

        self.sigStates = []
        self.sigFreezeTime = []
        for i in range(len(sc_ids)):
            self.sigStates.append(0) # 現在支援8個路口，路口數認傳入參數的 sc_ids 長度
            self.sigFreezeTime.append(0)
        
        #開環境
        
        
        if self.envRunTimes > (1200 // self.simulationTime) + 1:
            #關於跑幾輪需要重開VISSIM，測試起來在我的筆電
            #如果模擬長度300秒的話可以跑8輪，安全起見設5輪重跑
            #然後模擬長度越長就需要越早重開，所以就 (1200 // self.simulationTime) + 1
            #長度100是13輪，長度300是5輪，長度600是2輪
            self.envRunTimes = 0
            self.close()
            self.isVissimOpening = False #每跑10輪模擬，就把VISSIM關掉重開一次，避免OOM
            
        if self.isVissimOpening == False:
            #建立 COM 端口
            self.vissim = win32com.client.gencache.EnsureDispatch("Vissim.Vissim.22")
            #讀交通檔案
            self.vissim.LoadNet(self.inpxFilePath)
            self.vissim.LoadLayout(self.layxFilePath)
            #設參數
            self.vissim.Simulation.SetAttValue('UseMaxSimSpeed', self.useMaxSimSpeed)
            if self.useAllCores:
                self.vissim.Simulation.AttValue('UseAllCores')
            self.vissim.Graphics.CurrentNetworkWindow.SetAttValue("QuickMode", self.quickMode)
            self.vissim.Simulation.SetAttValue('SimPeriod', self.simulationTime)
            self.vissim.Simulation.SetAttValue('RandSeed', random.randint(0, 1000))

            
            self.targetIntersections = []
            self.sgs_all = []
            for i in range(len(sc_ids)):
                self.targetIntersections.append(self.vissim.Net.SignalControllers.ItemByKey(sc_ids[i]))
                self.sgs_all.append(self.targetIntersections[i].SGs.GetAll())
            
            
            self.isVissimOpening = True        
        
        #跑第一輪
        self.vissim.Simulation.Stop()
        self.runVissimForOneStep()
        
        # 回傳 state
        state, inCars, outCars = self.getState()
        return state
    
    
    
    def step(self, actions):
        done, self.vTime = self.runVissimForOneStep(actions)
        state, inCars, outCars = self.getState()
        reward = self.getReward(inCars, outCars)
        if done:#最後一輪模擬結束沒state了所以回傳前一輪的state
            return self.preState, self.preReward, done, self.vTime
        self.preState = state
        self.preReward = reward
        return state, reward, done, self.vTime
    
    
    
    def render(self):
        return
    
    def close(self):
        self.vissim.Exit()
        return
    
    def get_average_travel_speed(self):
        #取得平均旅行時間，平均旅行時間跑完一輪才會有一個，這邊固定回傳最後一輪的
        #return self.LastAvgTravelTimes
        return self.travelAvgSpeeds



#這邊是原作者的 replay_buffer.py



Transition = namedtuple('Transition', 's0 s1 a r done')
Transitions = namedtuple('Transitions', 's0 s1 a r done')  # functionally the same as Transition
NStepTransitions = namedtuple('Batch', 's0 s1 a n_step_sum_of_r n_step_s0 n_step_s1 done_within_n_step')  # each item corresponds to a tensor
GET_FRAMES = 1
class SequentialBuffer:

    def __init__(self, gamma: float, n_step: int):
        #self.memory = deque(maxlen=memory_max_size)
        self.memory = [] #這邊不能deque
        self.gamma = gamma
        self.n_step = n_step

    def push(self, transition: Transition) -> None:
        self.memory.append(transition)
        if len(self.memory) > memory_max_size: #這邊不能deque就手動del
            del self.memory[0]

    def instantiate_NStepTransitions_and_empty_buffer(self) -> NStepTransitions:

        """We empty the buffer because this is an on-policy algorithm."""
        #print("instantiate_NStepTransitions_and_empty_buffer ++ ")

        dummy_transition = Transition(np.zeros((self.memory[0].s0.shape)), np.zeros((self.memory[0].s1.shape)), 0, 0, 0)

        memory_with_dummies = self.memory + [dummy_transition] * (self.n_step - 1)
        #memory_with_dummies = self.memory + deque(dummy_transition) * (self.n_step - 1)

        transitions = Transitions(*zip(*memory_with_dummies))
        length = len(self.memory)  # not computed with memory_with_dummies
        #print(transitions.s)
        #print(transitions.a)
        s0        = torch.tensor(transitions.s0[:-(self.n_step-1) or None],  dtype=torch.float).view(length, num_intersections, num_features)
        s1        = torch.tensor(transitions.s1[:-(self.n_step-1) or None],  dtype=torch.float).view(length, num_intersections, num_neighbors, num_intersections)
        #s        = torch.tensor(transitions.s[:-(self.n_step-1) or None],  dtype=torch.float).view(length, GET_FRAMES, 64, 64)
        a        = torch.tensor(transitions.a[:-(self.n_step-1) or None],  dtype=torch.long ).view(length,  1)
        n_step_s0 = torch.tensor(transitions.s0[self.n_step-1:], dtype=torch.float).view(length, num_intersections, num_features)
        n_step_s1 = torch.tensor(transitions.s1[self.n_step-1:], dtype=torch.float).view(length, num_intersections, num_neighbors, num_intersections)
        #n_step_s = torch.tensor(transitions.s[self.n_step-1:], dtype=torch.float).view(length, GET_FRAMES, 64, 64)
        #print("n_step_s0")
        #print(s0.shape)
        #print(s1.shape)
        #print(a.shape)
        #print(n_step_s0.shape)
        #print(n_step_s1.shape)
        if use_cuda:
            #s = s.cuda()
            s0 = s0.cuda()
            s1 = s1.cuda()
            a = a.cuda()
            #n_step_s = n_step_s.cuda()
            n_step_s0 = n_step_s0.cuda()
            n_step_s1 = n_step_s1.cuda()
        # last few are dummies and their values will be ignored conveniently through done_within_n_step

        n_step_sum_of_r = np.zeros((len(self.memory),))
        done_within_n_step = np.zeros((len(self.memory),))  # including the zeroth and the (n-1)th step

        for t in range(len(transitions.r[:-(self.n_step-1) or None])):  # t = 0, 1, ..., T-1, (T, ..., T+n_step-1), where bracket terms due to dummies
            sum = 0
            for i in range(self.n_step):  # from 0 to n_step - 1 inclusive; exactly what we want
                sum += (self.gamma ** i) * transitions.r[t+i]
                if transitions.done[t+i]:
                    done_within_n_step[t] = 1  # indicates that we shouldn't care about the value of n_step_s
                    break
            n_step_sum_of_r[t] = sum

        n_step_sum_of_r = torch.tensor(n_step_sum_of_r, dtype=torch.float).view(length, 1)
        done_within_n_step = torch.tensor(done_within_n_step, dtype=torch.long).view(length, 1)
        if use_cuda:
            n_step_sum_of_r = n_step_sum_of_r.cuda()
            done_within_n_step = done_within_n_step.cuda()

        #self.memory = deque(maxlen=memory_max_size)
        self.memory = []
        #print("instantiate_NStepTransitions_and_empty_buffer -- ")
        return NStepTransitions(s0, s1, a, n_step_sum_of_r, n_step_s0, n_step_s1, done_within_n_step)
        #return NStepTransitions(s, a, n_step_sum_of_r, n_step_s, done_within_n_step)

# for SIL
# no done is needed here because we only need to calculate the monte-carlo return for one episode at a time
TransitionWithoutDone = namedtuple('TransitionWithoutDone', 's0 s1 a r')
TransitionWithoutDoneWithReturn = namedtuple('TransitionWithoutDoneWithReturn', 's0 s1 a R')
MonteCarloTransitions = namedtuple('MonteCarloTransitions', 's0 s1 a R')

class SILBuffer:

    def __init__(self, gamma: float):
        #這樣會OOM...改用deque限制長度
        #self.memory = []
        #self.current_episode = []
        self.memory = deque(maxlen=memory_max_size)
        self.current_episode = deque(maxlen=memory_max_size)
        self.gamma = gamma

    def push(self, transition: TransitionWithoutDone) -> None:
        self.current_episode.append(transition)

    def process_and_empty_current_episode(self) -> None:
        discounted_return = 0
        for transition in reversed(self.current_episode):
            discounted_return = transition.r + self.gamma * discounted_return
            self.memory.append(TransitionWithoutDoneWithReturn(transition.s0, transition.s1, transition.a, discounted_return))
        self.current_episode = deque(maxlen=memory_max_size)

    def ready_for(self, batch_size: int) -> bool:
        return len(self.memory) >= batch_size

    def sample(self, batch_size: int) -> MonteCarloTransitions:
        transitions = random.sample(self.memory, batch_size)
        transitions = MonteCarloTransitions(*zip(*transitions))
        #s = torch.tensor(transitions.s, dtype=torch.float).view(batch_size, -1)
        #s = torch.tensor(transitions.s, dtype=torch.float).view(batch_size, GET_FRAMES, 64, 64)
        s0 = torch.tensor(transitions.s0, dtype=torch.float).view(batch_size, num_intersections, num_features)
        s1 = torch.tensor(transitions.s1, dtype=torch.float).view(batch_size, num_intersections, num_neighbors, num_intersections)
        a = torch.tensor(transitions.a, dtype=torch.long ).view(batch_size,  1)
        R = torch.tensor(transitions.R, dtype=torch.float).view(batch_size,  1)
        if use_cuda:
            s0 = s0.cuda()
            s1 = s1.cuda()
            a = a.cuda()
            R = R.cuda()
        return MonteCarloTransitions(s0, s1, a, R)


#這邊是原作者的 params_pool.py

def get_net(
        num_in:int,
        num_out:int,
        final_activation,  
        num_hidden_layers:int=1,
        num_neurons_per_hidden_layer:int=64
    ) -> nn.Sequential:

    layers = []

    layers.extend([
        nn.Linear(num_in, num_neurons_per_hidden_layer),
        nn.ReLU(),
    ])

    for _ in range(num_hidden_layers):
        layers.extend([
            nn.Linear(num_neurons_per_hidden_layer, num_neurons_per_hidden_layer),
            nn.ReLU(),
        ])

    layers.append(nn.Linear(num_neurons_per_hidden_layer, num_out))

    if final_activation is not None:
        layers.append(final_activation)

    return nn.Sequential(*layers)
'''
class TestNet(nn.Module):
    def __init__(self, num_in, num_out, final_activation=None, num_hidden_layers=1, num_neurons_rate=64):
        super(TestNet, self).__init__()
        
        self.linear1 = nn.Linear(num_in, num_neurons_rate * 4)
        self.linear2 = nn.Linear(num_neurons_rate * 4, num_neurons_rate * 4)
        self.attn = nn.MultiheadAttention(num_neurons_rate, 4)
        self.linear3 = nn.Linear(num_neurons_rate * 4, num_out)
        self.final_activation = final_activation
        
        self.re_shape1 = (-1, 4, num_neurons_rate)
        self.re_shape2 = (-1, num_neurons_rate * 4)
        


    def forward(self, x):

        x = F.relu(self.linear1(x))
        x = F.relu(self.linear2(x))
        x = x.view(self.re_shape1)
        q = x.clone().detach()
        k = x.clone().detach()
        x, w = self.attn(q, k, x)
        x = F.softmax(x)
        x = x.view(self.re_shape2)
        x = self.linear3(x)
        
        if self.final_activation is not None:
            x = self.final_activation(x)
        return x
'''


class TestNetFromColight(nn.Module):
    def __init__(self, num_intersections, num_neighbors, num_features, num_out, final_activation=None, num_neurons_rate=64):
        super(TestNetFromColight, self).__init__()
        self.debugprint(f'TestNetFromColight init, num_intersections={num_intersections}')
        # 固定參數
        dim = 64   # 每個 agent 的表示維度
        dv = 64    # attention 投影後維度（可與 dim 相同）
        nv = 1     # head 數，這裡固定為 1
        dout = 64  # 中間層輸出維度（可自行調整）
        
        self.num_agents = num_intersections
        self.num_neighbors = num_neighbors
        
        # 輸入 shape 定義（僅作參考）
        self.in_shape0 = (-1, num_intersections, num_features)   # 特徵矩陣
        self.in_shape1 = (-1, num_intersections, num_neighbors, num_intersections)  # 鄰接矩陣
        
        # 特徵處理層
        self.linear1 = nn.Linear(num_features, dim)
        self.linear2 = nn.Linear(dim, dim)
        
        # Attention 分支的線性轉換
        self.linear_att1 = nn.Linear(dim, dv * nv)   # agent 分支
        self.linear_att2 = nn.Linear(dim, dv * nv)   # 鄰居分支
        
        # 鄰居隱藏表示（embedding）分支
        self.linear_emb1 = nn.Linear(dim, dv * nv)
        self.linear_emb2 = nn.Linear(dv * nv, dim)
        
        # 最後全連接層，先把每個 agent 的表示 flatten 後再輸出
        self.linear_out1 = nn.Linear(dim * self.num_agents, dim)
        self.linear_out2 = nn.Linear(dim, num_out)
        
        self.final_activation = final_activation

    def debugprint(self, msg):
        #print(msg)
        return

    def forward(self, x, in_neighbor):
        # x: [B, A, num_features]
        # in_neighbor: [B, A, N, A]
        self.debugprint("input shapes:")
        self.debugprint(x.shape)
        self.debugprint(in_neighbor.shape)
        
        # 固定參數
        dim = 64
        dv = 64
        nv = 1
        
        batch_size = x.shape[0]
        
        # 1. 特徵轉換：先經過兩層全連接與 ReLU
        x = F.relu(self.linear1(x))   # [B, A, dim]
        x = F.relu(self.linear2(x))   # [B, A, dim]
        self.debugprint(f"x after linear: {x.shape}")  # [B, A, 64]
        
        # 2. 建立 agent 表示（每個 agent 的表示為其自身特徵）
        agent_repr = x  # [B, A, dim]
        
        # 3. 建立 neighbor 表示：對每個 agent 重複所有 agent 的特徵，形成 [B, A, A, dim]
        #    其中 axis 1 為目標 agent，axis 2 為所有可能成為鄰居的 agent
        neighbor_repr = x.unsqueeze(1).repeat(1, self.num_agents, 1, 1)
        self.debugprint(f"neighbor_repr shape: {neighbor_repr.shape}")  # [B, A, A, dim]
        
        # 4. 利用 in_neighbor（鄰接矩陣）加權鄰居表示：
        #    in_neighbor: [B, A, N, A] 與 neighbor_repr: [B, A, A, dim]
        #    計算加權和：weighted_neighbor[b,i,n,d] = sum_j in_neighbor[b,i,n,j] * neighbor_repr[b,i,j,d]
        weighted_neighbor = torch.einsum('binj,bijd->bind', in_neighbor, neighbor_repr)
        self.debugprint(f"weighted_neighbor shape: {weighted_neighbor.shape}")  # [B, A, N, dim]
        
        # 5. Attention 分支：分別對 agent 與鄰居表示做線性轉換
        agent_repr_head = F.relu(self.linear_att1(agent_repr))       # [B, A, dv] 
        neighbor_repr_head = F.relu(self.linear_att2(weighted_neighbor))  # [B, A, N, dv]
        
        # 6. 計算注意力分數：對每個 agent，計算其表示與各鄰居表示的點積
        #    att[b, a, n] = dot( agent_repr_head[b, a, :], neighbor_repr_head[b, a, n, :] )
        att = torch.einsum('bad,ban d->ban', agent_repr_head, neighbor_repr_head)  # [B, A, N]
        att = F.softmax(att, dim=-1)  # 在鄰居維度上做 softmax
        self.debugprint(f"attention shape: {att.shape}")  # [B, A, N]
        
        # 7. 鄰居隱藏表示（embedding）：對 weighted_neighbor 再做一次線性變換
        neighbor_hidden = F.relu(self.linear_emb1(weighted_neighbor))  # [B, A, N, dv]
        
        # 8. 利用注意力分數對鄰居隱藏表示做加權求和，得到每個 agent 的聚合表示
        #    out[b, a, d] = sum_n att[b, a, n] * neighbor_hidden[b, a, n, d]
        out = torch.einsum('ban,ban d->bad', att, neighbor_hidden)  # [B, A, dv]
        self.debugprint(f"out after weighted sum: {out.shape}")  # [B, A, 64]
        
        # 9. 經過另一層轉換
        out = F.relu(self.linear_emb2(out))  # [B, A, dim]
        self.debugprint(f"out after linear_emb2: {out.shape}")
        
        # 10. 最後將各 agent 的表示 flatten，再通過全連接層輸出最終結果
        out = out.view(batch_size, -1)   # [B, A * dim]
        out = F.relu(self.linear_out1(out))
        out = self.linear_out2(out)
        self.debugprint(f"final out: {out.shape}")
        
        if self.final_activation is not None:
            out = self.final_activation(out)
        return out
    
'''
紀錄：
目前依據路口壓力公式，只算進入/離開車道車輛數，沒有管車種:是否要 (機車數量*0.5)+(小車*1)+(大車*2) 做為車當量？
目前跑 30 ep 沒啥長進，神經網路需要加大，或者lr需要調整
目前 state 也有點單純，考慮把[各車道平均車速/各車道停等車數量]也加進去
新版pytorch有adamW，聽說比adam強，改天試試
目前信號時相是按照順序切，也許可以改成允許跳著切
也許可以考慮對state做norm
目前train到ep60，loss降到12.8了(剛開始100多)，但reward還是沒長進
信號燈不是每個step都能切換，也許agent可以改成，能切信號燈的時候才呼叫agent，agent看[目前的信號時相是否要延長n秒]
一輪模擬的開頭應該要先把所有燈號全切紅燈，不然初始全車道都綠燈，agent沒下ation的話會撞車
在100多ep時，reward總算有上升的趨勢了，代表模型有學到東西
'''

class ParamsPool:

    def __init__(self,
            num_actions :int,
            gamma: float,
            n_step: int,
        ):

        #self.policy_net = TestNet(num_in=input_dim, num_hidden_layers=3, num_out=num_actions, final_activation=nn.Softmax(dim=1))
        #self.value_net  = TestNet(num_in=input_dim, num_hidden_layers=3, num_out=1,           final_activation=None)
        self.policy_net = TestNetFromColight(num_intersections=num_intersections,
                                  num_neighbors=num_neighbors,
                                  num_features=num_features,
                                  num_out=num_actions, 
                                  final_activation=nn.Softmax(dim=1))
        self.value_net  = TestNetFromColight(num_intersections=num_intersections,
                                  num_neighbors=num_neighbors,
                                  num_features=num_features, 
                                  num_out=1,           
                                  final_activation=None)
        
        if use_cuda:
            self.policy_net = self.policy_net.cuda()
            self.value_net = self.value_net.cuda()
            

        # learning rates are chosen heuristically
        #print("self.policy_net.parameters")
        #print(self.policy_net)
        self.policy_net_optimizer = optim.Adam(self.policy_net.parameters(), lr=learning_rate)
        self.value_net_optimizer  = optim.Adam(self.value_net.parameters(),  lr=learning_rate)

        self.gamma = gamma
        self.n_step = n_step

        # copied from author's implementation in TF
        #self.entropy_loss_weight = 0.01
        #self.value_loss_weight = 0.5
        self.entropy_loss_weight = 0.3
        self.value_loss_weight = 0.5

    def update_networks(self, transitions: Union[NStepTransitions, MonteCarloTransitions], use_sil: bool=False) -> None:

        # these are needed by both
        debugprint(transitions.s0.shape)
        debugprint(transitions.s1.shape)
        dist_over_a_given_s = self.policy_net(transitions.s0, transitions.s1)
        log_p_a_given_s = torch.log(dist_over_a_given_s.gather(1, transitions.a))
        predicted_values_with_grad = self.value_net(transitions.s0, transitions.s1)
        predicted_values_without_grad = predicted_values_with_grad.detach()  # does not affect predicted_values_with_grad

        if use_sil:

            # here, we conveniently use F.relu to achieve the max(~, 0) operation
            monte_carlo_return = transitions.R
            POLICY_LOSS = - torch.mean(log_p_a_given_s * F.relu(monte_carlo_return - predicted_values_without_grad))  # equation 2
            VALUE_LOSS = torch.mean((1 / 2) * F.relu(monte_carlo_return - predicted_values_with_grad) ** 2)  # equation 3
            TOTAL_LOSS = POLICY_LOSS + self.value_loss_weight * VALUE_LOSS  # equation 1

        else:

            n_step_returns = transitions.n_step_sum_of_r + \
                             (self.gamma ** self.n_step) * self.value_net(transitions.n_step_s0, transitions.n_step_s1).detach() * (1 - transitions.done_within_n_step)
            POLICY_LOSS = - torch.mean(log_p_a_given_s * (n_step_returns - predicted_values_without_grad))  # equation 5 (first term)
            ENTROPY_LOSS = - torch.mean(torch.sum(dist_over_a_given_s * torch.log(dist_over_a_given_s), dim=1))  # equation 5 (second term)
            VALUE_LOSS = torch.mean((1 / 2) * (n_step_returns - predicted_values_with_grad) ** 2)  # equation 6
            TOTAL_LOSS = POLICY_LOSS \
                         + self.entropy_loss_weight * ENTROPY_LOSS \
                         + self.value_loss_weight * VALUE_LOSS  # equation 4

        self.policy_net_optimizer.zero_grad()
        self.value_net_optimizer.zero_grad()

        TOTAL_LOSS.backward()

        # doing a gradient clipping between -1 and 1 is equivalent to using Huber loss
        # guaranteed to improve stability so no harm in using at all
        for param in self.policy_net.parameters():
            param.grad.data.clamp_(-5, 5) # -1 1
        for param in self.value_net.parameters():
            param.grad.data.clamp_(-5, 5)

        self.policy_net_optimizer.step()
        self.value_net_optimizer.step()
        return float(TOTAL_LOSS.detach().cpu())

    def act(self, state0: np.array, state1: np.array) -> int:
        #state = torch.tensor(state).unsqueeze(0).float()
        #if len(state.shape) == 3:
        #    state = np.expand_dims(state, axis=0)
        state0 = torch.tensor(state0).float()
        state1 = torch.tensor(state1).float()
        #state= torch.tensor([item.cpu().detach().numpy() for item in state]).cuda() 
        if use_cuda:
            state0 = state0.cuda()
            state1 = state1.cuda()
        with torch.no_grad():
            dist = Categorical(self.policy_net(state0, state1))
        return int(dist.sample())



intersectionList = []
for i in range(len(vissim_out_lane_name)):
    intersectionList.append(Intersection(vissim_in_lane_name[i], vissim_out_lane_name[i]))

# 現在支援8個路口...這段直接叫 GPT 幫忙改
'''
act_index = []
act_one_hot = []
for i in range(16):
    act_index.append([i//8%2, i//4%2, i//2%2, i%2])
    arr = [0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0]
    arr[i] = 1
    act_one_hot.append(arr)
'''
act_index = []
act_one_hot = []
total_actions = num_actions
for i in range(total_actions):
    act_index.append([int(b) for b in f"{i:0{num_intersections}b}"])
    arr = [0] * total_actions
    arr[i] = 1
    act_one_hot.append(arr)

#print(act_index)
#print(act_one_hot)

if NO_ENV:
    env = None
else:
    env = VissimEnv(inpxFilePath, layxFilePath, intersectionList, quickMode=quickMode, simulationTime=vissim_one_round_simulationTime,
                useMaxSimSpeed=True, useAllCores=True, stepGap=train_stepGap)


#訓練
#這邊是原作者的 train_cartpole.py
#模型從CartPole-v0改我自己的VISSIM env

#env = gym.make('CartPole-v0')


#test_obs = [[0, 0, 0, 0, -1, 1, -1, -1, -1, 2, -1, 0, -1, 3, -1, 1, -1, -1, -1, 2, 0, 0, 0.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

if __name__ == '__main__':
    try:

        with open('rewards.pkl', 'rb') as f:
            total_rewards = pickle.load(f)
        with open('losses.pkl', 'rb') as f:
            losses = pickle.load(f)
        with open('iters.pkl', 'rb') as f:
            batch_indexs = pickle.load(f)
        with open('total_average_travel_speed.pkl', 'rb') as f:
            total_average_travel_speed = pickle.load(f)

        x = []
        y1 = []
        y2 = []
        y3 = []
        y4 = []
        t1 = []
        t2 = []
        for value in total_average_travel_speed:
            t1.append(value[0])
            t2.append(value[1])

        window_size = 20 #20 #這邊顯示結果，原本是20輪移動平均，但錄影片時只跑5輪，所以改為2輪移動平均
        for i in range(0, len(total_rewards) - window_size, 1):
            x.append(batch_indexs[i])
            target = np.mean(total_rewards[i:i+window_size])
            y1.append(target)
            target = np.mean(losses[i:i+window_size])
            y2.append(target)
            target = np.mean(t1[i:i+window_size])
            y3.append(target)
            target = np.mean(t2[i:i+window_size])
            y4.append(target)


        '''
        plt.figure(figsize=(12,8))
        plt.plot(x, y1, label="reward")
        plt.plot(x, y2, label="loss")
        plt.xlabel('episode')
        plt.legend()
        plt.show()
        '''
        # reward 和 loss 分開畫，不然loss變化太小會看不出來
        plt.figure(figsize=(12,8))
        plt.plot(x, y1, label="reward")
        plt.xlabel('episode')
        plt.legend()
        plt.show()

        plt.figure(figsize=(12,8))
        plt.plot(x, y2, label="loss")
        plt.xlabel('episode')
        plt.legend()
        plt.show()

        plt.figure(figsize=(12,8))
        plt.plot(x, y3, label="total_average_travel_speed")
        plt.plot(x, y4, label="total_average_travel_speed")
        plt.xlabel('episode')
        plt.legend()
        plt.show()

    except Exception as e:
        print(e)

    print("開始進行RL預測...")
    #讀入訓練完的模型試跑
    env = VissimEnv(inpxFilePath, layxFilePath, intersectionList, quickMode=1, simulationTime=vissim_one_round_simulationTime,
                    useMaxSimSpeed=True, useAllCores=True, stepGap=eval_stepGap)


    param = ParamsPool(
        num_actions=num_actions,
        gamma=gamma,
        n_step=n_step
    )

    param.policy_net.load_state_dict(torch.load("a2c_actor_best_1.pth"))
    param.value_net.load_state_dict(torch.load("a2c_critic_best_1.pth"))

    if use_cuda:
        param.policy_net = param.policy_net.to(device="cuda")
        param.value_net = param.value_net.to(device="cuda")

    #連續實驗10輪
    for i in range(test_epochs):
        print(f'目前運行第{i+1}輪，總共要運行{test_epochs}輪')
        score = 0.0
        done = False
        s = env.reset()
        s0 = s[0]
        s1 = s[1]
        s0 = np.expand_dims(s0, axis=0)
        s1 = np.expand_dims(s1, axis=0)
        times = 0
        while not done:
            if use_cuda:
                prob = param.policy_net(torch.from_numpy(s0).cuda().float(), torch.from_numpy(s1).cuda().float())
                prob = prob.flatten()
                a = Categorical(prob).sample().cpu().numpy()
            else:
                prob = param.policy_net(torch.from_numpy(s0).float(), torch.from_numpy(s1).float())
                prob = prob.flatten()
                a = Categorical(prob).sample().numpy()
            a = int(a)
            s, r, done, info = env.step(act_index[a])
            s0 = s[0]
            s1 = s[1]
            s0 = np.expand_dims(s0, axis=0)
            s1 = np.expand_dims(s1, axis=0)

            times += 1
            if not done:
                print(f"\rstep: {times}, time: {env.vTime//10}, reward: {np.mean(r):.2f}, sigs: {env.sigStates}          ", end="")
            score += np.mean(r)
        print("final reward:")
        print(score)
    print("get_average_travel_speed: ")
    print(env.get_average_travel_speed())
    env.close()
    print('RL預測結束')


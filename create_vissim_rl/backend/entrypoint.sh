#!/bin/sh



export LD_LIBRARY_PATH=/app/backend/thirdparty:$LD_LIBRARY_PATH

cd backend
python manage.py collectstatic --noinput
#python manage.py makemigrations
#python manage.py migrate
#python manage.py makemigrations app
python manage.py migrate app

exec "$@"
# VISSIM RL 程式碼產生器

# 設定檔：
請先把 .env.example 複製一份命名為 .env  
APP_HOST 、 APP_URL 系列：本機執行就保留 127.0.0.1 ，否則填實際的對外IP address  
DJANGO_SECRET_KEY: 亂數填一段長度50以上字串，懶得自己產生的話，可以用這個網頁生成 https://djecrety.ir/  
ACCESS: 請隨意設置，前端呼叫後端 API 時，代入的密碼  

# 單獨啟動後端

```
# 第一次跑
cd xxx\create_vissim_rl\backend\  
pip3 install -r requirements.txt  

# runserver
cd xxx\create_vissim_rl\backend\app  
python3 manage.py runserver  
python3 manage.py runserver --noreload  
```
==========
# 單獨啟動前端
如果沒有 npm 和 Node.js 的話要先安裝  
```
sudo apt update  
sudo apt install nodejs npm -y  
```

```
cd xxx\create_vissim_rl\frontend  

# 第一次跑
npm install  

# runserver
npm run dev  
```
======

# 直接跑 Docker:

若沒安裝 Docker Compose 的話要先安裝  
```
sudo apt update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable"
sudo reboot 然後重連
sudo apt update
sudo apt-get install docker-ce
sudo curl -L "https://github.com/docker/compose/releases/download/v2.6.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo usermod -aG docker ubuntu
newgrp docker
```
若已經安裝的話，直接
```
cd xxx\create_vissim_rl
docker-compose build
docker-compose up
```
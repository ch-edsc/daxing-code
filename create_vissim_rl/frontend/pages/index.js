import { useState } from 'react';
import Head from 'next/head';
import axios from 'axios';
import { StrictMode } from 'react';
import { createRoot} from 'react-dom/client';


// const BACKEND_DOMAIN = 'http://localhost:8000';
const BACKEND_DOMAIN = process.env.NEXT_PUBLIC_BACKEND_DOMAIN;

let root = null;

const sleep = (milliseconds) => {
  //讓 react sleep ，這邊來自 https://www.codegrepper.com/code-examples/javascript/sleep+in+react+js
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
};

const Index = () => {
  // const [textValue, setTextValue] = useState('');
  // const [nowSelect, setNowSelect] = useState('EN-US');
  const [accessValue, setAccessValue] = useState('vissim123');
  const [fileSrc, setFileSrc] = useState({});
  // 全域設定參數
  const [inpxFilePath, setInpxFilePath] = useState("");
  const [layxFilePath, setLayxFilePath] = useState("");
  const [quickMode, setQuickMode] = useState(1);
  const [simulationTime, setSimulationTime] = useState(3900);
  const [yellowTime, setYellowTime] = useState(30);
  const [redTime, setRedTime] = useState(30);

  // 路口數量
  const [scNums, setScNums] = useState(1);
  const [scNames, setScNames] = useState([""]);
  const [scIds, setScIds] = useState([""]);
  const [sigActions, setSigActions] = useState([[[1, 0], [0, 1]]]);
  const [sigActionsColdTimes, setSigActionsColdTimes] = useState([[200, 100]]);
  
  // 進入/離開路段，結構：每個路口包含 4 個方向，每個方向的路段 ID 是一個陣列
  const [vissimInLaneName, setVissimInLaneName] = useState(
    Array.from({ length: scNums }, () => [[], [], [], []])
  );

  const [vissimOutLaneName, setVissimOutLaneName] = useState(
    Array.from({ length: scNums }, () => [[], [], [], []])
  );

  // 更新路口數量
  const handleScNumsChange = (e) => {
    const num = Math.max(1, Math.min(8, Number(e.target.value)));
    setScNums(num);

    setScNames(Array(num).fill("").map((_, i) => scNames[i] || ""));
    setScIds(Array(num).fill("").map((_, i) => scIds[i] || ""));
    setSigActions(Array(num).fill([]).map((_, i) => sigActions[i] || [[1, 0], [0, 1]]));
    setSigActionsColdTimes(Array(num).fill([]).map((_, i) => sigActionsColdTimes[i] || [200, 100]));
    setVissimInLaneName(Array.from({ length: num }, () => [[], [], [], []]));
    setVissimOutLaneName(Array.from({ length: num }, () => [[], [], [], []]));
  };

  // 安全解析 JSON
  const handleTextareaChange = (setter, index, e) => {
    const inputValue = e.target.value;
    try {
      const parsedValue = JSON.parse(inputValue);
      setter((prev) => {
        const updated = [...prev];
        updated[index] = parsedValue;
        return updated;
      });
    } catch (error) {
      console.error("JSON 格式錯誤:", error);
    }
  };

  // 下載 JSON 設定檔
  const handleDownload = () => {
    const config = {
      inpxFilePath,
      layxFilePath,
      quickMode: Number(quickMode),
      vissim_one_round_simulationTime: Number(simulationTime),
      YELLOW_TIME: Number(yellowTime),
      RED_TIME: Number(redTime),
      sc_names: scNames,
      sc_ids: scIds.map(Number),
      sigActions,
      sigActionsColdTimes,
      vissim_in_lane_name: vissimInLaneName,
      vissim_out_lane_name: vissimOutLaneName,
    };

    const jsonString = JSON.stringify(config, null, 2);
    const blob = new Blob([jsonString], { type: "application/json" });
    const url = URL.createObjectURL(blob);

    const link = document.createElement("a");
    link.href = url;
    link.download = "vissim_setting.json";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    URL.revokeObjectURL(url);
  };
  
  const setAccess = (event) => {
    // console.log(event.target.value);
    setAccessValue(event.target.value);
  };

  const taskHandlerWithParams = async (api, params) => {
    try {
      if (!root) {
        const rootElement = document.getElementById('container');
        root = createRoot(rootElement);
      }
      console.log(api);
      const url = `${BACKEND_DOMAIN}/api/v1/${api}`;
      const access = 'Bearer ' + accessValue;
      let response = await axios({
        method: 'GET',
        url,
        headers: {
          Authorization: access,
          'Content-Type': 'application/json'
        },
        params: params
      });
      console.log(response);

      root.render(
        <StrictMode>
          <p>回傳值為：</p>
          <div style={{whiteSpace: 'pre-wrap'}}>{JSON.stringify(response.data.data, 1, 2)}</div>
        </StrictMode>
      );
    } catch (err) {
      console.log('err..');
      console.log(err);
      console.log(err.response.data);
      root.render(
        <StrictMode>
          <p>回傳值為：</p>
          <div style={{whiteSpace: 'pre-wrap'}}>{JSON.stringify(err.response.data, 1, 2)}</div>
        </StrictMode>
      );
    }
  };

  const taskHandlerPostWithParams = async (api, params) => {
    try {
      if (!root) {
        const rootElement = document.getElementById('container');
        root = createRoot(rootElement);
      }
      console.log(api);
      const url = `${BACKEND_DOMAIN}/api/v1/${api}`;
      const access = 'Bearer ' + accessValue;
      let response = await axios({
        method: 'POST',
        url,
        headers: {
          Authorization: access,
          'Content-Type': 'application/json'
        },
        data: params
      });
      console.log(response);

      root.render(
        <StrictMode>
          <p>回傳值為：</p>
          <div style={{whiteSpace: 'pre-wrap'}}>{JSON.stringify(response.data.data, 1, 2)}</div>
        </StrictMode>
      );
    } catch (err) {
      console.log('err..');
      console.log(err);
      console.log(err.response.data);
      root.render(
        <StrictMode>
          <p>回傳值為：</p>
          <div style={{whiteSpace: 'pre-wrap'}}>{JSON.stringify(err.response.data, 1, 2)}</div>
        </StrictMode>
      );
    }
  };

  const uploadFileHandler = async (api) => {
    try {
      if (!root) {
        const rootElement = document.getElementById('container');
        root = createRoot(rootElement);
      }
      console.log(fileSrc);
      const url = `${BACKEND_DOMAIN}/api/v1/${api}`;
      const response = await axios({
        method: 'POST',
        url,
        headers: {
          Authorization: 'Bearer ' + accessValue,
          'Content-Type': 'application/json'
        },
        data: fileSrc
      });
      root.render(
        <StrictMode>
          <p>回傳值為：</p>
          <div style={{whiteSpace: 'pre-wrap'}}>{JSON.stringify(response.data.data, 1, 2)}</div>
        </StrictMode>
      );
    } catch (err) {
      console.log('err..');
      console.log(err);
      console.log(err.response.data);
      root.render(
        <StrictMode>
          <p>回傳值為：</p>
          <div style={{whiteSpace: 'pre-wrap'}}>{JSON.stringify(err.response.data, 1, 2)}</div>
        </StrictMode>
      );
    }
  };

  const downloadFilePost = async (api, params) => {
    try {
      if (!root) {
        const rootElement = document.getElementById('container');
        root = createRoot(rootElement);
      }
      root.render(
        <StrictMode>
          <div style={{whiteSpace: 'pre-wrap'}}></div>
        </StrictMode>
      );

      console.log(fileSrc);

      console.log(api);
      axios({
        url: `${BACKEND_DOMAIN}/api/v1/${api}`,
        headers: {
          Authorization: 'Bearer ' + accessValue,
          'Content-Type': 'application/json'
        },
        method: 'POST',
        data: fileSrc,
        responseType: 'blob'
      })
      .then((response) => {
        const fileName = decodeURIComponent(
          response.headers['content-disposition'].split('filename=')[1]
        ).replaceAll('"', '');
        const href = URL.createObjectURL(response.data);
        const link = document.createElement('a');
        link.href = href;
        link.setAttribute('download', fileName);
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        URL.revokeObjectURL(href);
      })
      .catch((err) => {
        console.log(err);
        root.render(
          <StrictMode>
            <div style={{whiteSpace: 'pre-wrap'}}>{"錯誤:驗證碼不符合"}</div>
          </StrictMode>
        );
      });
    } catch (err) {
      console.log('err1');
      console.log(err);
      root.render(
        <StrictMode>
          <div style={{whiteSpace: 'pre-wrap'}}>{JSON.stringify(err.response.message, 1, 2)}</div>
        </StrictMode>
      );
    }
  };

  const setFileHandler = (event) => {
    const files = (event.target).files;
    if (files.length > 0) {
      const file = files[0];
      const reader = new FileReader();
      reader.addEventListener(
        'load',
        function () {

          setFileSrc(reader.result);
        },
        false
      );
      if (file) {
        reader.readAsDataURL(file);
      }
    }
  };

  const handleScNameChange = (index, e) => {
    const value = e.target.value;
    setScNames((prev) => {
      const updated = [...prev];
      updated[index] = value; // 允許直接輸入字串
      return updated;
    });
  };


  return (
    <div style={styles.body}>
      <Head>
        <title>VISSIM_RL程式碼生成器</title>
      </Head>
      <div style={styles.container}>
        <h1 style={styles.title}>VISSIM_RL 程式碼生成器</h1>
        <div style={styles.section}>
          <button style={styles.button} onClick={() => downloadFilePost('download_setting', {})}>
            下載設定檔範本
          </button>
          <button style={styles.button} onClick={() => downloadFilePost('download_setting_explain', {})}>
            下載設定檔說明文件
          </button>
        </div>

        <div className="p-6 max-w-3xl mx-auto bg-white shadow-md rounded-lg">
          <h2 className="text-xl font-bold mb-4">VISSIM 產生設定檔</h2>
          <p>***目前燈號設定、最短綠燈時間、進入路段、離開路段還沒銜接成功，請下載設定檔回去後再手動編輯檔案***</p>

          {/* 全域設定輸入 */}
          <label>INPX File Path:</label>
          <br></br>
          <input type="text" value={inpxFilePath} onChange={(e) => setInpxFilePath(e.target.value)} />
          <br></br>

          <label>LAYX File Path:</label>
          <br></br>
          <input type="text" value={layxFilePath} onChange={(e) => setLayxFilePath(e.target.value)} />
          <br></br>

          <label>Quick Mode (0 or 1):</label><br></br>
          <input type="number" value={quickMode} onChange={(e) => setQuickMode(e.target.value)} /><br></br>

          <label>Simulation Time (秒):</label><br></br>
          <input type="number" value={simulationTime} onChange={(e) => setSimulationTime(e.target.value)} /><br></br>

          <label>黃燈時間 (秒):</label><br></br>
          <input type="number" value={yellowTime} onChange={(e) => setYellowTime(e.target.value)} /><br></br>

          <label>紅燈時間 (秒):</label><br></br>
          <input type="number" value={redTime} onChange={(e) => setRedTime(e.target.value)} /><br></br>

          <label>路口數量 (1-8):</label><br></br>
          <input type="number" min="1" max="8" value={scNums} onChange={handleScNumsChange} /><br></br>

          {/* 路口設定 */}
          {Array.from({ length: scNums }).map((_, i) => (
            <div key={i} className="mt-4 p-4 border rounded">
              <h3 className="text-lg font-semibold">路口 {i + 1}</h3>

              <label>名稱:</label><br></br>
              <input value={scNames[i]} onChange={(e) => handleScNameChange(i, e)} /><br></br>

              <label>ID:</label><br></br>
              <input type="number" value={scIds[i]} onChange={(e) => handleTextareaChange(setScIds, i, e)} /><br></br>


              <label>燈號設定:</label><br></br>
              <textarea
                value={JSON.stringify(sigActions[i], null, 2)}
                onChange={(e) => handleTextareaChange(setSigActions, i, e)}
              /><br></br>

              <label>最短綠燈時間:</label><br></br>
              <textarea
                value={JSON.stringify(sigActionsColdTimes[i], null, 2)}
                onChange={(e) => handleTextareaChange(setSigActionsColdTimes, i, e)}
              /><br></br>

              <label>進入路段 (北、東、南、西):</label><br></br>
              <textarea
                value={JSON.stringify(vissimInLaneName[i], null, 2)}
                onChange={(e) => handleTextareaChange(setVissimInLaneName, i, e)}
              /><br></br>

              <label>離開路段 (北、東、南、西):</label><br></br>
              <textarea
                value={JSON.stringify(vissimOutLaneName[i], null, 2)}
                onChange={(e) => handleTextareaChange(setVissimOutLaneName, i, e)}
              /><br></br>

            </div>
          ))}

          <button className="mt-6" onClick={handleDownload}>
            下載設定檔
          </button>
        </div>

        <div style={styles.section}>
          <p style={styles.text}>上傳設定檔</p>
          <input type="file" onChange={setFileHandler} style={styles.fileInput} />
        </div>
        <div style={styles.section}>
          <button style={styles.button} onClick={() => downloadFilePost('download_train', {})}>
            下載 train.py
          </button>
          <button style={styles.button} onClick={() => downloadFilePost('download_test', {})}>
            下載 test.py
          </button>
        </div>
        <div id="container" style={styles.containerBox}></div>
      </div>
    </div>
  );
};

const styles = {
  body: {
    fontFamily: "'Roboto', sans-serif",
    background: "linear-gradient(to bottom, #0000FF, #0066CC, #003300)",
    minHeight: "100vh",
    margin: 0,
    color: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    background: "rgba(0, 0, 50, 0.8)",
    borderRadius: "12px",
    padding: "20px",
    width: "80%",
    maxWidth: "600px",
    textAlign: "center",
    boxShadow: "0 0 15px rgba(0, 0, 0, 0.5)",
  },
  title: {
    fontSize: "24px",
    fontWeight: "bold",
    marginBottom: "20px",
    textShadow: "0 0 10px #00c6ff",
  },
  section: {
    marginBottom: "20px",
  },
  text: {
    fontSize: "16px",
    padding: "20px",
    marginBottom: "10px",
  },
  input: {
    width: "80%",
    padding: "10px",
    borderRadius: "8px",
    border: "1px solid #00c6ff",
    background: "transparent",
    color: "#fff",
    outline: "none",
    fontSize: "16px",
  },
  fileInput: {
    color: "#fff",
    background: "transparent",
    outline: "none",
  },
  button: {
    background: "linear-gradient(to right, #00c6ff, #0072ff)",
    border: "none",
    color: "#fff",
    padding: "10px 20px",
    margin: "5px",
    borderRadius: "8px",
    fontSize: "16px",
    cursor: "pointer",
    boxShadow: "0 4px 8px rgba(0, 0, 0, 0.2)",
    transition: "all 0.3s ease",
  },
  buttonHover: {
    transform: "scale(1.1)",
    boxShadow: "0 6px 12px rgba(0, 0, 0, 0.3)",
  },
  containerBox: {
    marginTop: "20px",
  },
};
export default Index;
